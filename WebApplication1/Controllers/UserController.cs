﻿using IAV.Data.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Web.Http;

namespace IAVAPI.Controllers
{
   
    public class UserController :  IDisposable
    {
        string constring = ConfigurationManager.ConnectionStrings["markeet_demoEntities"].ConnectionString;
        markeet_demoEntities dbContextIAV = new markeet_demoEntities();

        public bool checkAuthorization(string token)
        {
            //var resultFailed = new { status = "Failed", msg = "Unauthorized" };
            if (token != null)
            {
                var u = dbContextIAV.users.Where(s => s.password == token);
                if (u.Count() > 0)
                {
                    return true;
                }
            }
            return false;
        }

        public string insertOne(user user)
        {
            var fail = new { status = "fail", msg = "fail" };
            var success = new { status = "success", msg = "User added successfully!" };
            try
            {
                dbContextIAV.users.Add(user);
                dbContextIAV.SaveChanges();
                return JsonConvert.SerializeObject(success);
            }
            catch (Exception e)
            { 
                return JsonConvert.SerializeObject(fail);
            }
        }

        public string updateOne(user user)
        {
            var fail = new { status = "Failed", msg = "failed" };
            var success = new { status = "success", msg = "User updated successfully!" };
            user userUpdate = dbContextIAV.users.Where(u => u.id == user.id).FirstOrDefault();
            try
            {
                userUpdate.email = user.email;
                userUpdate.name = user.name;
                //userUpdate.password = user.password;
                userUpdate.username = user.username;
                dbContextIAV.Entry(userUpdate).State = System.Data.Entity.EntityState.Modified;
                dbContextIAV.SaveChanges();
                return JsonConvert.SerializeObject(success);
            }
            catch (DbEntityValidationException e)
            {
                return JsonConvert.SerializeObject(fail);
            }
            catch (Exception e)
            {
                return JsonConvert.SerializeObject(fail);
            }
        }

        public user findOne(int id)
        {
            return dbContextIAV.users.Where(s => s.id == id).FirstOrDefault();
        }

        public user processLogin(user usr)
        {
            //          if ($this->get_request_method() != "POST") $this->response('', 406);
            //$customer = json_decode(file_get_contents("php://input"), true);
            //$username = $customer["username"];
            //$password = $customer["password"];
            //          if (!empty($username) and !empty($password)){ // empty checker
            //	$query = "SELECT id, name, username, email, password FROM user WHERE password = '".md5($password)."' AND username = '$username' LIMIT 1";
            //	$r = $this->mysqli->query($query) or die($this->mysqli->error.__LINE__);
            //              if ($r->num_rows > 0) {
            //		$result = $r->fetch_assoc();
            //	    $resp = array('status' => "success", "user" => $result);
            //		$this->show_response($resp);
            //              }
            //	$error = array('status' => "failed", "msg" => "Username or Password not found");
            //	$this->show_response($error);
            //          }
            //$error = array('status' => "failed", "msg" => "Invalid username or Password");
            //$this->show_response($error);

            
            if (usr != null)
            {
                if(!string.IsNullOrEmpty(usr.username) && !string.IsNullOrEmpty(usr.password))
                {
                    string pwd = CalculateMD5Hash(usr.password);
                    user u = dbContextIAV.users.Where(us => us.username == usr.username && us.password == pwd).FirstOrDefault();
                    if (u != null)
                        return u;
                    else
                        return null;
                }
                
            }
            return null;
        }

        public string CalculateMD5Hash(string input)
        {

            // step 1, calculate MD5 hash from input

            MD5 md5 = MD5.Create();

            byte[] inputBytes = Encoding.ASCII.GetBytes(input);

            byte[] hash = md5.ComputeHash(inputBytes);

            // step 2, convert byte array to hex string

            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }

            return sb.ToString();

        }


        void IDisposable.Dispose()
        {

        }
    }
}