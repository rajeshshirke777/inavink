﻿
using IAV.Data.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace IAVAPI.Controllers
{
    
    public class ClientController : IDisposable
    {
        string constring = ConfigurationManager.ConnectionStrings["markeet_demoEntities"].ConnectionString;
        markeet_demoEntities dbContextIAV = new markeet_demoEntities();

        public string info(string version)
        {
            int versioncode = 0;
            int.TryParse(version, out versioncode);
            if (versioncode == 0)
            {
                //throw error that version is incorrect
                var resp = new { status = "fail", info = "version code incorrect!" };
                return JsonConvert.SerializeObject(resp);
            }

            //$query = "SELECT COUNT(DISTINCT a.id) FROM app_version a WHERE version_code = $version AND active = 1";
            int count = dbContextIAV.app_version.Where(v => v.version_code == versioncode && v.active == true).Select(v => v.id).Distinct().Count();
            if (dbContextIAV.app_version.Any(v => v.version_code == versioncode && v.active == true))
            {
                //get config
                List<config> config = new List<IAV.Data.Models.config>();

                using (ConfigController c = new ConfigController())
                {
                    config = c.findAllArr();
                }

                //array to be returned
                bool active = count > 0 ? true : false;
                string tax = getValue(config, "TAX");
                string currency = getValue(config, "CURRENCY");
                string shipping = getValue(config, "SHIPPING"); // json_decode($this->getValue($config_arr, 'SHIPPING'), true) TITIKSHA JSON DECODE TO BE DONE
                char[] ship;
                if (shipping == "[]")
                    ship = new char[0];
                else
                    ship = shipping.ToCharArray();
                var info = new { active = active, tax = tax, currency = currency, shipping = ship };

                //$response = array("status" => "success", "info" => $info);
                var response = new { status = "success", info = info };

                return JsonConvert.SerializeObject(response);
            }
            else 
            {

                var resp = new { status = "fail", info = "version code incorrect!" };
                return JsonConvert.SerializeObject(resp);
            
            
            }
        }

        public string findAllFeaturedNewsInfo()
        {
            using (NewsInfoController cc = new NewsInfoController())
            {
                List<news_info> n =  cc.findAllFeatured();

                // array('status' => 'success', 'news_infos' => $object_res);
                var response = new { status = "success", news_infos = n };
                return JsonConvert.SerializeObject(response);
            }
            
        }

        public string findAllProduct(string page = "1", string limit = "10", string q = "", string category_id = "-1")
        {
            int intPage = 0;
            int intLimit = 0;
            int intCategory = 0;

            int.TryParse(page, out intPage);
            int.TryParse(limit, out intLimit);
            int.TryParse(category_id, out intCategory);

            if (intPage == 0 || intLimit == 0 || intCategory == 0)
            {
                var resp = new { status = "fail" };
                return JsonConvert.SerializeObject(resp);
            }

            int offset = (intPage * intLimit) - intLimit;

            //$count_total = $this->product->allCountPlainForClient($q, $category_id);
            //$products = $this->product->findAllByPagePlainForClient($limit, $offset, $q, $category_id);

            using (ProductController p = new ProductController())
            {
                int count_total = p.allCountPlainForClient(q, intCategory);
                List<product> product = p.findAllByPagePlainForClient(intLimit, offset, q, intCategory);
                int count = product.Count;

                // $response = array(            'status' => 'success', 'count' => $count, 'count_total' => $count_total, 'pages' => $page, 'products' => $object_res        );
                var response = new { status = "success", count = count, count_total= count_total , pages = intPage, products = product};
                return JsonConvert.SerializeObject(response, Formatting.None,
                        new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                        });
            }
        }

        public string findProductDetails(string id)
        {
            int product_id = 0;
            int.TryParse(id, out product_id);
            if (product_id == 0)
            {
                //throw error that version is incorrect
                var resp = new { status = "failed", msg = "product Id incorrect!" };
                return JsonConvert.SerializeObject(resp);
            }
            using (ProductController p = new ProductController())
            using (CategoryController c = new CategoryController())
            using (ProductImageController pi = new ProductImageController())
            {
                product product = p.findOnePlain(product_id);
                if (product != null)
                {
                    List<category> categories = c.getAllByProductIdPlain(product_id);
                    List<product_image> product_images = pi.findAllByProductIdPlain(product_id);

                    var response = new { status = "success", product = new { id = product.id, name =  product.name, image = product.image, price = product.price, stock = product.stock, draft = product.draft, description = product.description, status = product.status, created_at = product.created_at, last_update = product.last_update, categories = categories, product_images = product_images } };
                    return JsonConvert.SerializeObject(response, Formatting.None,
                        new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                        });
                }
                else
                {
                    var response = new { status = "failed", product = (product)null };
                    return JsonConvert.SerializeObject(response);
                }
            }     
        }

        public string findAllCategory()
        {
            //$categories = $this->category->findAllForClient();
            //$response = array(            'status' => 'success', 'categories' => $categories        );
            using (CategoryController c = new CategoryController())
            {
                List<category> category =  c.findAllForClient();
                var response = new { status = "success", categories = category };
                return JsonConvert.SerializeObject(response, Formatting.None,
                        new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                        });
            }
        }

        public string findAllNewsInfo(string page = "1", string limit = "10", string q = "")
        {
            //$offset = ($page * $limit) - $limit;
            int intPage = 0;
            int intLimit = 0;

            int.TryParse(page, out intPage);
            int.TryParse(limit, out intLimit);

            if (intPage == 0 || intLimit == 0)
            {
                var resp = new { status = "fail" };
                return JsonConvert.SerializeObject(resp);
            }

            int offset = (intPage * intLimit) - intLimit;

            //$count_total = $this->news_info->allCountPlain($q, 1);
            //$news_infos = $this->news_info->findAllByPagePlain($limit, $offset, $q, 1);
            //$count = count($news_infos);
            //$response = array('status' => 'success', 'count' => $count, 'count_total' => $count_total, 'pages' => $page, 'news_infos' => $object_res );
            
            using (NewsInfoController ni = new NewsInfoController())
            {
                int count_total = ni.allCountPlain(q, 1);
                List <news_info> news = ni.findAllByPagePlain(intLimit, intPage, q, 1);
                int count = news.Count;

                var response = new { status = "success", count = count , count_total  = count_total , pages= intPage, news_infos = news };

                return JsonConvert.SerializeObject(response, Formatting.None,
                        new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                        });
            }
        }

        public string findNewsDetails(string id)
        {
            
            int intId = 0;
            int.TryParse(id, out intId);

            if (intId == 0)
            {
                var resp = new { status = "fail" };
                return JsonConvert.SerializeObject(resp);
            }

            using (NewsInfoController cc = new NewsInfoController())
            {
                news_info n = cc.findOnePlain(intId);

                // array('status' => 'success', 'news_infos' => $object_res);
                var response = new { status = "success", news_info = n };
                return JsonConvert.SerializeObject(response);
            }
        }

        public string submitProductOrder(product_order po, List<product_order_detail> pod)
        {
            /* TITIKSHA - To be done
            // checking security code
            if (!isset($this->_header['Security']) || $this->_header['Security'] != $this->conf->SECURITY_CODE){
            $m = array('status' => 'failed', 'msg' => 'Invalid security code', 'data' => null);
            $this->show_response($m);
                return;
            }
            */
            /*
            //read json
            product_order po = dbContextIAV.product_order.Where(p => p.id == 1).FirstOrDefault();
            List<product_order_detail> pod = dbContextIAV.product_order_detail.Where(p => p.order_id == 1).ToList();
            var json = new { product_order = po, product_order_detail = pod };
            string jsondata = JsonConvert.SerializeObject(json, Formatting.None,
                        new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                        });
            */
            
            // submit order
            using (ProductOrderController poc = new ProductOrderController())
            using (ProductOrderDetailsController podc = new ProductOrderDetailsController())
            {
                dynamic product_response = JsonConvert.DeserializeObject(poc.insertOnePlain(po));
                string status = product_response.status.ToObject<string>();
                product_order addedproduct = product_response.data.ToObject<product_order>();

                if (status.Contains("success"))
                {
                    long order_id = addedproduct.id;
                    var productorderdetail_response = podc.insertAllPlain(order_id, pod);

                    if (productorderdetail_response.ToLower().Contains("success"))
                    {
                        var success = new { status = "success", msg = "Success submit product order", data = po };//data has product_order

                        return JsonConvert.SerializeObject(success, Formatting.None,
                        new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                        });
                    }
                    else
                    {
                        poc.deleteOnePlain(order_id);
                        var failed = new { status = "failed", msg = "Failed when submit order", data = po };
                        return JsonConvert.SerializeObject(failed, Formatting.None,
                        new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                        });
                    }
                }
                else
                {
                    var failed = new { status = "failed", msg = "Failed when submit order", data = po };
                    return JsonConvert.SerializeObject(failed, Formatting.None,
                       new JsonSerializerSettings()
                       {
                           ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                       });
                }
            }
        }

        private string getValue(List<config> data, string code)
        {
            foreach (config c in data)
            {
                if (c.code == code)
                {
                    return c.value;
                }
            }
            return string.Empty;
        }


        void IDisposable.Dispose()
        {

        }
    }
}