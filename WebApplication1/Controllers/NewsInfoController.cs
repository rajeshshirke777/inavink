﻿using IAV.Data.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace IAVAPI.Controllers
{
  
    public class NewsInfoController :  IDisposable
    {
        string constring = ConfigurationManager.ConnectionStrings["markeet_demoEntities"].ConnectionString;
        markeet_demoEntities dbContextIAV = new markeet_demoEntities();

        public int countFeaturedPlain()
        {
            //$query = "SELECT COUNT(DISTINCT ni.id) FROM news_info ni WHERE ni.status='FEATURED' AND ni.draft=0 ";
            return dbContextIAV.news_info.Where(s => s.status == "FEATURED" && s.draft == false).Distinct().Count();
        }

        public int countByDraftPlain(int i)
        {
            //string query = "SELECT COUNT(DISTINCT ni.id) FROM news_info ni WHERE ni.draft=$i ";
            return dbContextIAV.news_info.Where(s => s.draft == (i == 0 ? false : true)).Distinct().Count();
        }

        public List<news_info> findAll()
        {
            //if ($this->get_request_method() != "GET") $this->response('', 406);
            //$this->show_response($this->findAllPlain());
            return findAllPlain();
        }

        public List<news_info> findAllPlain()
        {
            //$query = "SELECT * FROM news_info ni ORDER BY ni.id DESC";
            //return $this->db->get_list($query);

            return dbContextIAV.news_info.OrderByDescending(o => o.id).ToList();
        }

        public news_info findOnePlain(int id)
        {
            //$query = "SELECT distinct * FROM news_info ni WHERE ni.id=$id";
            return findOne(id);
        }

        public news_info findOne(int id)
        {
            //if ($this->get_request_method() != "GET") $this->response('', 406);
            //if (!isset($this->_request['id'])) $this->responseInvalidParam();
            //$id = (int)$this->_request['id'];
            //$this->show_response($this->findOnePlain($id));

            return dbContextIAV.news_info.Find(id);
        }

        public int allCountPlain(string q, int client)
        {
            //$query = "SELECT COUNT(DISTINCT ni.id) FROM news_info ni ";
            //$keywordQuery = "(ni.title REGEXP '$q' OR ni.brief_content REGEXP '$q' OR ni.full_content REGEXP '$q') ";
            //if ($client != 0){
            //$query = $query. " WHERE ni.draft <> 1 ";
            //if ($q != "") $query = $query. "AND ". $keywordQuery;
            //} else {
            //if ($q != "") $query = $query. "WHERE ". $keywordQuery;
            //}
            //return $this->db->get_count($query);
            List<news_info> news = new List<news_info>();
            if (!string.IsNullOrEmpty(q))
            {
                if (client != 0)
                    news = dbContextIAV.news_info
                                .Where(ni => ni.title.ToLower().Contains(q.ToLower()) ||
                                ni.brief_content.ToLower().Contains(q.ToLower())
                                || ni.full_content.ToLower().Contains(q.ToLower()))
                                .ToList();
                else
                    news = dbContextIAV.news_info
                                .Where(ni => ni.title.ToLower().Contains(q.ToLower()) ||
                                ni.brief_content.ToLower().Contains(q.ToLower())
                                || ni.full_content.ToLower().Contains(q.ToLower())
                                && !ni.draft)
                                .ToList();
            }
            else
            {
                news = dbContextIAV.news_info.Distinct().OrderByDescending(o => o.id).ToList();
            }
            return news.Count;

        }

        public int allCount(string q)
        {
            //if ($this->get_request_method() != "GET") $this->response('', 406);
            //$q = (isset($this->_request['q'])) ? ($this->_request['q']) : "";
            //$client = (isset($this->_request['client'])) ? ((int)$this->_request['client']) : 0;
            //$this->show_response_plain($this->allCountPlain($q, $client));

            return allCountPlain(q, 0);
        }

        public List<news_info> findAllByPage(int limit, int page, string q)
        {
            return findAllByPagePlain(limit, page, q, 0);
        }

        public List<news_info> findAllByPagePlain(int limit, int page, string q, int client)
        {
            //$query = "SELECT ni.* FROM news_info ni ";
		    //$keywordQuery = "(ni.title REGEXP '$q' OR ni.brief_content REGEXP '$q' OR ni.full_content REGEXP '$q') ";
            //if ($client != 0){
		    //$query = $query. " WHERE ni.draft <> 1 ";
            //if ($q != "") $query = $query. "AND ". $keywordQuery;
            //} else {
            //if ($q != "") $query = $query. "WHERE ". $keywordQuery;
            //}
		    //$query = $query. "ORDER BY ni.id DESC LIMIT $limit OFFSET $offset ";
            //return $this->db->get_list($query);

            List<news_info> news = new List<news_info>();
            int offset = page - 1;

            if (!string.IsNullOrEmpty(q))
            {
                if (client != 0)
                    news = dbContextIAV.news_info
                                .Where(c => c.title.ToLower().Contains(q.ToLower())
                                || c.brief_content.ToLower().Contains(q.ToLower())
                                || c.full_content.ToLower().Contains(q.ToLower())
                                || !c.draft)
                                .ToList().Distinct().OrderByDescending(o => o.id).Skip(offset).Take(limit).ToList();
                else
                    news = dbContextIAV.news_info
                                 .Where(c => c.title.ToLower().Contains(q.ToLower())
                                || c.brief_content.ToLower().Contains(q.ToLower())
                                || c.full_content.ToLower().Contains(q.ToLower())
                                || !c.draft)
                                .ToList().Distinct().OrderByDescending(o => o.id).Skip(offset).Take(limit).ToList();

            }
            else
            {
                news = dbContextIAV.news_info.Distinct().OrderByDescending(o => o.id).Skip(offset).Take(limit).ToList();
            }
            return news;
        }

        public string insertOne(news_info news)
        {
            //if ($this->get_request_method() != "POST") $this->response('', 406);
		    //$data = json_decode(file_get_contents("php://input"), true);
            //if (!isset($data)) $this->responseInvalidParam();
		    //$column_names = array('title', 'brief_content', 'full_content', 'image', 'draft', 'status', 'created_at', 'last_update');
		    //$table_name = 'news_info';
		    //$pk = 'id';

            //if ($data['status'] == 'FEATURED' && $data['draft'] == 0 && $this->isFeaturedExceed() == 1){
		    //$msg = array('status' => "failed", "msg" => "Featured News exceed the maximum amount", "data" => null);
		    //$this->show_response($msg);
            //}
		    //$resp = $this->db->post_one($data, $pk, $column_names, $table_name);
		    //$this->show_response($resp);

            news_info ni = new news_info();
            ni.title = news.title;
            ni.brief_content = news.brief_content;
            ni.full_content = news.full_content;
            ni.image = news.image;
            ni.draft = news.draft;
            ni.created_at = news.created_at;
            ni.last_update = news.last_update;

            if(news.status == "FEATURED" && !news.draft && isFeaturedExceed() == 1)
            {
                string msg = "{'staus':" + "failed" + ",'msg':" + "Featured News exceed the maximum amount" + ",'data':" + "null" + "}";
                return msg;
            }

            ni.status = news.status;
            dbContextIAV.news_info.Add(ni);
            dbContextIAV.SaveChanges();
            return "success";//dbContextIAV.categories.Where(pr => pr.id == dbContextIAV.categories.Max(p => p.id)).FirstOrDefault();
        }

        public string updateOne(int id, news_info news)
        {
  //          if ($this->get_request_method() != "POST") $this->response('', 406);
		//$data = json_decode(file_get_contents("php://input"), true);
  //          if (!isset($data['id'])) $this->responseInvalidParam();
		//$id = (int)$data['id'];
		//$column_names = array('title', 'brief_content', 'full_content', 'image', 'draft', 'status', 'created_at', 'last_update');
		//$table_name = 'news_info';
		//$pk = 'id';
  //          if ($data[$table_name]['status'] == 'FEATURED' && $data[$table_name]['draft'] == 0 && $this->isFeaturedExceed() == 1){
		//	$msg = array('status' => "failed", "msg" => "Featured News exceed the maximum amount", "data" => null);
		//	$this->show_response($msg);
  //          }
  //          if ($data[$table_name]['status'] == 'NORMAL' && $this->countFeaturedPlain() <= 1){
  //          $m = array('status' => "failed", "msg" => "Ops, At least there is one FEATURED news", "data" => null);
  //          $this->show_response($m);
  //              return;
  //          }
		//$this->show_response($this->db->post_update($id, $data, $pk, $column_names, $table_name));

            try
            {
                news_info ni = dbContextIAV.news_info.Find(id);
                dbContextIAV.Entry(ni).State = EntityState.Modified;
                ni.title = news.title;
                ni.brief_content = news.brief_content;
                ni.full_content = news.full_content;
                ni.image = news.image;
                ni.draft = news.draft;
                ni.created_at = news.created_at;
                ni.last_update = news.last_update;

                if (news.status == "FEATURED" && !news.draft && isFeaturedExceed() <= 1)
                {
                    string msg = "{'staus':" + "failed" + ",'msg':" + "Featured News exceed the maximum amount" + ",'data':" + "null" + "}";
                    return msg;
                }

                if (news.status == "NORMAL" && countFeaturedPlain() == 1)
                {
                    string msg = "{'staus':" + "failed" + ",'msg':" + "Ops, At least there is one FEATURED news" + ",'data':" + "null" + "}";
                    return msg;
                }

                ni.status = news.status;
                dbContextIAV.SaveChanges();
                return "success";// c;
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                Console.Write(ex.StackTrace);
                return "fail";
            }
        }

        public string deleteOne(int id)
        {
            //if ($this->get_request_method() != "GET") $this->response('', 406);
            //if (!isset($this->_request['id'])) $this->responseInvalidParam();
		    //$id = (int)$this->_request['id'];
		    //$table_name = 'news_info';
		    //$pk = 'id';
		    //$data = $this->findOnePlain($id);
            //if ($data['status'] == 'FEATURED' && $this->countFeaturedPlain() <= 1){
            //$m = array('status' => "failed", "msg" => "Ops, At least there is one FEATURED news", "data" => null);
            //$this->show_response($m);
            //return;
            //}
		    //$this->show_response($this->db->delete_one($id, $pk, $table_name));
            try
            {
                news_info news = dbContextIAV.news_info.Find(id);
                if (news.status == "NORMAL" && countFeaturedPlain() == 1)
                {
                    string msg = "{'staus':" + "failed" + ",'msg':" + "Ops, At least there is one FEATURED news" + ",'data':" + "null" + "}";
                    return msg;
                }
                
                dbContextIAV.news_info.Remove(news);
                dbContextIAV.SaveChanges();
                return "success";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public int isFeaturedExceed()
        {
            //$key_code = 'FEATURED_NEWS';
            //$query = "SELECT COUNT(counter) AS resp from (SELECT COUNT(id) AS counter FROM news_info WHERE status = 'FEATURED' AND draft = 0) as N "
            //   ."WHERE N.counter >= (SELECT value FROM config WHERE code = '$key_code')";
            //return $this->db->get_one($query)['resp'];

            string key_code = "FEATURED_NEWS";
            var val = dbContextIAV.configs.Where(c => c.code == key_code).ToList();
            int n = dbContextIAV.news_info.Where(ni => ni.status == "FEATURED" && ni.draft == false).Count();
            var s = val.Where(v => Convert.ToInt32(v.value) >= n).ToList().Count;
            return s;
        }

        public List<news_info> findAllFeatured()
        {
            //if ($this->get_request_method() != "GET") $this->response('', 406);
            //$query = "SELECT * FROM news_info ni WHERE ni.status='FEATURED' AND ni.draft=0 ORDER BY ni.id DESC";
            //return $this->db->get_list($query);
            return dbContextIAV.news_info.Where(ni => ni.status == "FEATURED" && !ni.draft).OrderByDescending(ni => ni.id).ToList();
        }

        void IDisposable.Dispose()
        {

        }
    }
}