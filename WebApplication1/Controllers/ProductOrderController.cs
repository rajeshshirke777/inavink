﻿
using IAV.Data.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace IAVAPI.Controllers
{
  
    public class ProductOrderController :  IDisposable
    {
        string constring = ConfigurationManager.ConnectionStrings["markeet_demoEntities"].ConnectionString;
        markeet_demoEntities dbContextIAV = new markeet_demoEntities();

        public int countByStatusPlain(string status)
        {
            //string query = "SELECT COUNT(DISTINCT po.id) FROM product_order po WHERE po.status='$status' ";
            return dbContextIAV.product_order.Where(s => s.status == status).Distinct().Count();
        }

        public int allCount()
        {
            //string query="SELECT COUNT(DISTINCT po.id) FROM product_order po";
            return dbContextIAV.product_order.Distinct().Count();
        }

        public List<product_order> findAllByPage(int page, int limit, string q)
        {
            //if($this->get_request_method() != "GET") $this->response('',406);
            //if(!isset($this->_request['limit']) || !isset($this->_request['page']))$this->responseInvalidParam();
            //$q = (isset($this->_request['q'])) ? ($this->_request['q']) : "";

            List<product_order> po = new List<product_order>();
            int offset = page - 1;
            if (!string.IsNullOrEmpty(q))
            {
               // string query = @"SELECT DISTINCT * FROM product_order po
                    //WHERE buyer REGEXP '$q' OR code REGEXP '$q' OR address REGEXP '$q' OR email REGEXP '$q' OR phone REGEXP '$q' OR comment REGEXP '$q' OR shipping REGEXP '$q' 
                    //ORDER BY po.id DESC LIMIT $limit OFFSET $offset";

                po = dbContextIAV.product_order
                    .Where(p => p.buyer.ToLower().Contains(q.ToLower()) ||
                        p.code.ToLower().Contains(q.ToLower()) ||
                        p.address.ToLower().Contains(q.ToLower()) ||
                        p.email.ToLower().Contains(q.ToLower()) ||
                        p.phone.ToLower().Contains(q.ToLower()) ||
                        p.comment.ToLower().Contains(q.ToLower()) ||
                        p.shipping.ToLower().Contains(q.ToLower()) 
                        )
                    .Distinct().OrderByDescending(o => o.id).Skip(offset).Take(limit).ToList();
            }
            else
            {
                //string query = "SELECT DISTINCT * FROM product_order po ORDER BY po.id DESC LIMIT $limit OFFSET $offset";
                po = dbContextIAV.product_order.Distinct().OrderByDescending(o => o.id).Skip(offset).Take(limit).ToList();
            }
            return po;
        }

        public string insertOne(product_order po)
        {
            return insertOnePlain(po);
        }

        public string deleteOnePlain(long id)
        {
            var success = new { status = "success", msg = "Order deleted successfully!" };
            var fail = new { status = "fail", msg = "Failed!" };

            List<product_order> po = dbContextIAV.product_order.Where(p => p.id == id).ToList();

            try
            {
                foreach (product_order p in po)
                {
                    dbContextIAV.product_order.Remove(p);
                    dbContextIAV.Entry(p).State = System.Data.Entity.EntityState.Deleted;
                    dbContextIAV.SaveChanges();
                }
                return JsonConvert.SerializeObject(success);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(fail);
            }
        }

        public string insertOnePlain(product_order po)
        {
            dbContextIAV.product_order.Add(po);
            try
            {
                dbContextIAV.Entry(po).State = System.Data.Entity.EntityState.Added;
                dbContextIAV.SaveChanges();
                var success = new { status = "success", msg = "Order saved successfully!", data = po };
                return JsonConvert.SerializeObject(success, Formatting.None,
                        new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                        });
            }
            catch (DbEntityValidationException e)
            {
                var fail = new { status = "fail", msg = "Failed!", data = po };
                return JsonConvert.SerializeObject(fail, Formatting.None,
                        new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                        });
            }
            catch (Exception e)
            {
                var fail = new { status = "fail", msg = "Failed!", data = po };
                return JsonConvert.SerializeObject(fail, Formatting.None,
                        new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                        });
            }

        }

        public string updateOne(product_order po)
        {
            var success = new { status = "success", msg = "Order updated successfully!" };
            var fail = new { status = "fail", msg = "Failed!" };

            product_order order_update = dbContextIAV.product_order.Where(p => p.id == po.id).FirstOrDefault();

            order_update.address = po.address;
            order_update.buyer = po.buyer;
            order_update.code = po.code;
            order_update.comment = po.comment == null ? string.Empty : po.comment;
            order_update.date_ship = po.date_ship;
            order_update.email = po.email;
            order_update.last_update = po.last_update;
            order_update.phone = po.phone;
            order_update.shipping = po.shipping;
            order_update.status = po.status;
            order_update.tax = po.tax;
            order_update.total_fees = po.total_fees;

            try
            {
                dbContextIAV.Entry(order_update).State = System.Data.Entity.EntityState.Modified;
                dbContextIAV.SaveChanges();
                return JsonConvert.SerializeObject(success);
            }
            catch (DbEntityValidationException e)
            {
                return JsonConvert.SerializeObject(fail);
            }
            catch(Exception e)
            {
                return JsonConvert.SerializeObject(fail);
            }

        }

        public string deleteOne(int order_id)
        {
            var success = new { status = "success" };
            var fail = new { status = "fail" };

            product_order order_delete = dbContextIAV.product_order.Where(s => s.id == order_id).FirstOrDefault();

            try
            {
                dbContextIAV.Entry(order_delete).State = System.Data.Entity.EntityState.Deleted; 
                dbContextIAV.SaveChanges();
                return JsonConvert.SerializeObject(success);
            }
            catch (DbEntityValidationException e)
            {
                return JsonConvert.SerializeObject(fail);
            }
            catch (Exception e)
            {
                return JsonConvert.SerializeObject(fail);
            }

        }

        public processOrderModel processOrder(int id, product_order order, List<product_order_detail> order_detail)
        {
            processOrderModel model = new processOrderModel();
            try
            {
                using (ProductOrderDetailsController pod = new ProductOrderDetailsController())
                {
                    model = pod.checkAvailableProductOrderDetail(order_detail);
                    if (model.status == "success")
                    {
                        // process product stock
                        //   foreach($resp_od['data'] as $od){
                        foreach (processOrderDetailsModel d in model.data)
                        {
                            int val = d.stock - d.amount;
                            //       $product_id = $od['product_id'];
                            //       $query = "UPDATE product SET stock=$val WHERE id=$product_id";
                            //       $this->mysqli->query($query) or die($this->mysqli->error.__LINE__);
                            product order_update = dbContextIAV.products.Where(p => p.id == d.product_id).FirstOrDefault();
                            order_update.stock = val;
                            dbContextIAV.Entry(order_update).State = System.Data.Entity.EntityState.Modified;
                            dbContextIAV.SaveChanges();
                        }
                        //       // update order status
                        //       $order_id = $order['id'];
                        product_order o = dbContextIAV.product_order.Where(p => p.id == order.id).FirstOrDefault();
                        o.status = "PROCESSED";
                        dbContextIAV.Entry(o).State = System.Data.Entity.EntityState.Modified;
                        dbContextIAV.SaveChanges();
                        //       $query_2 = "UPDATE product_order SET status='PROCESSED' WHERE id=$order_id";
                        //       $this->mysqli->query($query_2) or die($this->mysqli->error.__LINE__);
                    }
                }
            }
            catch (DbEntityValidationException e)
            {

            }
            catch (Exception e)
            {

            }
            return model;
        }

        public product_order findOne(int order_id)
        { 
             //$query="SELECT distinct * FROM product_order po WHERE po.id=$id";
            return dbContextIAV.product_order.Where(o => o.id == order_id).Distinct().FirstOrDefault();
        }

        void IDisposable.Dispose()
        {

        }

    }
}