﻿
using IAV.Data.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace IAVAPI.Controllers
{
    
    public class FcmController :  IDisposable
    {
        string constring = ConfigurationManager.ConnectionStrings["markeet_demoEntities"].ConnectionString;
        markeet_demoEntities dbContextIAV = new markeet_demoEntities();

        public List<fcm> findAll()
        {
            //$query = "SELECT DISTINCT f.id, f.device, f.version, f.serial, f.regid, f.created_at FROM fcm f ORDER BY f.last_update DESC";
            return dbContextIAV.fcms.Distinct().OrderByDescending(o => o.last_update).ToList();
        }

        public int allCount(string q)
        {
            if (!string.IsNullOrEmpty(q)) {
                //$query = "SELECT COUNT(DISTINCT f.id) FROM fcm f WHERE device REGEXP '$q' OR serial REGEXP '$q' OR version REGEXP '$q' ";
                return dbContextIAV.fcms.Where(f => f.device.ToLower().Contains(q.ToLower())
                || f.serial.ToLower().Contains(q.ToLower())
                || f.app_version.ToLower().Contains(q.ToLower())
                || f.os_version.ToLower().Contains(q.ToLower())
                ).Select(f => f.id).Distinct().Count();

            } else {
                //$query = "SELECT COUNT(DISTINCT f.id) FROM fcm f";
                return dbContextIAV.fcms.Select(f => f.id).Distinct().Count();
            }
        }


        public List<fcm> findAllByPage(int page, int limit, string q)
        {
            int offset = page - 1;

            if (!string.IsNullOrEmpty(q))
            {
                //$query = "SELECT DISTINCT * FROM fcm f WHERE device REGEXP '$q' OR serial REGEXP '$q' OR version REGEXP '$q' ORDER BY f.last_update DESC LIMIT $limit OFFSET $offset";
                return dbContextIAV.fcms.Where(f => f.device.ToLower().Contains(q.ToLower())
                || f.serial.ToLower().Contains(q.ToLower())
                || f.app_version.ToLower().Contains(q.ToLower())
                || f.os_version.ToLower().Contains(q.ToLower())
                ).Distinct().OrderByDescending(o => o.last_update).Skip(offset).Take(limit).ToList();

            }
            else
            {
                //$query = "SELECT DISTINCT * FROM fcm f ORDER BY f.last_update DESC LIMIT $limit OFFSET $offset";
                return dbContextIAV.fcms.Distinct().OrderByDescending(o => o.last_update).Skip(offset).Take(limit).ToList();
            }

        }

        public int allCountPlain()
        {
            //string query="SELECT COUNT(DISTINCT f.id) FROM fcm f";
            return dbContextIAV.fcms.Distinct().Count();
        }

        public string insertOne(fcm fcm)
        {
            /* TITIKSHA - to be done
            // checking security code
            if (!isset($this->_header['Security']) || $this->_header['Security'] != $this->conf->SECURITY_CODE){
            $m = array('status' => 'failed', 'msg' => 'Invalid security code', 'data' => null);
            $this->show_response($m);
                return;
            }
            */

            //$query = "SELECT f.* FROM fcm f WHERE f.serial='$serial' LIMIT 1";
            var count = dbContextIAV.fcms.Where(f => f.serial.ToLower() == fcm.serial.ToLower()).Count();

            try
            {
                if (count > 0)
                {
                    var fcmUpdate = dbContextIAV.fcms.Where(f => f.serial.ToLower() == fcm.serial.ToLower()).FirstOrDefault();
                    fcmUpdate.app_version = fcm.app_version;
                    fcmUpdate.device = fcm.device;
                    fcmUpdate.os_version = fcm.os_version;
                    fcmUpdate.regid = fcm.regid;
                    fcmUpdate.serial = fcm.serial;
                    fcmUpdate.created_at = fcm.created_at;
                    fcmUpdate.last_update = DateTime.Now.Ticks;
                    dbContextIAV.Entry(fcmUpdate).State = System.Data.Entity.EntityState.Modified;
                    dbContextIAV.SaveChanges();
                }
                else
                {
                    fcm.created_at = DateTime.Now.Ticks;
                    fcm.last_update = DateTime.Now.Ticks;
                    dbContextIAV.fcms.Add(fcm);
                    dbContextIAV.Entry(fcm).State = System.Data.Entity.EntityState.Added;
                    dbContextIAV.SaveChanges();
                }
            }
            catch
            {
                var fail = new { status = "failed", msg = "Notification failed!" };
                return JsonConvert.SerializeObject(fail);
            }

            var success = new { status = "success", msg = "Notification saved successfully!" };
            return JsonConvert.SerializeObject(success);
        }

        void IDisposable.Dispose()
        {

        }
    }
}