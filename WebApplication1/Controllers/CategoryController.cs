﻿
using IAV.Data.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace IAVAPI.Controllers
{
   
    public class CategoryController :  IDisposable
    {
        markeet_demoEntities dbContextIAV = new markeet_demoEntities();

        public List<IAV.Data.Models.category> findAllForClient()
        {
            //$query = "SELECT * FROM category c WHERE c.draft=0 ORDER BY c.priority ASC";
            return dbContextIAV.categories.Where(c => c.draft == false).OrderBy(c => c.priority).ToList();
        }

        public int countByDraftPlain(int i)
        {
            //string query = "SELECT COUNT(DISTINCT c.id) FROM category c WHERE c.draft=$i ";
            return dbContextIAV.categories.Where(s => s.draft == (i == 0 ? false : true)).Distinct().Count();
        }

        public List<category> getAllCategories()
        {
            //$query = "SELECT * FROM category c ORDER BY c.priority ASC";
            return dbContextIAV.categories.Include("product_category").OrderBy(c => c.name).ToList();
        }

        public List<IAV.Data.Models.category> getAllCategoryByProductId(int product_id)
        {
            //if ($this->get_request_method() != "GET") $this->response('', 406);
            //if (!isset($this->_request['product_id'])) $this->responseInvalidParam();
            //$product_id = (int)$this->_request['product_id'];
            //$this->show_response($this->getAllByProductIdPlain($product_id));

            return getAllByProductIdPlain(product_id);
        }

        public List<IAV.Data.Models.category> getAllByProductIdPlain(int product_id)
        {
            //$query = "SELECT DISTINCT c.* FROM category c WHERE c.id IN (SELECT pc.category_id FROM product_category pc WHERE pc.product_id=$product_id);";
            //return $this->db->get_list($query);

            var prodcats = dbContextIAV.product_category.Where(pc => pc.product_id == product_id);

            var cats = (from cat in dbContextIAV.categories
                       join pc in prodcats
                       on cat.id equals pc.category_id
                       select cat).ToList<IAV.Data.Models.category>();

            return cats;  

        }

        public IAV.Data.Models.category findOne(int id)
        {
            //$query = "SELECT distinct * FROM category c WHERE c.id=$id";
            return dbContextIAV.categories.Find(id);
        }

        public int allCountPlain(string q, int client)
        {
              //$query = "SELECT COUNT(DISTINCT c.id) FROM category c ";
		      //$keywordQuery = "(c.name REGEXP '$q' OR c.status REGEXP '$q' OR c.brief REGEXP '$q') ";
              //if ($client != 0){
		      //$query = $query. " WHERE c.draft <> 1 ";
              //if ($q != "") $query = $query. "AND ". $keywordQuery;
              //} else {
              //if ($q != "") $query = $query. "WHERE ". $keywordQuery;
              //}
              //return $this->db->get_count($query);
        
            List<IAV.Data.Models.category> categories = new List<IAV.Data.Models.category>();
            if (!string.IsNullOrEmpty(q))
            {
                if (client != 0)
                    categories = dbContextIAV.categories
                                .Where(c => c.name.ToLower().Contains(q.ToLower()) ||
                                c.brief.ToLower().Contains(q.ToLower())
                                || !c.draft)
                                .ToList();
                else
                    categories = dbContextIAV.categories
                                .Where(c => c.name.ToLower().Contains(q.ToLower()) ||
                                c.brief.ToLower().Contains(q.ToLower()))
                                .ToList();
            }
            else
            {
                categories = dbContextIAV.categories.Distinct().OrderByDescending(o => o.id).ToList();
            }
            return categories.Count;
        }

        public int allCount(string q)
        {
            //if ($this->get_request_method() != "GET") $this->response('', 406);
		    //$q = (isset($this->_request['q'])) ? ($this->_request['q']) : "";
		    //$client = (isset($this->_request['client'])) ? ((int)$this->_request['client']) : 0;
		    //$this->show_response_plain($this->allCountPlain($q, $client));
            return allCountPlain(q, 0);
        }

        public List<IAV.Data.Models.category> findAllByPage(int limit, int page, string q)
        {
            return findAllByPagePlain(limit, page, q, 0);
        }

        public List<IAV.Data.Models.category> findAllByPagePlain(int limit, int page, string q, int client)
        {
            //$query = "SELECT c.* FROM category c ";
		    //$keywordQuery = "(c.name REGEXP '$q' OR c.status REGEXP '$q' OR c.brief REGEXP '$q') ";
            //if ($client != 0){
		    //$query = $query. " WHERE c.draft <> 1 ";
            //if ($q != "") $query = $query. "AND ". $keywordQuery;
            //} else {
            //if ($q != "") $query = $query. "WHERE ". $keywordQuery;
            //}
		    //$query = $query. "ORDER BY c.id DESC LIMIT $limit OFFSET $offset ";
            //return $this->db->get_list($query);

            List<IAV.Data.Models.category> categories = new List<IAV.Data.Models.category>();
            int offset = page - 1;
            
            if (!string.IsNullOrEmpty(q))
            {
                if (client != 0)
                    categories = dbContextIAV.categories
                                .Where(c => c.name.ToLower().Contains(q.ToLower())
                                || c.brief.ToLower().Contains(q.ToLower())
                                || !c.draft)
                                .ToList().Distinct().OrderByDescending(o => o.id).Skip(offset).Take(limit).ToList();
                else
                    categories = dbContextIAV.categories
                                .Where(c => c.name.ToLower().Contains(q.ToLower())
                                || c.brief.ToLower().Contains(q.ToLower()))
                                .ToList().Distinct().OrderByDescending(o => o.id).Skip(offset).Take(limit).ToList();

            }
            else
            {
                categories = dbContextIAV.categories.Distinct().OrderByDescending(o => o.id).Skip(offset).Take(limit).ToList();
            }

          
            return categories;
        }

        public string insertOne(IAV.Data.Models.category category)
        {
            //if ($this->get_request_method() != "POST") $this->response('', 406);
		    //$data = json_decode(file_get_contents("php://input"), true);
            //if (!isset($data)) $this->responseInvalidParam();
		    //$column_names = array('name', 'icon', 'draft', 'brief', 'color', 'priority', 'created_at', 'last_update');
		    //$table_name = 'category';
		    //$pk = 'id';
		    //$resp = $this->db->post_one($data, $pk, $column_names, $table_name);
		    //$this->show_response($resp);
            category cat = new IAV.Data.Models.category();
            cat.created_at = category.created_at;
            cat.brief = category.brief;
            cat.draft = category.draft;
            cat.color = category.color;
            cat.last_update = category.last_update;
            cat.name = category.name;
            cat.icon = category.icon;
            cat.priority = category.priority;
            dbContextIAV.categories.Add(cat);
            dbContextIAV.SaveChanges();
            return "success";//dbContextIAV.categories.Where(pr => pr.id == dbContextIAV.categories.Max(p => p.id)).FirstOrDefault();
        }

        public string updateOne(int id, IAV.Data.Models.category category)
        {
            //if ($this->get_request_method() != "POST") $this->response('', 406);
		    //$data = json_decode(file_get_contents("php://input"), true);
            //if (!isset($data['id'])) $this->responseInvalidParam();
		    //$id = (int)$data['id'];
		    //$column_names = array('name', 'icon', 'draft', 'brief', 'color', 'priority', 'created_at', 'last_update');
		    //$table_name = 'category';
		    //$pk = 'id';
		    //$this->show_response($this->db->post_update($id, $data, $pk, $column_names, $table_name));

            try
            {
                category c = dbContextIAV.categories.Find(id);
                dbContextIAV.Entry(c).State = EntityState.Modified;

                c.brief = category.brief;
                c.draft = category.draft;
                c.last_update = category.last_update;
                c.created_at = category.created_at;
                c.name = category.name;
                c.color = category.color;
                c.icon = category.icon;
                c.priority = category.priority;

                dbContextIAV.SaveChanges();
                return "success";// c;
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                Console.Write(ex.StackTrace);
                return "fail";
            }
        }

        public string deleteOne(int id)
        {
            //if ($this->get_request_method() != "GET") $this->response('', 406);
            //if (!isset($this->_request['id'])) $this->responseInvalidParam();
		    //$id = (int)$this->_request['id'];
		    //$table_name = 'category';
		    //$pk = 'id';
		    //$this->show_response($this->db->delete_one($id, $pk, $table_name));
            try
            {
                category c = dbContextIAV.categories.Find(id);
                dbContextIAV.categories.Remove(c);
                dbContextIAV.SaveChanges();
                return "success";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }


        void IDisposable.Dispose()
        {

        }
    }
}