﻿
using IAV.Data.Models;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace IAVAPI.Controllers
{
    public class MerchantController : IDisposable
    {
        markeet_demoEntities dbContextIAV = new markeet_demoEntities();


        //public string getMerchans() 
        //{
        //    List<merchant> merchants = dbContextIAV.merchants.OrderBy(c => c.business_name).ToList();
        //    var response = new { status = "success", merchants = merchants };
        //    return JsonConvert.SerializeObject(response, Formatting.None,
        //    new JsonSerializerSettings()
        //    {
        //        ReferenceLoopHandling = ReferenceLoopHandling.Ignore
        //    });
            
        //}
        public string getMerchans(string searchString)
        {
            //$query = "SELECT * FROM category c WHERE c.draft=0 ORDER BY c.priority ASC";
            List<merchant> merchants = dbContextIAV.merchants.Where(c => c.business_name.Contains(searchString)).OrderBy(c => c.business_name).ToList();

            //dbContextIAV.product_merchant.FirstOrDefault().
            var response = new { status = "success", merchants = merchants };
            return JsonConvert.SerializeObject(response, Formatting.None,
            new JsonSerializerSettings()
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });
        }

        public string getProductsByMerchantId(int id)
        {


            if (dbContextIAV.product_merchant.Any(c => c.merchant_id == id.ToString()))
            {
                //var pm = dbContextIAV.product_merchant.Where(c => c.merchant_id == id.ToString()).OrderBy(c => c.merchant_id).ToList(); //calling data from product_merchant


                var promerch = (from pm1 in dbContextIAV.product_merchant

                                join p in dbContextIAV.products

                                on pm1.product_id equals p.id.ToString()

                                where pm1.merchant_id == id.ToString()
                                select new
                                {
                                    p.id,
                                    p.name,
                                    p.image,
                                    p.price,
                                    p.stock,
                                    p.draft,
                                    p.description,
                                    p.status,
                                    p.created_at,
                                    p.last_update
                                }).ToList();


                var response = new { status = "success", products = promerch };
                return JsonConvert.SerializeObject(response, Formatting.None,
                     new JsonSerializerSettings()
                     {
                         ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                     });

            }
            else
            {
                var response1 = new { status = "failed", products = "failed" };
                return JsonConvert.SerializeObject(response1, Formatting.None,
                    new JsonSerializerSettings()
                    {
                        ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                    });
            }

        }
        public string getCategoryByMerchantId(int id) 
        {

           // if (dbContextIAV.product_merchant.Any(c => c.merchant_id == id.ToString()))
           // { ExecuteStoreQuery
                int a=3;
               // string query = @" SELECT t3.*,SUM(T1.quantity_available)FROM Product_Merchant t1 INNER join (product_category t2 inner join category t3 on t2.category_id=t3.id)on t1.product_id = t2.product_id where t1.merchant_id=3 GROUP BY t3.id";
                string query = @"Select * from product_merchant";
                var getCategoryByMerchantId = dbContextIAV.Database.SqlQuery<string>(query);
                //new MySqlParameter("@a", a)

                        var response = new { status = "success", Category = getCategoryByMerchantId };
                        return JsonConvert.SerializeObject(response, Formatting.None,
                         new JsonSerializerSettings()
                         {
                             ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                         });


          


                //var getCategoryByMerchantId =  (from o in dbContextIAV.product_merchant
                //                               join d in dbContextIAV.product_category
                //                               on  d.product_id equals o.product_id
                //                               join f in dbContextIAV.categories
                //                               on f.id == d.category_id 
                //                               where o.merchant_id equals 3
                //                               select o).ToList();
                             // }
           

            //var response2 = new { status = "failed", products = "failed" };
            //return JsonConvert.SerializeObject(response2, Formatting.None,
            // new JsonSerializerSettings()
            // {
            //     ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            // });
        }

        public string getMerchants(decimal latitude, decimal longitude)
        {

            decimal constValue = Convert.ToDecimal(ConfigurationManager.AppSettings["constValue"].ToString());
            decimal latFrom = latitude - constValue;
            decimal latTo = latitude + constValue;
            decimal lonfFrom = longitude - constValue;
            decimal longTo = longitude + constValue;

            var merchants = (from m in dbContextIAV.merchants
                             where (m.latitude >= latFrom && m.latitude <= latTo) && (m.longitude >= lonfFrom && m.longitude <= longTo)
                             select new
                             {
                                m.mid,
                                m.business_name,
                                m.gstin,
                                m.tan,
                                m.cin,
                                m.signature,
                                m.address_line1,
                                m.address_line2,
                                m.pincode,
                                m.city,
                                m.state,
                                m.store_no,
                                m.phone_no,
                                m.latitude,
                                m.longitude,
                                m.store_picture
                                 
                             }).ToList();


            var response = new { status = "success", merchant = merchants };
            return JsonConvert.SerializeObject(response, Formatting.None,
                 new JsonSerializerSettings()
                 {
                     ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                 });
        
        }



        public string getMerchantsByProductId(int productId, decimal latitude, decimal longitude)
        {

            decimal constValue = Convert.ToDecimal(ConfigurationManager.AppSettings["constValue"].ToString());
            decimal latFrom = latitude - constValue;
            decimal latTo = latitude + constValue;
            decimal lonfFrom = longitude - constValue;
            decimal longTo = longitude + constValue;

            var promerch = (from pm1 in dbContextIAV.product_merchant

                            join p in dbContextIAV.products

                            on pm1.product_id equals p.id.ToString()

                            join m in dbContextIAV.merchants

                            on pm1.merchant_id equals m.mid.ToString()

                            where pm1.product_id == productId.ToString()
                            && (m.latitude >= latFrom && m.latitude <= latTo) && (m.longitude >= lonfFrom && m.longitude <= longTo)
                            select new
                            {
                                m.mid,
                                m.business_name,
                                m.gstin,
                                m.tan,
                                m.cin,
                                m.signature,
                                m.address_line1,
                                m.address_line2,
                                m.pincode,
                                m.city,
                                m.state,
                                m.store_no,
                                m.phone_no,
                                m.latitude,
                                m.longitude,
                                m.store_picture

                            }).Distinct().ToList();


            var response = new { status = "success", merchant = promerch };
            return JsonConvert.SerializeObject(response, Formatting.None,
                 new JsonSerializerSettings()
                 {
                     ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                 });
        }



        void IDisposable.Dispose()
        {

        }
    }
    
}
