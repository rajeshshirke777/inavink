﻿using IAV.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IAVAPI.Controllers
{
    public class ProductCategoryController : IDisposable
    {
        markeet_demoEntities dbContextIAV = new markeet_demoEntities();
        public string deleteInsertAll(int product_id, List<int> categories)
        {

            string status = string.Empty;
            try
            {
                //remove all categories of product
                var pclist = dbContextIAV.product_category.Where(item => item.product_id == product_id).ToList();
                foreach (var pcl in pclist)
                {
                    dbContextIAV.product_category.Remove(pcl);
                }

                //add new categories for the product
                foreach (var cid in categories)
                {
                    //var category = dbContextIAV.categories.Where(c => c.id == cid);
                    product_category pc = new product_category() { product_id = product_id, category_id = cid };
                    dbContextIAV.product_category.Add(pc);
                    dbContextIAV.SaveChanges();
                }
                status = "success";
            }
            catch (Exception ex)
            {
                status = ex.Message;
            }
            return status;
            
            
        }
      
        void IDisposable.Dispose()
        {

        }
    }
}