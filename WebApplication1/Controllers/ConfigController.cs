﻿using IAV.Data.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using IAV.Data.Models;
using System.Data.Entity.Validation;

namespace IAVAPI.Controllers
{
    
    public class ConfigController :  IDisposable
    {
        string constring = ConfigurationManager.ConnectionStrings["markeet_demoEntities"].ConnectionString;
        markeet_demoEntities dbContextIAV = new markeet_demoEntities();

        public int countByDraftPlain(int i)
        {
            //string query = "SELECT COUNT(DISTINCT c.id) FROM category c WHERE c.draft=$i ";
            return dbContextIAV.categories.Where(s => s.draft == (i == 0 ? false : true)).Distinct().Count();
        }

        public List<config> findAllArr()
        {
            //$query="SELECT * FROM config cn";
            return dbContextIAV.configs.ToList();

        }

        public List<config> findAllPlain()
        {
		    //string query="SELECT * FROM config cn";
            return dbContextIAV.configs.ToList();
        }

        public string updateAll(List<config> config)
        {
            var success = new { status = "success", msg = "Config saved successfully!" };
            var fail = new { status = "fail", msg = "Failed!" };

            try
            {
                foreach (config c in config)
                {
                    config configUpdate = dbContextIAV.configs.Where(co => co.code == c.code).FirstOrDefault();
                    configUpdate.value = c.value;
                    dbContextIAV.Entry(configUpdate).State = System.Data.Entity.EntityState.Modified;
                    dbContextIAV.SaveChanges();
                }               
                
                return JsonConvert.SerializeObject(success);
            }
            catch (DbEntityValidationException e)
            {
                return JsonConvert.SerializeObject(fail);
            }
            catch (Exception e)
            {
                return JsonConvert.SerializeObject(fail);
            }
        }

        void IDisposable.Dispose()
        {

        }
    }
}