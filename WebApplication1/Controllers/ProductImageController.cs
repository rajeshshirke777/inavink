﻿
using IAV.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IAVAPI.Controllers
{
    public class ProductImageController : IDisposable
    {
        markeet_demoEntities dbContextIAV = new markeet_demoEntities();

        public List<product_image> getAllProductImageByProductId(int product_id)
        {
            //      if ($this->get_request_method() != "GET") $this->response('', 406);
            //      if (!isset($this->_request['product_id']))$this->responseInvalidParam();
            //$product_id = (int)$this->_request['product_id'];
            //$this->show_response($this->findAllByProductIdPlain($product_id));
            return getAllProductImageByProductIdPlain(product_id);
        }

        public List<product_image> findAllByProductIdPlain(int product_id)
        {
		 //$query = "SELECT DISTINCT * FROM product_image i WHERE i.product_id=$product_id";
            return getAllProductImageByProductIdPlain(product_id);
        }


        public List<product_image> getAllProductImageByProductIdPlain(int product_id)
        {
            //$query = "SELECT DISTINCT * FROM product_image i WHERE i.product_id=$product_id";
            //return $this->db->get_list($query);
            return dbContextIAV.product_image.Where(pi => pi.product_id == product_id).Distinct().ToList();
        }

        public string insertAll(int product_id, List<string> images)
        {
            string status = string.Empty;
            try
            {
                //remove all images of product
                var pclist = dbContextIAV.product_image.Where(item => item.product_id == product_id).ToList();
                foreach (var pcl in pclist)
                {
                    dbContextIAV.product_image.Remove(pcl);
                }

                //add new images for the product
                foreach (var name in images)
                {
                    product_image pi = new product_image() { product_id = product_id, name = name };
                    dbContextIAV.product_image.Add(pi);
                    dbContextIAV.SaveChanges();
                }
                status = "success";
            }
            catch (Exception ex)
            {
                status = ex.Message;
            }
            return status;


        }

        void IDisposable.Dispose()
        {

        }
    }
}