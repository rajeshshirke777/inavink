﻿
using IAV.Data.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace IAVAPI.Controllers
{

    public class ProductController : IDisposable
    {
        markeet_demoEntities dbContextIAV = new markeet_demoEntities();

        public List<product> findAllByPagePlainForClient(int limit, int offset, string q, int category_id)
        {
            List<product> products = findAllByPageForClient(q, category_id);
            List<product> returnProd = products.Skip(offset).Take(limit).ToList();

            return returnProd;
        }
        
        public int allCountPlainForClient(string q, int category_id)
        {
            List<product> products = findAllByPageForClient(q, category_id);
            return products.Count;
        }

        public List<product> findAllByPageForClient(string q, int category_id)
        {
            List<product> products = new List<product>();

            if (!string.IsNullOrEmpty(q))
            {
                //var filteredQuery = main.Where(m => m.name.ToLower().Contains(q.ToLower()) || m.status.ToLower().Contains(q.ToLower()) || m.description.ToLower().Contains(q.ToLower()));
                if (category_id > 0)
                {
                    List<product_category> res = dbContextIAV.product_category.Where(c => c.id == category_id).ToList();
                    if (!string.IsNullOrEmpty(q))
                    {
                        var prods = (from pc in res
                                     join prod in dbContextIAV.products
                                     on pc.product_id equals prod.id
                                     where prod.draft == false
                                     select prod).ToList();

                        products = prods
                       .Where(p => p.name.ToLower().Contains(q.ToLower()) ||
                           p.status.ToLower().Contains(q.ToLower()) ||
                           p.description.ToLower().Contains(q.ToLower()))
                           .Distinct().OrderByDescending(o => o.name).ToList();

                    }
                }
                else
                {
                    products = dbContextIAV.products
                        .Where(p => p.draft == false && (p.name.ToLower().Contains(q.ToLower()) ||
                            p.status.ToLower().Contains(q.ToLower()) ||
                            p.description.ToLower().Contains(q.ToLower())
                            )).Distinct().OrderByDescending(o => o.name).ToList();
                }

            }
            else
            {
                if (category_id > 0)
                {
                    List<product_category> res = dbContextIAV.product_category.Where(c => c.id == category_id).ToList();
                    products = (from pc in res
                                join prod in dbContextIAV.products
                                on pc.product_id equals prod.id
                                where prod.draft == false
                                select prod).OrderByDescending(o => o.name).ToList();
                }
                else
                {
                    products = dbContextIAV.products.Where(prod => prod.draft == false).Distinct().OrderByDescending(o => o.name).ToList();
                }

            }

            return products;
        }

        public int countByStatusPlain(string status)
        {
            //string query = "SELECT COUNT(DISTINCT p.id) FROM product p WHERE p.status='$status' ";
            return dbContextIAV.products.Where(s => s.status == status).Distinct().Count();
        }

        public int countByDraftPlain(int i)
        {
            //string query = "SELECT COUNT(DISTINCT p.id) FROM product p WHERE p.draft=$i ";
            return dbContextIAV.products.Where(s => s.draft == (i == 0 ? false : true)).Distinct().Count();
        }

        public List<product> findAllByPage(int limit, int page, string q, int category_id)
        {
            return findAllByPagePlain(limit, page, q, category_id);
        }

        public List<product> findAllByPagePlain(int limit, int page, string q, int category_id)
        {
            List<product> products = new List<product>();
            int offset = page - 1;

            if (!string.IsNullOrEmpty(q))
            {
                if (category_id == -1)
                {
                    products = dbContextIAV.products.Include("product_image").Include("product_category")
                        .Where(p => p.name.ToLower().Contains(q.ToLower()) ||
                            p.status.ToLower().Contains(q.ToLower()) ||
                            p.description.ToLower().Contains(q.ToLower())
                            ).Distinct().OrderByDescending(o => o.id).Skip(offset).Take(limit).ToList();
                }
                else
                {
                    List<product_category> res = dbContextIAV.product_category.Where(c => c.id == category_id).ToList();
                    if (!string.IsNullOrEmpty(q))
                    {
                        var prods = (from pc in res
                                    join prod in dbContextIAV.products
                                    on pc.product_id equals prod.id
                                    select prod).ToList();

                        products = prods
                       .Where(p => p.name.ToLower().Contains(q.ToLower()) ||
                           p.status.ToLower().Contains(q.ToLower()) ||
                           p.description.ToLower().Contains(q.ToLower()))
                           .Distinct().OrderByDescending(o => o.id).Skip(offset).Take(limit).ToList();

                    }
                   
                }
            }
            else if (category_id > -1)
            {
                //var cats = dbContextIAV.product_category.Where(c => c.category_id == category_id);
                //products = dbContextIAV.products
                //        .Where(p => p.id == cats)
                //            .Distinct().OrderByDescending(o => o.id).ToList();

                List<product_category> res = dbContextIAV.product_category.Where(c => c.id == category_id).ToList();

                if (!string.IsNullOrEmpty(q))
                {
                     products = (from pc in res
                     join prod in dbContextIAV.products
                     on pc.product_id equals prod.id
                     select prod).ToList();
                }
            }
            else
            {
                products = dbContextIAV.products.Include("product_image").Include("product_category").Distinct().OrderByDescending(o => o.id).Skip(offset).Take(limit).ToList();
            }
            return products;
        }

        public product findOnePlan(int id)
        {
            //$query = "SELECT * FROM product p WHERE p.id=$id LIMIT 1";
            return dbContextIAV.products.Find(id);
        }

        public product findOnePlain(int id)
        {
            //$query = "SELECT * FROM product p WHERE p.id=$id LIMIT 1";
            return findOnePlan(id);
        }

        public List<product> findAll()
        {
            //$query = "SELECT * FROM product p WHERE p.id=$id LIMIT 1";
            return dbContextIAV.products.ToList();
        }

        public product findOne(int id)
        {
            //if ($this->get_request_method() != "GET") $this->response('', 406);
            //if (!isset($this->_request['id'])) $this->responseInvalidParam();
            //$id = (int)$this->_request['id'];
            //$this->show_response($this->findOnePlain($id));

            return findOnePlan(id);
        }

        public int allCountPlain(string q, int category_id)
        {
            //   $query = "SELECT COUNT(DISTINCT p.id) FROM product p ";
            //$keywordQuery = "(p.name REGEXP '$q' OR p.status REGEXP '$q' OR p.description REGEXP '$q') ";
            //   if ($category_id != -1){
            //   $query = $query. ", product_category pc WHERE pc.product_id=p.id AND pc.category_id=$category_id ";
            //       if ($q != "") $query = $query. "AND ". $keywordQuery;
            //   } else {
            //       if ($q != "") $query = $query. "WHERE ". $keywordQuery;
            //   }
            //   return $this->db->get_count($query);

            List<product> products = new List<product>();
            if (!string.IsNullOrEmpty(q))
            {
                if (category_id == -1)
                {
                    products = dbContextIAV.products
                        .Where(p => p.name.ToLower().Contains(q.ToLower()) ||
                            p.status.ToLower().Contains(q.ToLower()) ||
                            p.description.ToLower().Contains(q.ToLower())
                            ).Distinct().OrderByDescending(o => o.id).ToList();
                }
                else
                {
                    List<product_category> res = dbContextIAV.product_category.Where(c => c.id == category_id).ToList();
                    if (!string.IsNullOrEmpty(q))
                    {
                        var prods = (from pc in res
                                     join prod in dbContextIAV.products
                                     on pc.product_id equals prod.id
                                     select prod).ToList();

                        products = prods
                       .Where(p => p.name.ToLower().Contains(q.ToLower()) ||
                           p.status.ToLower().Contains(q.ToLower()) ||
                           p.description.ToLower().Contains(q.ToLower()))
                           .Distinct().OrderByDescending(o => o.id).ToList();

                    }

                }
            }
            else if (category_id > -1)
            {
                //var cats = dbContextIAV.product_category.Where(c => c.category_id == category_id);
                //products = dbContextIAV.products
                //        .Where(p => p.id == cats)
                //            .Distinct().OrderByDescending(o => o.id).ToList();

                List<product_category> res = dbContextIAV.product_category.Where(c => c.id == category_id).ToList();

                if (!string.IsNullOrEmpty(q))
                {
                    products = (from pc in res
                                join prod in dbContextIAV.products
                                on pc.product_id equals prod.id
                                select prod).ToList();
                }
            }
            else
            {
                products = dbContextIAV.products.Distinct().OrderByDescending(o => o.id).ToList();
            }
            return products.Count;
        }

        public int allCount(string q, int category_id)
        {
            //if ($this->get_request_method() != "GET") $this->response('', 406);
            //$q = (isset($this->_request['q'])) ? ($this->_request['q']) : "";
            //$category_id = isset($this->_request['category_id']) ? ((int)$this->_request['category_id']) : -1;
            //$this->show_response_plain($this->allCountPlain($q, $category_id));
            return allCountPlain(q, category_id);
        }

        public product insertOne(product product)
        {
            //if ($this->get_request_method() != "POST") $this->response('', 406);
            //$data = json_decode(file_get_contents("php://input"), true);
            //if (!isset($data)) $this->responseInvalidParam();
            //$column_names = array('name', 'image', 'price', 'stock', 'draft', 'description', 'status', 'created_at', 'last_update');
            //$table_name = 'product';
            //$pk = 'id';
            //$resp = $this->db->post_one($data, $pk, $column_names, $table_name);
            //$this->show_response($resp);
            product pro = new product();
            pro.created_at = product.created_at;
            pro.description = product.description;
            pro.draft = product.draft;
            pro.image = product.image;
            pro.last_update = product.last_update;
            pro.name = product.name;
            pro.brand_name = product.brand_name;
            pro.price = product.price;
            pro.status = product.status;
            pro.stock = product.stock;
            dbContextIAV.products.Add(pro);
            dbContextIAV.SaveChanges();
            return dbContextIAV.products.Where(pr => pr.id == dbContextIAV.products.Max(p => p.id)).FirstOrDefault();
        }

        public product updateOne(int id, product product)
        {
            //          if ($this->get_request_method() != "POST") $this->response('', 406);
            //$data = json_decode(file_get_contents("php://input"), true);
            //          if (!isset($data['id'])) $this->responseInvalidParam();
            //$id = (int)$data['id'];
            //$column_names = array('name', 'image', 'price', 'stock', 'draft', 'description', 'status', 'created_at', 'last_update');
            //$table_name = 'product';
            //$pk = 'id';
            //$this->show_response($this->db->post_update($id, $data, $pk, $column_names, $table_name));

            try
            {
                product p = dbContextIAV.products.Find(id);
                dbContextIAV.Entry(p).State = EntityState.Modified;

                p.description = product.description;
                p.draft = product.draft;
                p.last_update = product.last_update;
                p.created_at = product.created_at;
                p.name = product.name;
                p.brand_name = product.brand_name;
                p.price = product.price;
                p.status = product.status;
                p.stock = product.stock;
                
                dbContextIAV.SaveChanges();
                return p;
            }
            catch(Exception ex)
            {
                Console.Write(ex.Message);
                Console.Write(ex.StackTrace);
                return null;
            }
        }

        public string deleteOne(int id)
        {
            //      if ($this->get_request_method() != "GET") $this->response('', 406);
            //      if (!isset($this->_request['id'])) $this->responseInvalidParam();
            //$id = (int)$this->_request['id'];
            //$table_name = 'product';
            //$pk = 'id';
            //$this->show_response($this->db->delete_one($id, $pk, $table_name));
            try
            {
                product p = dbContextIAV.products.Find(id);
                dbContextIAV.products.Remove(p);
                dbContextIAV.SaveChanges();
                return "success";
            }
            catch(Exception ex)
            {
                return ex.Message;
            }
        }

        void IDisposable.Dispose()
        {

        }
    }
}