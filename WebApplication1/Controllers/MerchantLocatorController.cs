﻿using IAV.Data.Models;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace IAVAPI.Controllers
{
    [Route("api/MerchantLocator")]

    public class MerchantLocatorController : ApiController
    {
       
        string constring = ConfigurationManager.ConnectionStrings["markeet_demoEntities"].ConnectionString;
        markeet_demoEntities dbContextIAV = new markeet_demoEntities();
        public MerchantLocatorController()
        { }

        // GET api/<controller>
        /*
        public string Get()
        {
            using (MySqlConnection conn = new MySqlConnection(constring))
            {
                try
                {
                    MySqlCommand cmd = new MySqlCommand("getNearestMerchants", conn);
                    conn.Open();
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("product_id", 133);
                    cmd.Parameters.AddWithValue("latitude", "123");
                    cmd.Parameters.AddWithValue("longitude", "123");
                    using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
                    {
                        using (DataTable dt = new DataTable())
                        {
                            adp.Fill(dt);
                            return JsonConvert.SerializeObject(dt);
                        }
                    }

                }
                catch (Exception ex)
                {
                    conn.Close();
                    return ex.Message;
                }
            }
        }
        */

        /*
        // GET: api/MerchantLocator/5
        public string Get(int id)
        {
            return "value";
        }
        */
        // GET: api/MerchantLocator?product_id=131&latitude=90.13&longitude=90.12

        //[Route("GetNearByMerchantStores/{product_id=id}/{latitude=lat}/{longitude=lon}")]
        //[ActionName("GetNearByMerchantStores")]
        // GET: api/MerchantLocator?product_id=133&latitude="19.2092"&longitude="73.1610"

        [HttpGet]
        public JArray Get(string product_name, string latitude, string longitude)
        {
            
            var empDetails = dbContextIAV.getNearestMerchants(product_name, latitude, longitude).ToList();
            string jsonstr = JsonConvert.SerializeObject(empDetails);
            JArray jobj = JArray.Parse(jsonstr);
            return jobj;
        }

        // POST: api/MerchantLocator
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/MerchantLocator/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/MerchantLocator/5
        public void Delete(int id)
        {
        }
    }
}
