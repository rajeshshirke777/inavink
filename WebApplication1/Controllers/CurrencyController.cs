﻿using IAV.Data.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace IAVAPI.Controllers
{
   
    public class CurrencyController :  IDisposable
    {
        markeet_demoEntities dbContextIAV = new markeet_demoEntities();

        public List<currency> findAll()
        {
            return dbContextIAV.currencies.Distinct().ToList();
        }

        void IDisposable.Dispose()
        {

        }
    }
}