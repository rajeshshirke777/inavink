﻿using IAV.Data.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace IAVAPI.Controllers
{
    
    public class AppVersionController : IDisposable
    {
        string constring = ConfigurationManager.ConnectionStrings["markeet_demoEntities"].ConnectionString;
        markeet_demoEntities dbContextIAV = new markeet_demoEntities();

        public int countActiveVersion()
        {
            //stringquery="SELECT COUNT(DISTINCT a.id) FROM app_version a WHERE a.active = 1 ;";
            return dbContextIAV.app_version.Where(s => s.active == true).Distinct().Count();
        }

        public int countInactiveVersion()
        {
            //stringquery="SELECT COUNT(DISTINCT a.id) FROM app_version a WHERE a.active = 0 ;";
            return dbContextIAV.app_version.Where(s => s.active == false).Distinct().Count();
        }

        public int allCount(string q)
        {

            if (!string.IsNullOrEmpty(q))
            {
                //$query = "SELECT COUNT(DISTINCT a.id) FROM app_version a WHERE version_code REGEXP '$q' OR version_name REGEXP '$q' ";
                return dbContextIAV.app_version.Where(a => a.version_code.ToString().ToLower().Contains(q.ToLower())
                || a.version_name.ToLower().Contains(q.ToLower())
                ).Select(a => a.id).Distinct().Count();

            }
            else
            {
                //$query = "SELECT COUNT(DISTINCT a.id) FROM app_version a";
                return dbContextIAV.app_version.Select(a => a.id).Distinct().Count();
            }
        }

        public List<app_version> findAllByPage(int page, int limit, string q)
        {
            int offset = page - 1;
            if (!string.IsNullOrEmpty(q)) {
                //$query = "SELECT a.* FROM app_version a WHERE version_code REGEXP '$q' OR version_name REGEXP '$q' ORDER BY a.id DESC LIMIT $limit OFFSET $offset";
                return dbContextIAV.app_version.Where(a => a.version_code.ToString().ToLower().Contains(q.ToLower())
                    || a.version_name.ToLower().Contains(q.ToLower())
                    ).Distinct().OrderByDescending(o => o.id).Skip(offset).Take(limit).ToList();
            } else {
                //$query = "SELECT a.* FROM app_version a ORDER BY a.id DESC LIMIT $limit OFFSET $offset";
                return dbContextIAV.app_version.Distinct().OrderByDescending(o => o.id).Skip(offset).Take(limit).ToList();
            }
		
        }

        public app_version findOne(long id)
        {
            return findOnePlain(id);
        }

        public app_version findOnePlain(long id)
        {
            //$query = "SELECT * FROM app_version a WHERE a.id=$id LIMIT 1";
            return dbContextIAV.app_version.Where(a => a.id == id).FirstOrDefault();
        }

        public string insertOne(app_version app)
        {
            dbContextIAV.app_version.Add(app);
            try
            {
                dbContextIAV.Entry(app).State = System.Data.Entity.EntityState.Added;
                dbContextIAV.SaveChanges();
                var success = new { status = "success", msg = "added app_version successfully!" };
                return JsonConvert.SerializeObject(success);
            }
            catch (Exception ex)
            {
                var fail = new { status = "failed", msg = "add app_version failed!" };
                return JsonConvert.SerializeObject(fail);
            }
        }

        public string updateOne(app_version app)
        {
            if (app.active == false && countActiveVersion() <= 1){
                var fail = new { status = "failed", msg = "Ops, At least there is one active app version", data = (app_version)null };
                return JsonConvert.SerializeObject(fail);
            }

            app_version appUpdate = findOnePlain(app.id);
            appUpdate.active = app.active;
            appUpdate.version_code = app.version_code;
            appUpdate.version_name = app.version_name;

            try
            {
                dbContextIAV.Entry(appUpdate).State = System.Data.Entity.EntityState.Modified;
                dbContextIAV.SaveChanges();
                var success = new { status = "success", msg = "modified app_version successfully!" };
                return JsonConvert.SerializeObject(success);
            }
            catch (Exception ex)
            {
                var fail = new { status = "failed", msg = "modified app_version failed!" };
                return JsonConvert.SerializeObject(fail);
            }
        }


        public string deleteOne(int id)
        {
            app_version app = findOnePlain(id);
            if (app.active == true && countActiveVersion() <= 1)
            {
                var fail = new { status = "failed", msg = "Ops, At least there is one active app version", data = (app_version)null };
                return JsonConvert.SerializeObject(fail);
            }
            else
            {
                dbContextIAV.app_version.Remove(app);
                try
                {
                    dbContextIAV.Entry(app).State = System.Data.Entity.EntityState.Deleted;
                    dbContextIAV.SaveChanges();
                    var success = new { status = "success", msg = "deleted app_version successfully!" };
                    return JsonConvert.SerializeObject(success);
                }
                catch (Exception ex)
                {
                    var fail = new { status = "failed", msg = "delete app_version failed!" };
                    return JsonConvert.SerializeObject(fail);
                }
            }
        }

        void IDisposable.Dispose()
        {

        }
    }
}