﻿using IAV.Data.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft;
using Newtonsoft.Json.Linq;

using IAV.Data.Models;

namespace IAVAPI.Controllers
{
    [Route("api/Request")]

    public class RequestController : ApiController
    {
        #region    dashboard 
        [Route("api/Request/getDashboardProduct")]
        [HttpGet]
        public dashboardModel getDashboardProduct()
        {
            dashboardModel d = new dashboardModel();
            using (ProductController p = new ProductController())
            using (ProductOrderController po = new ProductOrderController())
            using (CategoryController c = new CategoryController())
            {
                d.order.waiting = po.countByStatusPlain("WAITING");
                d.order.processed = po.countByStatusPlain("PROCESSED");
                d.order.total = d.order.waiting + d.order.processed;

                d.product.published = p.countByDraftPlain(0);
                d.product.draft = p.countByDraftPlain(1);
                d.product.ready_stock = p.countByStatusPlain("READY STOCK");
                d.product.out_of_stock = p.countByStatusPlain("OUT OF STOCK");
                d.product.suspend = p.countByStatusPlain("SUSPEND");

                d.category.published = c.countByDraftPlain(0);
                d.category.draft = c.countByDraftPlain(1);

            }
            return d;
        }

        [Route("api/Request/getDashboardOthers")]
        [HttpGet]
        public dashboardModel getDashboardOthers()
        {
           
            dashboardModel d = new dashboardModel();
            using (NewsInfoController n = new NewsInfoController())
            using (AppVersionController a = new AppVersionController())
            using (ConfigController c = new ConfigController())
            using (FcmController f = new FcmController())
            {
                d.news.featured = n.countFeaturedPlain();
                d.news.published = n.countByDraftPlain(0);
                d.news.draft = n.countByDraftPlain(1);

                d.app.inactive = a.countInactiveVersion();
                d.app.active = a.countActiveVersion();

                List<config> setting_result = c.findAllPlain();

                d.setting.currency = getValue(setting_result, "CURRENCY");
                d.setting.tax = Convert.ToInt32(getValue(setting_result, "TAX"));
                d.setting.featured_news = Convert.ToInt32(getValue(setting_result, "FEATURED_NEWS"));

                d.notification.users = f.allCountPlain();

            }
            return d;
        }

        #endregion

        #region    currency 
        [Route("api/Request/getAllCurrency")]
        [HttpGet]
        public string getAllCurrency()
        {
            using (CurrencyController c = new CurrencyController())
            {
                List<currency> curr = c.findAll();
                return JsonConvert.SerializeObject(curr, Formatting.None,
                        new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                        });
            }
        }

        #endregion

        #region    order 
        [Route("api/Request/getAllProductOrderCount")]
        [HttpGet]
        public int getAllProductOrderCount()
        {
            using (ProductOrderController po = new ProductOrderController())
            {
                return po.allCount();
            }
        }

        [Route("api/Request/getAllProductOrderByPage")]
        [HttpGet]
        public string getAllProductOrderByPage(int page = 1, int limit = 10, string q = "")
        {
            using (ProductOrderController po = new ProductOrderController())
            {
                List<IAV.Data.Models.product_order> order = po.findAllByPage(page, limit, q);
                return JsonConvert.SerializeObject(order, Formatting.None,
                        new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                        });
            }
            
        }

        [Route("api/Request/getAllProductOrderDetailByOrderId")]
        [HttpGet]
        public string getAllProductOrderDetailByOrderId(int order_id)
        {
            using (ProductOrderDetailsController pod = new ProductOrderDetailsController())
            {
                List<IAV.Data.Models.product_order_detail> orderDetial = pod.findAllByOrderId(order_id);
                return JsonConvert.SerializeObject(orderDetial, Formatting.None,
                        new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                        });
            }

        }

        [Route("api/Request/insertOneProductOrder")]
        [HttpPost]
        public string insertOneProductOrder(JObject objData)
        {

            dynamic jsonData = objData;
            
            JObject json_order = jsonData;
            IAV.Data.Models.product_order order = json_order.ToObject<IAV.Data.Models.product_order>();

            using (ProductOrderController pod = new ProductOrderController())
            {
                return pod.insertOne(order);
            }
        }

        [Route("api/Request/insertAllProductOrderDetail")]
        [HttpPost]
        //[HttpGet]
        public string insertAllProductOrderDetail(JArray objData)
        {
            //string a = "[{'id':32,'order_id':14,'product_id':125,'product_name':'Samsung Galaxy J3 Pro','amount':1,'price_item':7990.0,'created_at':1502910082120,'last_update':1502910082120,'product_order':{'id':14,'code':'KP43001WR','buyer':'test1','address':'dfn sdlkf est','email':'9892413631','shipping':'FEDEX','date_ship':1502910027587,'phone':'amey.thakoor@gmail.com','comment':null,'status':'WAITING','total_fees':658.0,'tax':0.0,'created_at':1502910027587,'last_update':1512378899820,'product_order_detail':[]}},{'id':0,'order_id':14,'product_id':133,'product_name':'Unistuff Tempered Glass Guard for Motorola Moto E4 Plus','amount':1,'price_item':309.0,'created_at':1512379072746,'last_update':1512379072746,'product_order':null}]";
            //List<product_order_detail> order = JsonConvert.DeserializeObject<List<product_order_detail>>(a);
            List<IAV.Data.Models.product_order_detail> order = objData.ToObject<List<IAV.Data.Models.product_order_detail>>();

            using (ProductOrderDetailsController pod = new ProductOrderDetailsController())
            {
                return pod.deleteInsertAll(order);
            }
        }

        [Route("api/Request/updateOneProductOrder")]
        [HttpPost]
        public string updateOneProductOrder(JObject objData)
        {
            
            dynamic jsonData = objData;
            
            JObject json_order = jsonData;
            IAV.Data.Models.product_order order = json_order.ToObject<IAV.Data.Models.product_order>();
           
            using (ProductOrderController pod = new ProductOrderController())
            {
                return pod.updateOne(order);
            }           
        }


        [Route("api/Request/deleteOneProductOrder")]
        [HttpGet]
        public string deleteOneProductOrder(int order_id)
        {
            using (ProductOrderController pod = new ProductOrderController())
            {
                return pod.deleteOne(order_id);
            } 

        }

        [Route("api/Request/getOneProductOrder")]
        [HttpGet]
        public string getOneProductOrder(int order_id)
        {
            IAV.Data.Models.product_order o; 
            using (ProductOrderController pod = new ProductOrderController())
            {
                o = pod.findOne(order_id);
            }
            return JsonConvert.SerializeObject(o, Formatting.None,
                        new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                        });
        }

        [Route("api/Request/processProductOrder")]
        [HttpPost]
        public processOrderModel processProductOrder(JObject objData)
        {
            //JObject objData = JObject.Parse("{'id':1,'product_order':{'id':1,'code':null,'buyer':null,'address':null,'email':null,'shipping':null,'date_ship':0,'phone':null,'comment':null,'status':null,'total_fees':0.0,'tax':0.0,'created_at':0,'last_update':0,'product_order_detail':[{'id':1,'order_id':1,'product_id':125,'product_name':'Samsung Galaxy J3 Pro','amount':1,'price_item':7990.0,'created_at':1502910082120,'last_update':1502910082120,'product_order':{'id':1,'code':'KP43001WR','buyer':'test','address':'dfn sdlkf est','email':'9892413631','shipping':'FEDEX','date_ship':1502910027587,'phone':'amey.thakoor@gmail.com','comment':null,'status':'WAITING','total_fees':7990.0,'tax':0.0,'created_at':1502910027587,'last_update':1502910027587,'product_order_detail':[]}}]},'product_order_detail':{'id':1,'order_id':0,'product_id':0,'product_name':null,'amount':0,'price_item':0.0,'created_at':0,'last_update':0,'product_order':{'id':1,'code':'KP43001WR','buyer':'test','address':'dfn sdlkf est','email':'9892413631','shipping':'FEDEX','date_ship':1502910027587,'phone':'amey.thakoor@gmail.com','comment':null,'status':'WAITING','total_fees':7990.00,'tax':0.0,'created_at':1502910027587,'last_update':1502910027587,'product_order_detail':[{'id':1,'order_id':1,'product_id':125,'product_name':'Samsung Galaxy J3 Pro','amount':1,'price_item':7990.0,'created_at':1502910082120,'last_update':1502910082120,'product_order':null}]}}}");
            dynamic jsonData = objData;
            string json_id = jsonData.id;
            JObject json_order = jsonData.product_order;
            JObject json_order_detail = jsonData.product_order_detail;

            int id = Convert.ToInt32(json_id);
            product_order order = jsonData.product_order.ToObject<IAV.Data.Models.product_order>();
            product_order_detail order_detail = jsonData.product_order_detail.ToObject<IAV.Data.Models.product_order_detail>();

            List<IAV.Data.Models.product_order_detail> o = order.product_order_detail.ToList();
            using (ProductOrderController pod = new ProductOrderController())
            {
                return pod.processOrder(id, order, o);
            }

        }

        #endregion

        #region   USER TRANSACTION 
        [Route("api/Request/checkAuthorization")]
        [HttpGet]
        public bool checkAuthorization(string token)
        {
            using (UserController u = new UserController())
            {
                return u.checkAuthorization(token);
            }

        }

        [Route("api/Request/insertOneUser")]
        [HttpPost]
        public string insertOneUser(JObject objData)
        {
            dynamic jsonData = objData;
            user user = jsonData.ToObject<IAV.Data.Models.user>();
            //string jsonData = "{'id':0,'name':'Admin User1','username':'admin_user1','email':'users @mail.com','password':'admin'}";
            //user user = JsonConvert.DeserializeObject<user>(jsonData);
            using (UserController u = new UserController())
            {
                return u.insertOne(user);
            }

        }

        [Route("api/Request/updateOneUser")]
        [HttpPost]
        public string updateOneUser(JObject objData)
        {
            //string jsonData = @"{'id':1,'name':'Admin User','username':'admin_user','email':'user @mail.com','password':' * ****'}";
            //user user = JsonConvert.DeserializeObject<user>(jsonData);
            dynamic jsonData = objData;
            IAV.Data.Models.user user = jsonData.ToObject<IAV.Data.Models.user>();
            using (UserController u = new UserController())
            {
                return u.updateOne(user);
                
            }
        }

        
        [Route("api/Request/getOneUser")]
        [HttpGet]
        public string getOneUser(int id)
        {
            using (UserController u = new UserController())
            {
                IAV.Data.Models.user usr = u.findOne(id);
                return JsonConvert.SerializeObject(usr, Formatting.None,
                        new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                        });
            }
        }

        #endregion

        #region TABLE CONFIG TRANSACTION 
        [Route("api/Request/getAllConfig")]
        [HttpGet]
        public string getAllConfig()
        {
            using (ConfigController c = new ConfigController())
            {
                List<config> configs = c.findAllPlain();
                return JsonConvert.SerializeObject(configs, Formatting.None,
                        new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                        });
            }

        }

        [Route("api/Request/updateAllConfig")]
        [HttpPost]
        //[HttpGet]
        public string updateAllConfig(JArray objData)
        {
            //string a = @"[{'code':'CURRENCY','value':'INR'},{'code':'FEATURED_NEWS','value':'5'},{'code':'LOCATION_ACCURACY','value':'0.0001'},{'code':'SHIPPING','value':'[]'},{'code':'TAX','value':'1'}]";
            //List<config> jsonData = JsonConvert.DeserializeObject<List<config>>(a);

            List<config> con = objData.ToObject<List<config>>();
            using (ConfigController c = new ConfigController())
            {
                return c.updateAll(con);
            }
        }

        private string getValue(List<config> data, string code)
        {
            foreach (config c in data)
            {
                if (c.code == code)
                {
                    return c.value;
                }
            }
            return string.Empty;
        }

        #endregion

        #region products

        [Route("api/Request/getAllProductByPage")]
        [HttpGet]
        public string getAllProductByPage(int page = 1, int limit = 20, string q = "", int category_id = -1)
        {
            using (ProductController p = new ProductController())
            {
                List<product> products = p.findAllByPage(limit, page, q, category_id);
                return JsonConvert.SerializeObject(products, Formatting.None,
                        new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                        });
            }
        }

        [Route("api/Request/getAllProductCount")]
        [HttpGet]
        public string getAllProductCount(string q, int category_id)
        {
            using (ProductController p = new ProductController())
            {
                string count = p.allCount(q, category_id).ToString();
                return JsonConvert.SerializeObject(count, Formatting.None,
                        new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                        });
            }
        }

        [Route("api/Request/deleteOneProduct")]
        [HttpGet]
        public string deleteOneProduct(int id)
        {
            using (ProductController p = new ProductController())
            {
                string status = p.deleteOne(id);
                //string status = "fail";
                return JsonConvert.SerializeObject(status, Formatting.None,
                        new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                        });
            }
        }

        [Route("api/Request/getOneProduct")]
        [HttpGet]
        public string getOneProduct(int id)
        {
            using (ProductController p = new ProductController())
            {
                product product = p.findOne(id);
                //string status = "fail";
                return JsonConvert.SerializeObject(product, Formatting.None,
                        new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                        });
            }
        }

        [Route("api/Request/updateOneProduct")]
        [HttpPost]
        public string updateOneProduct(JObject objData)
        {
            using (ProductController p = new ProductController())
            {
                dynamic jsonData = objData;
                JObject productJson = jsonData.product;
                string idJson = jsonData.id;
                
                product product = productJson.ToObject<product>();
                int id = Convert.ToInt32(idJson);
                product pro = p.updateOne(id, product);

                //status = "fail";
                return JsonConvert.SerializeObject(pro, Formatting.None,
                        new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                        });
            }
        }

        [Route("api/Request/insertOneProduct")]
        [HttpPost]
        public string insertOneProduct(JObject objData)
        {
            using (ProductController p = new ProductController())
            {
                dynamic jsonData = objData;
                product product = jsonData.ToObject<product>();
                product pro = p.insertOne(product);

                return JsonConvert.SerializeObject(pro, Formatting.None,
                        new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                        });
            }
        }

        #endregion

        #region category

        [Route("api/Request/getAllCategory")]
        [HttpGet]
        public string getAllCategory()
        {
            using (CategoryController c = new CategoryController())
            {
                List<category> categories = c.getAllCategories();
                return JsonConvert.SerializeObject(categories, Formatting.None,
                        new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                        });
            }
        }

        [Route("api/Request/getAllCategoryByProductId")]
        [HttpGet]
        public string getAllCategoryByProductId(int product_id)
        {
            using (CategoryController c = new CategoryController())
            {
                List<category> categories = c.getAllCategoryByProductId(product_id);
                return JsonConvert.SerializeObject(categories, Formatting.None,
                        new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                        });
            }
        }

        [Route("api/Request/getAllCategoryCount")]
        [HttpGet]
        public string getAllCategoryCount(string q = "")
        {
            using (CategoryController c = new CategoryController())
            {
                string count = c.allCount(q).ToString();
                return JsonConvert.SerializeObject(count, Formatting.None,
                        new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                        });
            }
        }

        [Route("api/Request/getAllCategoryByPage")]
        [HttpGet]
        public string getAllCategoryByPage(int page = 1, int limit = 5, string q = "")
        {
            using (CategoryController c = new CategoryController())
            {
                List<category> categories = c.findAllByPage(limit, page, q);
                return JsonConvert.SerializeObject(categories, Formatting.None,
                        new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                        });
            }
        }

        [Route("api/Request/deleteOneCategory")]
        [HttpGet]
        public string deleteOneCategory(int id)
        {
            using (CategoryController c = new CategoryController())
            {
                string status = c.deleteOne(id);
                //string status = "fail";
                return JsonConvert.SerializeObject(status, Formatting.None,
                        new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                        });
            }
        }

        [Route("api/Request/insertOneCategory")]
        [HttpPost]
        public string insertOneCategory(JObject objData)
        {
            using (CategoryController c = new CategoryController())
            {
                dynamic jsonData = objData;
                category category = jsonData.ToObject<category>();
                string status = c.insertOne(category);

                return JsonConvert.SerializeObject(status, Formatting.None,
                        new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                        });
            }
        }

        [Route("api/Request/updateOneCategory")]
        [HttpPost]
        public string updateOneCategory(JObject objData)
        {
            using (CategoryController c = new CategoryController())
            {
                dynamic jsonData = objData;
                JObject categoryJson = jsonData.category;
                string idJson = jsonData.id;

                category category = categoryJson.ToObject<category>();
                int id = Convert.ToInt32(idJson);
                string status = c.updateOne(id, category);

                return JsonConvert.SerializeObject(status, Formatting.None,
                        new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                        });
            }
        }

        [Route("api/Request/getOneCategory")]
        [HttpGet]
        public string getOneCategory(int id)
        {
            using (CategoryController c = new CategoryController())
            {
                category category = c.findOne(id);
                //string status = "fail";
                return JsonConvert.SerializeObject(category, Formatting.None,
                        new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                        });
            }
        }

        #endregion

        #region product image

        [Route("api/Request/getAllProductImageByProductId")]
        [HttpGet]
        public string getAllProductImageByProductId(int product_id)
        {
            using (ProductImageController pic = new ProductImageController())
            {
                List<product_image> productImages = pic.getAllProductImageByProductId(product_id);
                return JsonConvert.SerializeObject(productImages, Formatting.None,
                        new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                        });
            }
        }


        [Route("api/Request/insertAllProductImage")]
        [HttpPost]
        public string insertAllProductImage(JArray objData)
        {
            using (ProductImageController pic = new ProductImageController())
            {
                dynamic jsonData = objData;

                List<string> images = new List<string>();
                string pidJson = jsonData[0].product_id;
                int product_id = Convert.ToInt32(pidJson);

                for (int i = 0; i < objData.Count; i++)
                {
                    string name = jsonData[i].name;
                    images.Add(name);
                }
                string status = pic.insertAll(product_id, images);
                return JsonConvert.SerializeObject(status, Formatting.None,
                        new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                        });
            }
        }

        #endregion

        #region product category

        [Route("api/Request/deleteInsertAll")]
        [HttpPost]
        public string deleteInsertAll(JArray objData)
        {
            using (ProductCategoryController pcc = new ProductCategoryController())
            {
                dynamic jsonData = objData;

                List<int> categories = new List<int>();
                string pidJson = jsonData[0].product_id;
                int product_id = Convert.ToInt32(pidJson);

                for (int i = 0; i < objData.Count; i++)
                {
                    string cidJson = jsonData[i].category_id;
                    int category_id = Convert.ToInt32(cidJson);
                    categories.Add(category_id);
                }
                string status = pcc.deleteInsertAll(product_id, categories);
                return JsonConvert.SerializeObject(status, Formatting.None,
                        new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                        });
            }
        }

        #endregion

        #region news info

        [Route("api/Request/deleteOneNewsInfo")]
        [HttpGet]
        public string deleteOneNewsInfo(int id)
        {
            using (NewsInfoController ni = new NewsInfoController())
            {
                string status = ni.deleteOne(id);
                //string status = "fail";
                return JsonConvert.SerializeObject(status, Formatting.None,
                        new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                        });
            }
        }

        [Route("api/Request/insertOneNewsInfo")]
        [HttpPost]
        public string insertOneNewsInfo(JObject objData)
        {
            using (NewsInfoController c = new NewsInfoController())
            {
                dynamic jsonData = objData;
                 news_info news = jsonData.ToObject<news_info>();
                string status = c.insertOne(news);

                return JsonConvert.SerializeObject(status, Formatting.None,
                        new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                        });
            }
        }

        [Route("api/Request/updateOneNewsInfo")]
        [HttpPost]
        public string updateOneNewsInfo(JObject objData)
        {
            using (NewsInfoController ni = new NewsInfoController())
            {
                dynamic jsonData = objData;
                JObject newsinfoJson = jsonData.news_info;
                string idJson = jsonData.id;

                news_info news = newsinfoJson.ToObject<news_info>();
                int id = Convert.ToInt32(idJson);
                string status = ni.updateOne(id, news);

                return JsonConvert.SerializeObject(status, Formatting.None,
                        new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                        });
            }
        }

        [Route("api/Request/getOneNewsInfo")]
        [HttpGet]
        public string getOneNewsInfo(int id)
        {
            using (NewsInfoController ni = new NewsInfoController())
            {
                news_info news = ni.findOne(id);
                //string status = "fail";
                return JsonConvert.SerializeObject(news, Formatting.None,
                        new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                        });
            }
        }

        [Route("api/Request/getAllNewsInfoCount")]
        [HttpGet]
        public string getAllNewsInfoCount(string q = "")
        {
            using (NewsInfoController ni = new NewsInfoController())
            {
                string count = ni.allCount(q).ToString();
                return JsonConvert.SerializeObject(count, Formatting.None,
                        new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                        });
            }
        }

        [Route("api/Request/getAllNewsInfoByPage")]
        [HttpGet]
        public string getAllNewsInfoByPage(int page = 1, int limit = 5, string q = "")
        {
            using (NewsInfoController ni = new NewsInfoController())
            {
                List<news_info> news = ni.findAllByPage(limit, page, q);
                return JsonConvert.SerializeObject(news, Formatting.None,
                        new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                        });
            }
        }

        [Route("api/Request/isFeaturedNewsExceed")]
        [HttpGet]
        public string isFeaturedNewsExceed()
        {
            using (NewsInfoController ni = new NewsInfoController())
            {
                string status = ni.isFeaturedExceed().ToString();
                return JsonConvert.SerializeObject(status, Formatting.None,
                        new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                        });
            }
        }

        #endregion

        #region   FCM

        [Route("api/Request/insertOneFcm")]
        [HttpPost]
        public JObject insertOneFcm(JObject objData)
        {
            dynamic jsonData = objData;
            fcm fcm = jsonData.ToObject<fcm>();

            //string json = "{'id':4,'device':'Device Name','os_version':'6.0.1','app_version':'1.0','serial':'GGC00C0888E426A','regid':'APA91bEj7qmlVePXUpG4UjKOtyqG5x9hpeZ4tMhPDsJgDRWL76psPGtckLK3uMtmpLFj3RSFfgaVoBMCKhg5iR8RnPZPjeuml8Llgkc','created_at':0,'last_update':636489489436993530}";
            //fcm fcm = JsonConvert.DeserializeObject<fcm>(json);
            
            using (FcmController fc = new FcmController())
            {
                string jsonstr = fc.insertOne(fcm);
                JObject jobj = JObject.Parse(jsonstr);
                return jobj;
            }
        }

        [Route("api/Request/getAllFcm")]
        [HttpGet]
        public string getAllFcm()
        {
            using (FcmController fc = new FcmController())
            {
                List<fcm> fcm = fc.findAll();
                return JsonConvert.SerializeObject(fcm, Formatting.None,
                        new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                        });
            }
        }

        [Route("api/Request/getAllFcmByPage")]
        [HttpGet]
        public string getAllFcmByPage(int page = 1, int limit = 10, string q = "")
        {
            using (FcmController fc = new FcmController())
            {
                List<fcm> fcm = fc.findAllByPage(page, limit, q);
                return JsonConvert.SerializeObject(fcm, Formatting.None,
                        new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                        });
            }
        }

        [Route("api/Request/getAllFcmCount")]
        [HttpGet]
        public int getAllFcmCount(string q)
        {
            using (FcmController fc = new FcmController())
            {
                return fc.allCount(q);
            }
        }
        #endregion

        #region App_Version

        [Route("api/Request/getOneAppVersion")]
        [HttpGet]
        public string getOneAppVersion(int id)
        {
            using (AppVersionController fc = new AppVersionController())
            {
                app_version app_version = fc.findOne(id);
                return JsonConvert.SerializeObject(app_version, Formatting.None,
                        new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                        });
            }
        }

        [Route("api/Request/getAllAppVersionByPage")]
        [HttpGet]
        public string getAllAppVersionByPage(int page = 1, int limit = 10, string q = "")
        {
            using (AppVersionController fc = new AppVersionController())
            {
                List<app_version> app_version = fc.findAllByPage(page, limit, q);
                return JsonConvert.SerializeObject(app_version, Formatting.None,
                        new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                        });
            }
        }

        [Route("api/Request/getAllAppVersionCount")]
        [HttpGet]
        public int getAllAppVersionCount(string q)
        {
            using (AppVersionController fc = new AppVersionController())
            {
                return fc.allCount(q);
            }
        }

        [Route("api/Request/insertOneAppVersion")]
        [HttpPost]
        public string insertOneAppVersion(JObject objData)
        {
            dynamic json = objData;
            app_version app = json.ToObject<app_version>();
            using (AppVersionController fc = new AppVersionController())
            {
                string response =  fc.insertOne(app);
                return JsonConvert.SerializeObject(response);
            }
        }

        [Route("api/Request/updateOneAppVersion")]
        [HttpPost]
        public string updateOneAppVersion(JObject objData)
        {
            dynamic json = objData;
            app_version app = json.ToObject<app_version>();
            using (AppVersionController fc = new AppVersionController())
            {
                string response = fc.updateOne(app);
                return JsonConvert.SerializeObject(response);
            }
        }

        [Route("api/Request/deleteOneAppVersion")]
        [HttpGet]
        public string deleteOneAppVersion(int id)
        {
            using (AppVersionController fc = new AppVersionController())
            {
                string response = fc.deleteOne(id);
                return JsonConvert.SerializeObject(response);
            }
        }

        #endregion

        #region ALL API Related android client 
        [Route("api/Request/info")]
        [HttpGet]
        /* Check status version and get some config data */
        public JObject info(string version)
        {
            using (ClientController cc = new ClientController())
            {
                string json = cc.info(version);
                JObject jobj = JObject.Parse(json);
                return jobj;
            }
        }

        [Route("api/Request/listFeaturedNews")]
        [HttpGet]
        public JObject listFeaturedNews()
        {
            using (ClientController cc = new ClientController())
            {
                string json = cc.findAllFeaturedNewsInfo();
                JObject jobj = JObject.Parse(json);
                return jobj;
            }
        }

        [Route("api/Request/listProduct")]
        [HttpGet]
        public JObject listProduct(string page = "1", string limit = "10", string q = "", string category_id = "-1")
        {
            using (ClientController cc = new ClientController())
            {
                string json = cc.findAllProduct(page, limit, q, category_id);
                //JObject jobj = JObject.Parse(json);
                string jtoken = JToken.Parse(json).RemoveFields(new string[] { "product_category", "product_image" }).ToString(Formatting.None);
                return JObject.Parse(jtoken);
            }
        }

        [Route("api/Request/getProductDetails")]
        [HttpGet]
        public JObject getProductDetails(string id)
        {
            using (ClientController cc = new ClientController())
            {
                string json = cc.findProductDetails(id);
                //JObject jobj = JObject.Parse(json);
                string token = JToken.Parse(json).RemoveFields(new string[] { "product_category", "product_image" }).ToString(Formatting.None);

                //JToken pc = jobj.SelectToken("$.product.product.product_category");
                //string pi = JToken.Parse(jtoken).SelectToken("$.product.product_images.product").ToString();
                //string jt = JToken.Parse(pi).RemoveFields(new string[] { "product" }).ToString(Formatting.None);
                //pc.Remove();

                string jtoken = JToken.Parse(token).RemoveFields(new string[] { "product" }, "product_images").ToString(Formatting.None);
                JObject jobj = JObject.Parse(jtoken);
                return jobj;
            }
        }

        [Route("api/Request/listCategory")]
        [HttpGet]
        public JObject listCategory()
        {
            using (ClientController cc = new ClientController())
            {
                string json = cc.findAllCategory();
                //JObject obj = JObject.Parse(json);
                string jtoken = JToken.Parse(json).RemoveFields(new string[] { "product_category" }).ToString(Formatting.None);
                return JObject.Parse(jtoken);
            }
        }

        [Route("api/Request/listNews")]
        [HttpGet]
        public JObject listNews(string page = "1", string limit = "10", string q = "")
        {
            using (ClientController cc = new ClientController())
            {
                string json = cc.findAllNewsInfo(page, limit, q);
                JObject jobj = JObject.Parse(json);
                return jobj;
            }
        }

        [Route("api/Request/getNewsDetails")]
        [HttpGet]
        public JObject getNewsDetails(string id)
        {
            using (ClientController cc = new ClientController())
            {
                string json = cc.findNewsDetails(id);
                JObject jobj = JObject.Parse(json);
                return jobj;
            }
        }

        [Route("api/Request/submitProductOrder")]
        [HttpPost]
        public JObject submitProductOrder(JObject jsondata)
        {
            //string jsondata = @"{'product_order':{'product_order_detail':[{'id':1,'order_id':1,'product_id':125,'product_name':'Samsung Galaxy J3 Pro','amount':1,'price_item':7990.00,'created_at':1502910082120,'last_update':1502910082120}],'id':1,'code':'KP43001WR','buyer':'test','address':'dfn sdlkf est','email':'9892413631','shipping':'FEDEX','date_ship':1502910027587,'phone':'amey.thakoor @gmail.com','comment':'','status':'WAITING','total_fees':7990.00,'tax':0.00,'created_at':1502910027587,'last_update':1502910027587},'product_order_detail':[{'product_order':{'product_order_detail':[],'id':1,'code':'KP43001WR','buyer':'test','address':'dfn sdlkf est','email':'9892413631','shipping':'FEDEX','date_ship':1502910027587,'phone':'amey.thakoor @gmail.com','comment':'','status':'WAITING','total_fees':7990.00,'tax':0.00,'created_at':1502910027587,'last_update':1502910027587},'id':1,'order_id':1,'product_id':125,'product_name':'Samsung Galaxy J3 Pro','amount':1,'price_item':7990.00,'created_at':1502910082120,'last_update':1502910082120}]}";
            dynamic json = jsondata;//JsonConvert.DeserializeObject(jsondata);
            List<product_order_detail> pod = json.product_order_detail.ToObject<List<product_order_detail>>();
            product_order po = json.product_order.ToObject<product_order>();

            using (ClientController cc = new ClientController())
            {
                string jsonstr = cc.submitProductOrder(po, pod);
                JObject jobj = JObject.Parse(jsonstr);
                return jobj;
            }
        }
        #endregion


        [Route("api/Request/getMerchants")] 
        [HttpGet]
        public JObject getMerchants(string searchString)     //change this name in xcel sheet after pushing this code 
        {
            using (MerchantController c = new MerchantController())
            {
                string json = c.getMerchans(searchString);
                JObject jobj = JObject.Parse(json);
                return jobj;
            }
        }

        [Route("api/Request/getProductsByMerchantId")]
        [HttpPost]
        public JObject getProductsByMerchantId(JObject request) 
        {
            using(MerchantController c=new MerchantController())
            {
                int mid = 0;

                if (request["mid"] != null)
                    int.TryParse(request["mid"].ToString(), out mid);

                string json = c.getProductsByMerchantId(mid);

                JObject jo = JObject.Parse(json);
            return jo;
            }
        }
        [Route("api/Request/getMerchants")]
        [HttpPost]
        public JObject getMerchants(JObject request) 
        {
            using (MerchantController c = new MerchantController()) {
                decimal latitude = 0;
                decimal longitude = 0;

                if (request["latitude"] != null)
                    decimal.TryParse(request["latitude"].ToString(), out latitude);

                if (request["longitude"] != null)
                    decimal.TryParse(request["longitude"].ToString(), out longitude);

                string json = c.getMerchants(latitude, longitude);
            JObject jo =  JObject.Parse(json);
            return jo;
        }
        }


        [Route("api/Request/getMerchantsByProductId")]
        [HttpPost]
        public JObject getMerchantsByProductId(JObject request)
        {
            using (MerchantController c = new MerchantController())
            {
                int productId = 0;
                decimal latitude = 0;
                decimal longitude = 0;

                if (request["pid"] != null)
                    int.TryParse(request["pid"].ToString(), out productId);

                if (request["latitude"] != null)
                    decimal.TryParse(request["latitude"].ToString(), out latitude);

                if (request["longitude"] != null)
                    decimal.TryParse(request["longitude"].ToString(), out longitude);

                string json = c.getMerchantsByProductId(productId, latitude, longitude);
                JObject jo = JObject.Parse(json);
                return jo;
            }
        }

        [Route("api/Request/getCategoryByMerchantId")]
        [HttpPost]
        public JObject getCategoryByMerchantId(int mid) 
        {
            using (MerchantController c = new MerchantController())
            {
                string json = c.getCategoryByMerchantId(mid);
                JObject jo = JObject.Parse(json);
                return jo;
            }
        }
        [Route("api/Request/login")]
        [HttpPost]
        public string login(JObject objData)
        {
            using (UserController uc = new UserController())
            {
                dynamic jsonData = objData;
                user u = jsonData.ToObject<user>();
                user nu = uc.processLogin(u);

                return JsonConvert.SerializeObject(nu, Formatting.None,
                        new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                        });
            }
        }
    }
}