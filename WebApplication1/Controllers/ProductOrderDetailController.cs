﻿
using IAV.Data.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace IAVAPI.Controllers
{
  
    public class ProductOrderDetailsController :  IDisposable
    {
        string constring = ConfigurationManager.ConnectionStrings["markeet_demoEntities"].ConnectionString;
        markeet_demoEntities dbContextIAV = new markeet_demoEntities();

        public List<product_order_detail> findAllByOrderIdPlain(int order_id)
        { 
            //if($this->get_request_method() != "GET") $this->response('',406);
            //if(!isset($this->_request['limit']) || !isset($this->_request['page']))$this->responseInvalidParam();
            
            //string query = "SELECT DISTINCT * FROM product_order_detail pod WHERE pod.order_id=$order_id;";
            if (order_id == 0)
                return dbContextIAV.product_order_detail.Distinct().ToList();
            else
                return dbContextIAV.product_order_detail.Where(o => o.order_id == order_id).Distinct().ToList();
        }

        public List<product_order_detail> findAllByOrderId(int order_id = 0)
        {
            return findAllByOrderIdPlain(order_id);
        }

        public string deleteInsertAll(List<product_order_detail> po)
        {
            var success = new { status = "success" };
            var fail = new { status = "fail" };

            long order_id = po.FirstOrDefault().order_id;

            product_order order = dbContextIAV.product_order.Where(x => x.id == order_id).FirstOrDefault();
            List<product_order_detail> del = dbContextIAV.product_order_detail.Where(x => x.order_id == order_id).ToList();
            foreach (product_order_detail d in del)
                dbContextIAV.product_order_detail.Remove(d);

            try
            {
                dbContextIAV.SaveChanges();

                foreach (product_order_detail d in po)
                {
                    d.product_order = null;
                    order.product_order_detail.Add(d);
                    dbContextIAV.SaveChanges();
                }

                return JsonConvert.SerializeObject(success);
            }
            catch (DbEntityValidationException e)
            {
                return JsonConvert.SerializeObject(fail);
            }
            catch (Exception e)
            {
                return JsonConvert.SerializeObject(fail);
            }

        }


        public string insertAllPlain(long order_id, List<product_order_detail> data)
        {
            var success = new { status = "success", msg = "Order placed successfully!" };
            var fail = new { status = "fail", msg = "Order failed!" };

            for (int i = 0; i < data.Count(); i++) {
                data[i].order_id = order_id;
            }

            try
            {
                product_order order = dbContextIAV.product_order.Where(x => x.id == order_id).FirstOrDefault();

                foreach (product_order_detail d in data)
                {
                    d.product_order = null;
                    order.product_order_detail.Add(d);
                    dbContextIAV.SaveChanges();
                }
                return JsonConvert.SerializeObject(success);
            }
            catch (Exception e)
            {
                return JsonConvert.SerializeObject(fail);
            }
        }

        public processOrderModel checkAvailableProductOrderDetail(List<product_order_detail> order_detail)
        {
            processOrderModel model = new processOrderModel();
            //$resp = array('status' => 'success', 'data' => null);
            //$status = array();

            // find and check available each product
            foreach (product_order_detail od in order_detail)
            {
                //$status_item = array('product_id' => $od['product_id'], 'stock' => 0, 'amount' => 0, 'product_name' => $od['product_name'], 'msg' => 'OK');
                processOrderDetailsModel m = new processOrderDetailsModel();
                //$product_id = $od['product_id'];
                m.product_id = od.product_id;
                //$query = "SELECT * FROM product p WHERE p.id=$product_id LIMIT 1";
                var p = dbContextIAV.products.Where(pr => pr.id == m.product_id);
                //$result = array();
                //$r = $this->mysqli->query($query) or die($this->mysqli->error.__LINE__);
                if (p.Count() > 0)
                {
                    product pro = p.FirstOrDefault();
                    //$result = $r->fetch_assoc();
                    m.product_name = pro.name;
                    m.stock = pro.stock;
                    m.amount = od.amount;
                    if (m.stock < m.amount)
                    {
                        m.msg = "Stock Not Enough";
                        model.status = "failed";
                    }
                }
                else
                {
                    m.msg = "Product Not Exist";
                    model.status = "failed";
                }
                //array_push($status, $status_item);
                model.data.Add(m);
            }
            //$resp['data'] = $status;
            //return $resp;
            return model;
        }

        void IDisposable.Dispose()
        {

        }

    }
}