﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace IAV.Data.Models
{
    public class dashboardModel
    {
        public Order order { get; set; }
        public Product product { get; set; }
        public Category category { get; set; }
        public News news { get; set; }
        public App app { get; set; }
        public Setting setting { get; set; }
        public Notification notification { get; set; }

        public dashboardModel()
        {
            order = new Order();
            product = new Product();
            category = new Category();
            news = new News();
            app = new App();
            setting = new Setting();
            notification = new Notification();

            order.waiting = 0;
            order.processed = 0;
            order.total = 0;

            product.published = 0;
            product.draft = 0;
            product.ready_stock = 0;
            product.out_of_stock = 0;
            product.suspend = 0;

            category.published = 0;
            category.draft = 0;

            news.featured = 0;
            news.published = 0;
            news.draft = 0;

            app.active = 0;
            app.inactive = 0;

            setting.currency = string.Empty;
            setting.featured_news = 0;
            setting.tax = 0;

            notification.users = 0;
        }
    }

    public class Order
    {
        public int waiting { get; set; }
        public int processed { get; set; }
        public int total { get; set; }
    }

    public class Product
    {
        public int published { get; set; }
        public int draft { get; set; }
        public int ready_stock { get; set; }
        public int out_of_stock { get; set; }
        public int suspend { get; set; }
    }

    public class Category   
    {
        public int published { get; set; }
        public int draft { get; set; }
    }

    public class News   
    {
        public int featured { get; set; }
        public int published { get; set; }
        public int draft { get; set; }
    }

    public class App   
    {
        public int active { get; set; }
        public int inactive { get; set; }
    }

    public class Setting   
    {
        public string currency { get; set; }
        public int tax { get; set; }
        public int featured_news { get; set; }
    }

    public class Notification   
    {
        public int users { get; set; }
    }
}