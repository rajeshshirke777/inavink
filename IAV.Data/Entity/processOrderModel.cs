﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using IAV.Data.Models;

namespace IAV.Data.Models
{
    public class processOrderModel
    {
        public string status { get; set; }
        public List<processOrderDetailsModel> data { get; set; }

        public processOrderModel()
        {
            status = "success";
            data = new List<processOrderDetailsModel>();
        }
        
    }

    public class processOrderDetailsModel
    {
        public long product_id { get; set; }
        public int stock { get; set; }
        public int amount { get; set; }
        public string product_name { get; set; }
        public string msg { get; set; }

        public processOrderDetailsModel()
        {
            product_id = 0;
            stock = 0;
            amount = 0;
            product_name = string.Empty;
            msg = "OK";
        }
    }
}
