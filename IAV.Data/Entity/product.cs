﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using IAV.Data.Models;

namespace IAV.Data.Models
{
    [Table("app_version")]
    public partial class app_version
    {
        public virtual long id { get; set; }
        public virtual int version_code { get; set; }
        public virtual string version_name { get; set; }
        public virtual bool active { get; set; }
        public virtual long created_at { get; set; }
        public virtual long last_update { get; set; }
    }

    [Table("config")]
    public partial class config
    {
        [Key]
        public virtual string code { get; set; }
        public virtual string value { get; set; }
    }

    [Table("currency")]
    public partial class currency
    {
        public virtual long id { get; set; }
        public virtual string code { get; set; }
        public virtual string name { get; set; }
    }

    public partial class getNearestMerchants_Result
    {
        public virtual decimal latitude { get; set; }
        public virtual decimal longitude { get; set; }
        public virtual string business_name { get; set; }
        public virtual string address_line1 { get; set; }
        public virtual long store_no { get; set; }
        public virtual string phone_no { get; set; }
    }

    [Table("merchant")]
    public partial class merchant
    {
        [Key]
        public virtual string mid { get; set; }
        public virtual string organization_type { get; set; }
        public virtual string merchant_name_one { get; set; }
        public virtual string merchant_name_two { get; set; }
        public virtual string merchant_name_three { get; set; }
        public virtual string merchant_name_four { get; set; }
        public virtual string merchant_name_five { get; set; }
        public virtual string merchant_name_six { get; set; }
        public virtual string organization_name { get; set; }
        public virtual string store_name { get; set; }
        public virtual string gstin { get; set; }
        [Display(Name = "GST Certificate")]
        public virtual string tan { get; set; }
        public virtual string cin { get; set; }
        public virtual string gumasta_license { get; set; }
        public virtual string contact_person_name { get; set; }
        public virtual string primary_contact_number { get; set; }
        public virtual string secondary_contact_number { get; set; }

        public virtual string primary_emailid { get; set; }
        public virtual string secondary_emailid { get; set; }
        public virtual string working_hours { get; set; }

        public virtual string business_name { get; set; }

        public virtual string signature { get; set; }
        public virtual string address_line1 { get; set; }
        public virtual string address_line2 { get; set; }
        public virtual Nullable<int> pincode { get; set; }
        public virtual string city { get; set; }
        public virtual string state { get; set; }
        public virtual Nullable<int> store_no { get; set; }
        public virtual string phone_no { get; set; }
        public virtual Nullable<decimal> latitude { get; set; }
        public virtual Nullable<decimal> longitude { get; set; }
        public virtual string store_picture { get; set; }
        public virtual Nullable<decimal> delivery_charges { get; set; }
        [Display(Name = "for total and value less than")]
        public virtual Nullable<decimal> charge { get; set; }

        //public virtual user_account user_account { get; set; }
    }

    [Table("user_account")]
    public partial class user_account
    {
        [Key]
        public virtual int id { get; set; }
        public virtual string user_name { get; set; }
        public virtual string email { get; set; }
        public virtual string password { get; set; }
        public virtual Guid activation_code { get; set; }
        public virtual bool is_active { get; set; }
        public virtual bool is_deleted { get; set; }
        public virtual bool is_lock { get; set; }
        public virtual long? lockout_date { get; set; }
        public virtual int? failed_password_attempt_count { get; set; }
        public virtual long? failed_password_start_window { get; set; }
        public virtual long created_at { get; set; }
        public virtual long last_update { get; set; }


        public virtual ICollection<role> roles { get; set; }
    }

    [Table("user_reset_account")]
    public partial class user_reset_account
    {
        [Key]
        public virtual int id { get; set; }
        [ForeignKey("user_account")]
        public virtual int user_id { get; set; }
        public virtual user_account user_account { get; set; }
        public virtual string password_reset_id { get; set; }
        public virtual long password_reset_date { get; set; }
        public virtual bool is_active { get; set; }
        public virtual long created_at { get; set; }
        public virtual long last_update { get; set; }
    }

    [Table("role")]
    public partial class role
    {
        [Key]
        public virtual int id { get; set; }
        public virtual string role_name { get; set; }
        
        public virtual ICollection<user_account> users { get; set; }
    }


    [Table("news_info")]
    public partial class news_info
    {
        public virtual long id { get; set; }
        public virtual string title { get; set; }
        public virtual string brief_content { get; set; }
        public virtual string full_content { get; set; }
        public virtual string image { get; set; }
        public virtual bool draft { get; set; }
        public virtual string status { get; set; }
        public virtual long created_at { get; set; }
        public virtual long last_update { get; set; }
    }


    [Table("fcm")]
    public partial class fcm
    {
        public virtual long id { get; set; }
        public virtual string device { get; set; }
        public virtual string os_version { get; set; }
        public virtual string app_version { get; set; }
        public virtual string serial { get; set; }
        public virtual string regid { get; set; }
        public virtual long created_at { get; set; }
        public virtual long last_update { get; set; }
    }


    [Table("category")]
    public partial class category
    {
        //public virtual category()
        //{
        //    this.product_category = new HashSet<product_category>();
        //}

        public virtual long id { get; set; }
        public virtual string name { get; set; }
        public virtual string icon { get; set; }
        public virtual bool draft { get; set; }
        public virtual string brief { get; set; }
        public virtual string color { get; set; }
        public virtual int priority { get; set; }
        public virtual long created_at { get; set; }
        public virtual long last_update { get; set; }

        public virtual ICollection<product_category> product_category { get; set; }
    }


    [Table("product")]
    public class product
    {
        //public virtual product()
        //{
        //    this.product_category = new HashSet<product_category>();
        //    this.product_image = new HashSet<product_image>();
        //}

        public virtual long id { get; set; }
        public virtual string name { get; set; }
        public virtual string image { get; set; }
        public virtual decimal price { get; set; }
        public virtual int stock { get; set; }
        public virtual bool draft { get; set; }
        public virtual string description { get; set; }
        public virtual string status { get; set; }
        public virtual long created_at { get; set; }
        public virtual long last_update { get; set; }
        public virtual string brand_name { get; set; }

        public virtual ICollection<product_category> product_category { get; set; }
        public virtual ICollection<product_image> product_image { get; set; }
    }


    [Table("product_category")]
    public class product_category
    {
        [ForeignKey("product")]
        public virtual long product_id { get; set; }
        [ForeignKey("category")]
        public virtual long category_id { get; set; }
        public virtual long id { get; set; }

        public virtual category category { get; set; }
        public virtual product product { get; set; }
    }

    [Table("product_image")]
    public class product_image
    {
        [ForeignKey("product")]
        public virtual long product_id { get; set; }
        public virtual string name { get; set; }
        public virtual long id { get; set; }

        public virtual product product { get; set; }
    }

    [Table("product_merchant")]
    public class product_merchant
    {
        public virtual long id { get; set; }
        public virtual string product_id { get; set; }
        public virtual string merchant_id { get; set; }
        public virtual Nullable<long> quantity_available { get; set; }
        public virtual Nullable<decimal> actual_price { get; set; }
        public virtual Nullable<decimal> discounted_price { get; set; }
        public virtual long created_at { get; set; }
        public virtual long last_update { get; set; }
    }

    [Table("product_order")]
    public class product_order
    {
        //[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        //public product_order()
        //{
        //    this.product_order_detail = new HashSet<product_order_detail>();
        //}

        public virtual long id { get; set; }
        public virtual string code { get; set; }
        public virtual string buyer { get; set; }
        public virtual string address { get; set; }
        public virtual string email { get; set; }
        public virtual string shipping { get; set; }
        public virtual long date_ship { get; set; }
        public virtual string phone { get; set; }
        public virtual string comment { get; set; }
        public virtual string status { get; set; }
        public virtual decimal total_fees { get; set; }
        public virtual decimal tax { get; set; }
        public virtual long created_at { get; set; }
        public virtual long last_update { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<product_order_detail> product_order_detail { get; set; }
    }

    [Table("product_order_detail")]
    public partial class product_order_detail
    {
        [Key]
        public virtual long id { get; set; }
        [ForeignKey("product_order")]
        public virtual long order_id { get; set; }
        public virtual long product_id { get; set; }
        public virtual string merchant_id { get; set; }
        public virtual string product_name { get; set; }
        public virtual int amount { get; set; }
        public virtual decimal price_item { get; set; }
        public virtual long created_at { get; set; }
        public virtual long last_update { get; set; }

        public virtual product_order product_order { get; set; }
    }


    [Table("product_log")]
    public class product_log
    {
        public virtual long id { get; set; }
        [ForeignKey("product_merchant")]
        public virtual long product_merchant_id { get; set; }
        public virtual int quantity_added { get; set; }
        public virtual int quantity_removed { get; set; }
        public virtual string description { get; set; }
        public virtual long created_at { get; set; }
        public virtual long last_update { get; set; }


        //public virtual product product { get; set; }
        public virtual product_merchant product_merchant { get; set; }
        //public virtual merchant merchant { get; set; }
    }

    [Table("user")]
    public partial class user
    {
        public virtual long id { get; set; }
        public virtual string name { get; set; }
        public virtual string username { get; set; }
        public virtual string email { get; set; }
        public virtual string password { get; set; }
    }
}
