﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IAV.Data.Models
{
    public class markeet_demoEntities : DbContext
    {
        public markeet_demoEntities()
            : base("name=markeet_demoEntities")
        {
            this.Configuration.LazyLoadingEnabled = true;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //modelBuilder.IncludeMetadataInDatabase = false;
            //modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }

        public virtual DbSet<app_version> app_version { get; set; }
        public virtual DbSet<category> categories { get; set; }
        public virtual DbSet<config> configs { get; set; }
        public virtual DbSet<currency> currencies { get; set; }
        public virtual DbSet<fcm> fcms { get; set; }
        public virtual DbSet<merchant> merchants { get; set; }
        public virtual DbSet<user_account> user_accounts { get; set; }
        public virtual DbSet<user_reset_account> user_reset_accounts { get; set; }
        public virtual DbSet<role> roles { get; set; }
        public virtual DbSet<news_info> news_info { get; set; }
        public virtual DbSet<product> products { get; set; }
        public virtual DbSet<product_category> product_category { get; set; }
        public virtual DbSet<product_merchant> product_merchant { get; set; }
        public virtual DbSet<product_order> product_order { get; set; }
        public virtual DbSet<product_order_detail> product_order_detail { get; set; }
        public virtual DbSet<user> users { get; set; }
        public virtual DbSet<product_image> product_image { get; set; }

        public virtual DbSet<product_log> product_logs { get; set; }

        public virtual ObjectResult<getNearestMerchants_Result> getNearestMerchants(string product_name, string latitude, string longitude)
        { 
            var product_idParameter = product_name != null ?
                new ObjectParameter("product_name", product_name) :
                new ObjectParameter("product_name", typeof(string));

            var latitudeParameter = latitude != null ?
                new ObjectParameter("latitude", latitude) :
                new ObjectParameter("latitude", typeof(string));

            var longitudeParameter = longitude != null ?
                new ObjectParameter("longitude", longitude) :
                new ObjectParameter("longitude", typeof(string));

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<getNearestMerchants_Result>("getNearestMerchants", product_idParameter, latitudeParameter, longitudeParameter);
        }
    }
}
