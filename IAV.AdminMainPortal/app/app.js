angular.module('App', 
['ngMaterial', 'ngRoute', 'ngMessages', 'ngCookies', 'ngSanitize', 'cl.paging', 'textAngular', 'colorpicker.module']);

/* Theme Configuration
 */
angular.module('App').config(function ($mdThemingProvider) {

	// Use as Primary color
	var myPrimary = $mdThemingProvider.extendPalette('green', {
			'500': '4DB151',
			'contrastDefaultColor': 'light'
		});

	// Use as Accent color
	var myAccent = $mdThemingProvider.extendPalette('yellow', {
			'500': 'FFDE26'
		});

	// Register the new color palette
	$mdThemingProvider.definePalette('myPrimary', myPrimary);

	// Register the new color palette
	$mdThemingProvider.definePalette('myAccent', myAccent);

	$mdThemingProvider.theme('default')
	.primaryPalette('myPrimary')
	.accentPalette('myAccent');
});

/* Menu Route Configuration */

angular.module('App').config(['$routeProvider',
	function ($routeProvider) {
		//$routeProvider.
		//when('/dashboard', { redirectTo: '~/dashboard/Index', controller: 'DashboardController' }).
		//when('/order', { redirectTo: '~/order/Index', controller: 'OrderController' })
		//when('/create_order', { templateUrl: 'Views/order/create.html', controller: 'AddOrderController' }).
		//when('/product', { templateUrl: 'Views/product/list.html', controller: 'ProductController' }).
		//when('/create_product', { templateUrl: 'Views/product/create.html', controller: 'AddProductController' }).
		//when('/category', { templateUrl: 'Views/category/list.html', controller: 'CategoryController' }).
		//when('/create_category', { templateUrl: 'Views/category/create.html', controller: 'AddCategoryController' }).
		//when('/news', { templateUrl: 'Views/news/list.html', controller: 'NewsController' }).
		//when('/create_news', { templateUrl: 'Views/news/create.html', controller: 'AddNewsController' }).
		//when('/application', { templateUrl: 'Views/application/list.html', controller: 'ApplicationController' }).
		//when('/notification', { templateUrl: 'Views/notification/list.html', controller: 'NotificationController' }).
		//when('/setting', { templateUrl: 'Views/setting/setting.html', controller: 'SettingController' }).
		//when('/about', { templateUrl: 'Views/about/about.html', controller: 'AboutController' }).
        //when('/merchant', { templateUrl: 'Views/merchant/index.html', controller: 'MerchantController' }).
		//when('/login', { templateUrl: 'Views/login.html', controller: 'LoginController' }).
        //otherwise({ redirectTo: '/login' });
	}
]);


angular.module('App').factory('focus', function($timeout, $window) {
    return function(id) {
		// timeout makes sure that is invoked after any other event has been triggered.
		// e.g. click events that need to run before the focus or inputs elements that are in a disabled state but are enabled when those events are triggered.
		$timeout(function() {
			var element = $window.document.getElementById(id);
			if(element)element.focus();
		});
	};
});

angular.module('App').run(function ($location, $rootScope, $cookies) {
	$rootScope.$on('$routeChangeSuccess', function (event, current, previous) {
		// $rootScope.title = current.$$route.title;
	});
});

angular.module('App').filter('cut', function () {
	return function (value, wordwise, max, tail) {
		if (!value) return '';

		max = parseInt(max, 10);
		if (!max) return value;
		if (value.length <= max) return value;

		value = value.substr(0, max);
		if (wordwise) {
			var lastspace = value.lastIndexOf(' ');
			if (lastspace != -1) {
				//Also remove . and , so its gives a cleaner result.
				if (value.charAt(lastspace-1) == '.' || value.charAt(lastspace-1) == ',') {
					lastspace = lastspace - 1;
				}
				value = value.substr(0, lastspace);
			}
		}

		return value + (tail || ' …');
	};
});

//angular.module('App').factory('MerchantService', ['$http', function ($http) {

//    var MerchantService = {};
//    MerchantService.getMerchants = function () {
//        return $http.get('/Mmerchants/GetMerchants');
//    };
//    return MerchantService;

//}]);

//angular.module('App').controller('MerchantController', function ($scope, MerchantService) {

//    $scope.message = "Merchants";

//    getMerchants();
//    function getMerchants() {
//        MerchantService.getMerchants()
//            .success(function (merchants) {
//                $scope.merchants = merchants;
//                console.log($scope.merchants);
//            })
//            .error(function (error) {
//                $scope.status = 'Unable to load customer data: ' + error.message;
//                console.log($scope.status);
//            });
//    }

//});
