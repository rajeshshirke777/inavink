﻿
using IAV.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IAVAdmin.ViewModels
{
    public class MerchantProductViewModel
    {
        private markeet_demoEntities db = new markeet_demoEntities();
        public List<SelectListItem> ddlProducts { get; set; }
        public List<product> Products { get; set; }
        public List<merchant> Merchants { get; set; }
        public product_merchant MP { get; set; }
        public List<MPDetail> MerchantProducts { get; set; } 
        public MPDetail SelectedProduct { get; set; }

        public MerchantProductViewModel()
        {
          
            try
            {
                Products = db.products.ToList();
                Merchants = db.merchants.ToList();
                MP = db.product_merchant.Where(m => m.merchant_id == "3").FirstOrDefault();
                MerchantProducts = GetMerchantProducts("3");
                SelectedProduct = GetSelectedProduct(MP.id);
            }
            catch (Exception e) 
            {
                Console.WriteLine(e.Message);
            }
            
        }

        public MPDetail GetSelectedProduct(string id)
        {
            //return db.products.Where(p => p.id.ToString() == MP.product_id).FirstOrDefault();
            return MerchantProducts.Where(mp => mp.id == id).FirstOrDefault();
        }

        private List<MPDetail> GetMerchantProducts(string mid)
        {
            /*
            var prodcts = from p in db.products
                          let pid = from pm in db.product_merchant
                                    where pm.merchant_id == mid
                                    select pm.product_id
                          where pid.Contains(p.id.ToString())
                          select p;
            */
            List <MPDetail > details = new List<MPDetail>();
            foreach (var item in db.product_merchant.ToList())
            {
                if(item.merchant_id == mid)
                {
                    MPDetail mpd = new MPDetail();
                    mpd.id = item.id;
                    mpd.quantity_available = item.quantity_available;
                    mpd.discounted_price = item.discounted_price;
                    mpd.actual_price = item.actual_price;
                    mpd.merchantId = item.merchant_id;
                    foreach (var p in db.products.ToList())
                    {
                        if (p.id.ToString() == item.product_id)
                        {
                            mpd.productId = p.id.ToString();
                            mpd.Name = p.name;
                            break;
                        }
                    }
                    details.Add(mpd);
                }

            }
            return details;
        }
    }

    public class MPDetail
    {
        public string id { get; set; }
        public string productId { get; set; }
        public string merchantId { get; set; }
        public string Name { get; set; }
        public long? quantity_available { get; set; }
        public decimal? actual_price { get; set; }
        public decimal? discounted_price { get; set; }

    }
}