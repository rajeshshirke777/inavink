﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Dynamic;
using System.Web.Mvc.Html;
using IAVAdmin.ViewModels;
using IAV.Data.Models;
using System.Data.Entity;


namespace IAVAdmin.Controllers
{
    public class MerchantProductsController : Controller
    {
        private markeet_demoEntities db = new markeet_demoEntities();
        MerchantProductViewModel vmMerchantProducts = new MerchantProductViewModel();
        
        // GET: MerchantProducts
        public ActionResult Index()
        {
            return View(vmMerchantProducts);
        }

        // GET: MerchantProducts/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //product_merchant product_merchant = db.product_merchant.Find(id);
            //product product = db.products.Find(Convert.ToInt64(id));
            vmMerchantProducts.SelectedProduct = vmMerchantProducts.GetSelectedProduct(id);
            if (vmMerchantProducts.SelectedProduct == null)
            {
                return HttpNotFound();
            }
            return View(vmMerchantProducts);
        }

        // GET: MerchantProducts/Create
        public ActionResult Create()
        {
            vmMerchantProducts.ddlProducts = new List<SelectListItem>();
            foreach (var prod in vmMerchantProducts.Products)
            {
                vmMerchantProducts.ddlProducts.Add(new SelectListItem() { Text = prod.name, Value = prod.id.ToString() });
            }
            return View(vmMerchantProducts);
        }

        // POST: MerchantProducts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(MerchantProductViewModel vm)
        {
            //change merchant id
            vm.MP.merchant_id = "3";
            vm.MP.product_id = Request.Form["ddlProduct"].ToString();
            vm.MP.id = (Convert.ToInt32(db.product_merchant.ToList().Max(mp => mp.id)) + 1).ToString();

            if (ModelState.IsValid)
            {
                db.product_merchant.Add(vm.MP);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(vm);
        }

        // GET: MerchantProducts/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            //change merchant id
            //product_merchant product_merchant = db.product_merchant.Where(mp => mp.merchant_id == "3" && mp.product_id == id.ToString()).FirstOrDefault();
            vmMerchantProducts.MP = db.product_merchant.Where(mp => mp.merchant_id == "3" && mp.id == id.ToString()).FirstOrDefault();

            vmMerchantProducts.ddlProducts = new List<SelectListItem>();
            foreach (var prod in vmMerchantProducts.Products)
            {
                if (prod.id.ToString() == vmMerchantProducts.MP.product_id)
                    vmMerchantProducts.ddlProducts.Add(new SelectListItem() { Text = prod.name, Value = prod.id.ToString(), Selected = true });
                else
                    vmMerchantProducts.ddlProducts.Add(new SelectListItem() { Text = prod.name, Value = prod.id.ToString() });
            }

            if (vmMerchantProducts.MP == null)
            {
                return HttpNotFound();
            }
            return View(vmMerchantProducts);
        }

        // POST: MerchantProducts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(MerchantProductViewModel vm)
        {
            if (ModelState.IsValid)
            {
                db.Entry(vm.MP).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(vm);
        }

        // GET: MerchantProducts/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            //product_merchant product_merchant = db.product_merchant.Find(id);

            //ToDo: change merchant it
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            vmMerchantProducts.SelectedProduct = vmMerchantProducts.GetSelectedProduct(id);
            if (vmMerchantProducts.SelectedProduct == null)
            {
                return HttpNotFound();
            }
            return View(vmMerchantProducts);
        }

        // POST: MerchantProducts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            //product_merchant product_merchant = db.product_merchant.Find(id);
            //ToDo: change merchant it
            product_merchant product_merchant = db.product_merchant.Where(mp => mp.merchant_id == "3" && mp.id == id.ToString()).FirstOrDefault();
            db.product_merchant.Remove(product_merchant);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        
    }
}
