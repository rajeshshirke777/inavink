﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.Data.Entity;
using System.IO;
using System.Collections.Generic;
using IAV.Data.Models;


namespace IAVAdmin
{
    public class MmerchantsController : Controller
    {
        private markeet_demoEntities db = new markeet_demoEntities();
        
        // GET: Mmerchants
        public ActionResult Index()
        {
            List<merchant> abc = new List<merchant>();
            //IEnumerable<getNearestMerchants_Result> NearestMerchants = db.getNearestMerchants(133, "1.098", "9.099");
            try {
                abc = db.merchants.ToList(); 
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
           
            return View(abc);
        }

        public JsonResult GetMerchants()
        {
            //IEnumerable<getNearestMerchants_Result> NearestMerchants = db.getNearestMerchants(133, "1.098", "9.099");
            var result = db.merchants.ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        // GET: Mmerchants/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            merchant merchant = db.merchants.Find(id);
            if (merchant == null)
            {
                return HttpNotFound();
            }
            return View(merchant);
        }

        // GET: Mmerchants/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Mmerchants/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "mid,business_name,gstin,tan,cin,signature,address_line1,address_line2,pincode,city,state,phone_no,store_no, latitude, longitude,delivery_charges,charge")] merchant merchant, HttpPostedFileBase gstin, HttpPostedFileBase tan, HttpPostedFileBase cin, HttpPostedFileBase storePic)
        {
            CheckFileIsNull(gstin, "gstin");
            CheckFileIsNull(tan, "tan");
            CheckFileIsNull(cin, "cin");
            CheckFileIsNull(storePic, "storePic");
            if (ModelState.IsValid)
            {
                merchant.gstin = SaveFile(gstin);
                merchant.tan = SaveFile(tan);
                merchant.cin = SaveFile(cin);
                merchant.store_picture = SaveFile(storePic);
                merchant.mid = ((Convert.ToInt32(db.merchants.Max(m => m.mid))) + 1).ToString();
                merchant.latitude = Convert.ToDecimal(TempData["lat"]);
                merchant.longitude = Convert.ToDecimal(TempData["long"]);
                db.merchants.Add(merchant);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(merchant);
        }

        [HttpPost]
        
        public ActionResult UpdateLoc(string lat, string lon)
        {
            TempData["lat"] = lat;
            TempData["long"] = lon;
            return new EmptyResult();
        }

        // GET: Mmerchants/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            merchant merchant = db.merchants.Find(id);
            if (merchant == null)
            {
                return HttpNotFound();
            }
            return View(merchant);
        }

        // POST: Mmerchants/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(merchant merchant)
        {
           if (ModelState.IsValid)
            {
                db.Entry(merchant).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(merchant);
        }

        
        public void CheckFileIsNull(HttpPostedFileBase file, string name)
        {
            if (file == null || file.ContentLength == 0)
            {
                ModelState.AddModelError(name, "This field is required");
            }
        }

        public string SaveFile(HttpPostedFileBase file)
        {
            var uploadDir = "~/Uploads";
            var localPath = Path.Combine(Server.MapPath(uploadDir), file.FileName);
            var localUrl = Path.Combine(uploadDir, file.FileName);
            file.SaveAs(localPath);
            return localUrl;
        }

        // GET: Mmerchants/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            merchant merchant = db.merchants.Find(id);
            if (merchant == null)
            {
                return HttpNotFound();
            }
            return View(merchant);
        }

        // POST: Mmerchants/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            merchant merchant = db.merchants.Find(id);
            db.merchants.Remove(merchant);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
