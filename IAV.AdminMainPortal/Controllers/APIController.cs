﻿using IAV.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Net.Http.Formatting;
using System.Web;
using System.Net.Http;
using System.Web.Mvc;
using Newtonsoft;
using System.Configuration;
using Newtonsoft.Json;
using System.Text;
using Newtonsoft.Json.Linq;
using System.Security.Cryptography;

// Install-Package Newtonsoft.Json -Version 4.5.1
// update-package Newtonsoft.Json -reinstall
// Install-Package Newtonsoft.Json –IncludePrerelease - u need to upgrade first:  Install-Package NuGet.Client -Version 4.2.0
// service key added to webconfig

namespace IAV.AdminMainPortal.Controllers
{
    public class APIController : Controller
    {
        string serviceURL = ConfigurationManager.AppSettings["service"].ToString();
        string pwd = "ee11cbb19052e40b07aac0ca060c23ee";

        /* DASHBOARD */
        [HttpGet]      
        public async Task<JsonResult> getDashboardProduct() {
            var dashboardModel = new dashboardModel();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(serviceURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //GET Method  
                HttpResponseMessage response = await client.GetAsync("api/Request/getDashboardProduct");
                if (response.IsSuccessStatusCode)
                {
                    dashboardModel = await response.Content.ReadAsAsync<dashboardModel>();
                }
                else
                {
                    Console.WriteLine("Internal server Error");
                }
            }  
            return Json(dashboardModel, JsonRequestBehavior.AllowGet);  
        }

        [HttpGet]
        public async Task<JsonResult> getDashboardOthers()
        {
            var dashboardModel = new dashboardModel();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(serviceURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //GET Method  
                HttpResponseMessage response = await client.GetAsync("api/Request/getDashboardOthers");
                if (response.IsSuccessStatusCode)
                {
                    dashboardModel = await response.Content.ReadAsAsync<dashboardModel>();
                }
                else
                {
                    Console.WriteLine("Internal server Error");
                }
            }
            return Json(dashboardModel, JsonRequestBehavior.AllowGet);
        }

        /* ORDER */
        [HttpGet]      
        public async Task<int> getAllProductOrderCount(){
            int count = 0;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(serviceURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //GET Method  
                HttpResponseMessage response = await client.GetAsync("api/Request/getAllProductOrderCount");
                if (response.IsSuccessStatusCode)
                {
                    count = await response.Content.ReadAsAsync<int>();
                }
                else
                {
                    Console.WriteLine("Internal server Error");
                }
            }
            return count;
        }

        [HttpGet]
        public async Task<JsonResult> getAllProductOrderByPage(int page = 1, int limit = 10, string q = "")
        {
            List<product_order> po = new List<product_order>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(serviceURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //GET Method  
                HttpResponseMessage response = await client.GetAsync("api/Request/getAllProductOrderByPage?page=" + page + "&limit=" + limit + "&q=" + q);
                if (response.IsSuccessStatusCode)
                {
                    string result = await response.Content.ReadAsAsync<string>();
                    po = JsonConvert.DeserializeObject<List<product_order>>(result);

                }
                else
                {
                    Console.WriteLine("Internal server Error");
                }
            }
            return Json(po, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public async Task<JsonResult> getAllProductOrderDetailByOrderId(int order_id)
        {
            List<product_order_detail> pod = new List<product_order_detail>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(serviceURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //GET Method  
                HttpResponseMessage response = await client.GetAsync("api/Request/getAllProductOrderDetailByOrderId?order_id=" + order_id);
                if (response.IsSuccessStatusCode)
                {
                    string result = await response.Content.ReadAsAsync<string>();
                    pod = JsonConvert.DeserializeObject<List<product_order_detail>>(result);

                }
                else
                {
                    Console.WriteLine("Internal server Error");
                }
            }
            return Json(pod, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<ContentResult> insertOneProductOrder(string token, product_order product_order)
        {
            token = pwd; // Titiksha - need to pass token from login
            string result = string.Empty;
            if (await checkAuthorization(token))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(serviceURL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    string jsonContent = JsonConvert.SerializeObject(product_order);
                    var content = new StringContent(jsonContent, Encoding.UTF8, "application/json");

                    HttpResponseMessage response = await client.PostAsync("api/Request/insertOneProductOrder", content);//?json=" + JsonConvert.SerializeObject(product_order));
                    if (response.IsSuccessStatusCode)
                    {
                        result = await response.Content.ReadAsAsync<string>();
                    }
                }
            }
            return Content(result, "application/json");
        }

        [HttpPost]
        public async Task<ContentResult> insertAllProductOrderDetail(string token, List<product_order_detail> order_details)
        {
            token = pwd; // Titiksha - need to pass token from login
            string result = string.Empty;
            if (await checkAuthorization(token))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(serviceURL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    string jsonContent = JsonConvert.SerializeObject(order_details);
                    var content = new StringContent(jsonContent, Encoding.UTF8, "application/json");
                     
                    HttpResponseMessage response = await client.PostAsync("api/Request/insertAllProductOrderDetail", content);
                    if (response.IsSuccessStatusCode)
                    {
                        result = await response.Content.ReadAsAsync<string>();
                    }
                }
            }
            return Content(result, "application/json");
        }
        [HttpPost]
        public async Task<ContentResult> updateOneProductOrder(string token, string id, product_order product_order)
        {
            token = pwd; // Titiksha - need to pass token from login
            string  result = string.Empty;
            if (await checkAuthorization(token))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(serviceURL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    string jsonContent = JsonConvert.SerializeObject(product_order);
                    var content = new StringContent(jsonContent, Encoding.UTF8, "application/json");

                    HttpResponseMessage response = await client.PostAsync("api/Request/updateOneProductOrder", content);//?json=" + JsonConvert.SerializeObject(product_order));
                    if (response.IsSuccessStatusCode)
                    {
                         result = await response.Content.ReadAsAsync<string>(); 
                    }
                }
            }
            return Content(result, "application/json");            
        }

        [HttpGet]
        public async Task<ContentResult> deleteOneProductOrder(string token, int id)
        {
            token = pwd; // Titiksha - need to pass token from login
            string result = string.Empty;
            if (await checkAuthorization(token))
                {
                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new Uri(serviceURL);
                        client.DefaultRequestHeaders.Accept.Clear();
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        //GET Method  
                        HttpResponseMessage response = await client.GetAsync("api/Request/deleteOneProductOrder?order_id=" + id);
                        if (response.IsSuccessStatusCode)
                        {
                            result = await response.Content.ReadAsAsync<string>();  

                        }
                    }
                }
            return Content(result, "application/json");   
        }

        [HttpPost]
        public async Task<JsonResult> processProductOrder(string token, int id, product_order order, product_order_detail order_detail)
        {
            token = pwd; // Titiksha - need to pass token from login
            processOrderModel result = new processOrderModel();
            if (await checkAuthorization(token))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(serviceURL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    /*var data = new {id = id, product_order = order, product_order_detail = order_detail};
                    var myContent = JsonConvert.SerializeObject(data);
                    var buffer = System.Text.Encoding.UTF8.GetBytes(myContent);
                    var byteContent = new ByteArrayContent(buffer);*/

                    string jsonContent = "{'id':" + id.ToString() + ",'product_order':" + JsonConvert.SerializeObject(order) + ",'product_order_detail':" + JsonConvert.SerializeObject(order_detail) + "}";
                    var content = new StringContent(jsonContent, Encoding.UTF8, "application/json");

                    HttpResponseMessage response = await client.PostAsync("api/Request/processProductOrder", content);
                    if (response.IsSuccessStatusCode)
                    {
                        result = await response.Content.ReadAsAsync<processOrderModel>();

                    }
                }
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public async Task<ContentResult> getOneProductOrder(int id)
        {
            string result = string.Empty;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(serviceURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //GET Method  
                HttpResponseMessage response = await client.GetAsync("api/Request/getOneProductOrder?order_id=" + id);
                if (response.IsSuccessStatusCode)
                {
                     result = await response.Content.ReadAsAsync<string>();

                }
            }
            return Content(result, "application/json");        
        }

        /* TABLE CONFIG TRANSACTION */
        [HttpGet]
        public async Task<JsonResult> getAllConfig()
        {
            List<config> c = new List<config>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(serviceURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //GET Method  
                HttpResponseMessage response = await client.GetAsync("api/Request/getAllConfig");
                if (response.IsSuccessStatusCode)
                {
                    string result = await response.Content.ReadAsAsync<string>();
                    c = JsonConvert.DeserializeObject<List<config>>(result);

                }
                else
                {
                    Console.WriteLine("Internal server Error");
                }
            }
            return Json(c, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public async Task<ContentResult> updateAllConfig(string token, List<config> config)
        {
            token = pwd; // Titiksha - need to pass token from login
            string result = string.Empty;
            if (await checkAuthorization(token))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(serviceURL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    string jsonContent = JsonConvert.SerializeObject(config);
                    var content = new StringContent(jsonContent, Encoding.UTF8, "application/json");
                    HttpResponseMessage response = await client.PostAsync("api/Request/updateAllConfig", content);

                    if (response.IsSuccessStatusCode)
                    {
                        result = await response.Content.ReadAsAsync<string>();

                    }
                    else
                    {
                        Console.WriteLine("Internal server Error");
                    }
                }
            }
            return Content(result, "application/json");
        }

        /* currency table */
        public async Task<JsonResult> getAllCurrency()
        {
            List<currency> c = new List<currency>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(serviceURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //GET Method  
                HttpResponseMessage response = await client.GetAsync("api/Request/getAllCurrency");
                if (response.IsSuccessStatusCode)
                {
                    string result = await response.Content.ReadAsAsync<string>();
                    c = JsonConvert.DeserializeObject<List<currency>>(result);

                }
                else
                {
                    Console.WriteLine("Internal server Error");
                }
            }
            return Json(c, JsonRequestBehavior.AllowGet);
        }
        

        /* User */
        [HttpGet]
        public async Task<bool> checkAuthorization(string token)
        {
            bool r = false;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(serviceURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //GET Method  
                HttpResponseMessage response = await client.GetAsync("api/Request/checkAuthorization?token=" + token);
                if (response.IsSuccessStatusCode)
                {
                    r = await response.Content.ReadAsAsync<bool>();

                }
                else
                {
                    Console.WriteLine("Internal server Error");
                }
            }

            return r;
        }

        public async Task<JsonResult> getOneUser(int id)
        {
            user usr = new user();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(serviceURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //GET Method  
                HttpResponseMessage response = await client.GetAsync("api/Request/getOneUser?id=" + id);
                if (response.IsSuccessStatusCode)
                {
                    string result = await response.Content.ReadAsAsync<string>();
                    usr = JsonConvert.DeserializeObject<user>(result);
                }
                else
                {
                    Console.WriteLine("Internal server Error");
                }
            }
            return Json(usr, JsonRequestBehavior.AllowGet);
        }

        public async Task<ContentResult> insertOneUser(string token, user user)
        {
            token = pwd; // Titiksha - need to pass token from login
            string result = string.Empty;
            if (await checkAuthorization(token))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(serviceURL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    string jsonContent = JsonConvert.SerializeObject(user);
                    var content = new StringContent(jsonContent, Encoding.UTF8, "application/json");

                    HttpResponseMessage response = await client.PostAsync("api/Request/insertOneUser", content);
                    if (response.IsSuccessStatusCode)
                    {
                        result = await response.Content.ReadAsAsync<string>();
                    }
                    else
                    {
                        Console.WriteLine("Internal server Error");
                    }
                }
            }
            return Content(result, "application/json");
        }

        public async Task<ContentResult> updateOneUser(string token, int id, user user)
        {
            token = pwd; // Titiksha - need to pass token from login
            string result = string.Empty;
            if (await checkAuthorization(token))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(serviceURL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    string jsonContent = JsonConvert.SerializeObject(user);
                    var content = new StringContent(jsonContent, Encoding.UTF8, "application/json");

                    HttpResponseMessage response = await client.PostAsync("api/Request/updateOneUser", content);//?json=" + JsonConvert.SerializeObject(product_order));
                    if (response.IsSuccessStatusCode)
                    {
                        result = await response.Content.ReadAsAsync<string>();
                    }
                    else
                    {
                        Console.WriteLine("Internal server Error");
                    }
                }
            }
            return Content(result, "application/json");
        }

        #region  FCM 

        public async Task<string> sendNotif(string body, config config)
        {
            return "";
        }

        public async Task<JsonResult> getAllFcm()
        {
            List<fcm> fcm = new List<fcm>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(serviceURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //GET Method  
                HttpResponseMessage response = await client.GetAsync("api/Request/getAllFcm");
                if (response.IsSuccessStatusCode)
                {
                    string result = await response.Content.ReadAsAsync<string>();
                    fcm = JsonConvert.DeserializeObject<List<fcm>>(result);
                }
                else
                {
                    Console.WriteLine("Internal server Error");
                }
            }
            return Json(fcm, JsonRequestBehavior.AllowGet);
        }
        
        public async Task<JsonResult> getAllFcmByPage(int page = 1, int limit = 10, string q = "")
        {
            List<fcm> fcm = new List<fcm>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(serviceURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //GET Method  
                HttpResponseMessage response = await client.GetAsync("api/Request/getAllFcmByPage?page=" + page + "&limit=" + limit + "&q=" + q);
                if (response.IsSuccessStatusCode)
                {
                    string result = await response.Content.ReadAsAsync<string>();
                    fcm = JsonConvert.DeserializeObject<List<fcm>>(result);
                }
                else
                {
                    Console.WriteLine("Internal server Error");
                }
            }
            return Json(fcm, JsonRequestBehavior.AllowGet);
        }
        
        public async Task<int> getAllFcmCount(string q)
        {
            int count = 0;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(serviceURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //GET Method  
                HttpResponseMessage response = await client.GetAsync("api/Request/getAllFcmCount?q=" + q);
                if (response.IsSuccessStatusCode)
                {
                    count = await response.Content.ReadAsAsync<int>();
                }
                else
                {
                    Console.WriteLine("Internal server Error");
                }
            }
            return count;
        }

        #endregion

        #region  APP_VERSION 
	 
        public async Task<JsonResult> getOneAppVersion(int id)
        {
            app_version app = new app_version();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(serviceURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //GET Method  
                HttpResponseMessage response = await client.GetAsync("api/Request/getOneAppVersion?id=" + id);
                if (response.IsSuccessStatusCode)
                {
                    string result = await response.Content.ReadAsAsync<string>();
                    app = JsonConvert.DeserializeObject<app_version>(result);
                }
                else
                {
                    Console.WriteLine("Internal server Error");
                }
            }
            return Json(app, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> getAllAppVersionByPage(int page = 1, int limit = 10, string q = "")
        {
            List<app_version> app = new List<app_version>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(serviceURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //GET Method  
                HttpResponseMessage response = await client.GetAsync("api/Request/getAllAppVersionByPage?page=" + page + "&limit=" + limit + "&q=" + q);
                if (response.IsSuccessStatusCode)
                {
                    string result = await response.Content.ReadAsAsync<string>();
                    app = JsonConvert.DeserializeObject<List<app_version>>(result);
                }
                else
                {
                    Console.WriteLine("Internal server Error");
                }
            }
            return Json(app, JsonRequestBehavior.AllowGet);
        }

        public async Task<int> getAllAppVersionCount(string q)
        {
            int count = 0;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(serviceURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //GET Method  
                HttpResponseMessage response = await client.GetAsync("api/Request/getAllAppVersionCount?q=" + q);
                if (response.IsSuccessStatusCode)
                {
                    count = await response.Content.ReadAsAsync<int>();
                }
                else
                {
                    Console.WriteLine("Internal server Error");
                }
            }
            return count;
        }

        [HttpPost]
        public async Task<ContentResult> insertOneAppVersion(string token, app_version app_version)
        {
            string result = string.Empty;
            token = pwd; // Titiksha - need to pass token from login

            if (await checkAuthorization(token))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(serviceURL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    string jsonContent = JsonConvert.SerializeObject(app_version);
                    var content = new StringContent(jsonContent, Encoding.UTF8, "application/json");
                    HttpResponseMessage response = await client.PostAsync("api/Request/insertOneAppVersion", content);

                    if (response.IsSuccessStatusCode)
                    {
                        result = await response.Content.ReadAsAsync<string>();
                    }
                    else
                    {
                        Console.WriteLine("Internal server Error");
                    }
                }
            }

            return Content(result, "application/json");
        }

        [HttpPost]
        public async Task<ContentResult> updateOneAppVersion(string token, int id, app_version app_version)
        {
            string result = string.Empty;
            token = pwd; // Titiksha - need to pass token from login

            if (await checkAuthorization(token))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(serviceURL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    app_version.id = id;
                    string jsonContent = JsonConvert.SerializeObject(app_version);
                    var content = new StringContent(jsonContent, Encoding.UTF8, "application/json");
                    HttpResponseMessage response = await client.PostAsync("api/Request/updateOneAppVersion", content);

                    if (response.IsSuccessStatusCode)
                    {
                        result = await response.Content.ReadAsAsync<string>();
                    }
                    else
                    {
                        Console.WriteLine("Internal server Error");
                    }
                }
            }

            return Content(result, "application/json");
        }
        
        public async Task<ContentResult> deleteOneAppVersion(string token,  int id)
        {
            string result = string.Empty;
            token = pwd; // Titiksha - need to pass token from login

            if (await checkAuthorization(token))
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(serviceURL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    //GET Method  
                    HttpResponseMessage response = await client.GetAsync("api/Request/deleteOneAppVersion?id=" + id);
                    if (response.IsSuccessStatusCode)
                    {
                        result = await response.Content.ReadAsAsync<string>();
                    }
                    else
                    {
                        Console.WriteLine("Internal server Error");
                    }
                }
            }
            return Content(result, "application/json");
        }

        #endregion

        public async Task<JsonResult> getAllProductByPage()
        {
            List<product> products = new List<product>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(serviceURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //GET Method  
                HttpResponseMessage response = await client.GetAsync("api/Request/getAllProductByPage");
                if (response.IsSuccessStatusCode)
                {
                    string result = await response.Content.ReadAsAsync<string>();
                    products = JsonConvert.DeserializeObject<List<product>>(result);
                }
                else
                {
                    Console.WriteLine("Internal server Error");
                }
            }
            return Json(products, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> getAllCategory()
        {
            List<category> products = new List<category>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(serviceURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //GET Method  
                HttpResponseMessage response = await client.GetAsync("api/Request/getAllCategory");
                if (response.IsSuccessStatusCode)
                {
                    string result = await response.Content.ReadAsAsync<string>();
                    products = JsonConvert.DeserializeObject<List<category>>(result);
                }
                else
                {
                    Console.WriteLine("Internal server Error");
                }
            }
            return Json(products, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> getAllProductCount(string q, int category_id)
        {
            int count = 0;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(serviceURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //GET Method  
                HttpResponseMessage response = await client.GetAsync("api/Request/getAllProductCount?q=" + q + "&category_id=" + category_id);
                if (response.IsSuccessStatusCode)
                {
                    string result = await response.Content.ReadAsAsync<string>();
                    count = JsonConvert.DeserializeObject<int>(result);
                }
                else
                {
                    Console.WriteLine("Internal server Error");
                }
            }
            return Json(count, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> getAllCategoryByProductId(int product_id)
        {
            List<category> categories = new List<category>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(serviceURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //GET Method  
                HttpResponseMessage response = await client.GetAsync("api/Request/getAllCategoryByProductId?product_id="+product_id);
                if (response.IsSuccessStatusCode)
                {
                    string result = await response.Content.ReadAsAsync<string>();
                    categories = JsonConvert.DeserializeObject<List<category>>(result);
                }
                else
                {
                    Console.WriteLine("Internal server Error");
                }
            }
            return Json(categories, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> getAllProductImageByProductId(int product_id)
        {
            List<product_image> images = new List<product_image>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(serviceURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //GET Method  
                HttpResponseMessage response = await client.GetAsync("api/Request/getAllProductImageByProductId?product_id=" + product_id);
                if (response.IsSuccessStatusCode)
                {
                    string result = await response.Content.ReadAsAsync<string>();
                    images = JsonConvert.DeserializeObject<List<product_image>>(result);
                }
                else
                {
                    Console.WriteLine("Internal server Error");
                }
            }
            return Json(images, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> deleteOneProduct(int id)
        {
            string status = string.Empty;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(serviceURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //GET Method  
                HttpResponseMessage response = await client.GetAsync("api/Request/deleteOneProduct?id=" + id);
                if (response.IsSuccessStatusCode)
                {
                    string result = await response.Content.ReadAsAsync<string>();
                    status = JsonConvert.DeserializeObject<string>(result);
                }
                else
                {
                    Console.WriteLine("Internal server Error");
                }
            }
            return Json(status, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> getOneProduct(int id)
        {
            product product = new product();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(serviceURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //GET Method  
                HttpResponseMessage response = await client.GetAsync("api/Request/getOneProduct?id=" + id);
                if (response.IsSuccessStatusCode)
                {
                    string result = await response.Content.ReadAsAsync<string>();
                    product = JsonConvert.DeserializeObject<product>(result);
                }
                else
                {
                    Console.WriteLine("Internal server Error");
                }
            }
            return Json(product, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> updateOneProduct(int id, product product)
        {
            product pro = new product();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(serviceURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                string jsonContent = "{'id':" + id.ToString() + ",'product':" + JsonConvert.SerializeObject(product) + "}";

                //var content = new FormUrlEncodedContent(values);
                var content = new StringContent(jsonContent, Encoding.UTF8, "application/json");

                //Post Method  
                HttpResponseMessage response = await client.PostAsync("api/Request/updateOneProduct", content);
                if (response.IsSuccessStatusCode)
                {
                    string result = await response.Content.ReadAsAsync<string>();
                    pro = JsonConvert.DeserializeObject<product>(result);
                }
                else
                {
                    Console.WriteLine("Internal server Error");
                }
            }
            return Json(product, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> insertOneProduct(product product)
        {
            product pro = new product();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(serviceURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                string jsonContent = JsonConvert.SerializeObject(product);

                //var content = new FormUrlEncodedContent(values);
                var content = new StringContent(jsonContent, Encoding.UTF8, "application/json");

                //Post Method  
                HttpResponseMessage response = await client.PostAsync("api/Request/insertOneProduct", content);
                if (response.IsSuccessStatusCode)
                {
                    string result = await response.Content.ReadAsAsync<string>();
                    pro = JsonConvert.DeserializeObject<product>(result);
                }
                else
                {
                    Console.WriteLine("Internal server Error");
                }
            }
            return Json(product, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> insertAllProductCategory(List<product_category> prodcat)
        {
            string status = "";
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(serviceURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                string jsonContent = JsonConvert.SerializeObject(prodcat);

                //var content = new FormUrlEncodedContent(values);
                var content = new StringContent(jsonContent, Encoding.UTF8, "application/json");

                //Post Method  
                HttpResponseMessage response = await client.PostAsync("api/Request/deleteInsertAll", content);
                if (response.IsSuccessStatusCode)
                {
                    string result = await response.Content.ReadAsAsync<string>();
                    status = JsonConvert.DeserializeObject<string>(result);
                }
                else
                {
                    Console.WriteLine("Internal server Error");
                }
            }
            return Json(status, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> insertAllProductImage(List<product_image> prodimg)
        {
            string status = "";
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(serviceURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                string jsonContent = JsonConvert.SerializeObject(prodimg);

                //var content = new FormUrlEncodedContent(values);
                var content = new StringContent(jsonContent, Encoding.UTF8, "application/json");

                //Post Method  
                HttpResponseMessage response = await client.PostAsync("api/Request/insertAllProductImage", content);
                if (response.IsSuccessStatusCode)
                {
                    string result = await response.Content.ReadAsAsync<string>();
                    status = JsonConvert.DeserializeObject<string>(result);
                }
                else
                {
                    Console.WriteLine("Internal server Error");
                }
            }
            return Json(status, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> deleteOneCategory(int id)
        {
            string status = string.Empty;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(serviceURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //GET Method  
                HttpResponseMessage response = await client.GetAsync("api/Request/deleteOneCategory?id=" + id);
                if (response.IsSuccessStatusCode)
                {
                    string result = await response.Content.ReadAsAsync<string>();
                    status = JsonConvert.DeserializeObject<string>(result);
                }
                else
                {
                    Console.WriteLine("Internal server Error");
                }
            }
            return Json(status, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> updateOneCategory(int id, category category)
        {
            string status = string.Empty;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(serviceURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                string jsonContent = "{'id':" + id.ToString() + ",'category':" + JsonConvert.SerializeObject(category) + "}";

                //var content = new FormUrlEncodedContent(values);
                var content = new StringContent(jsonContent, Encoding.UTF8, "application/json");

                //Post Method  
                HttpResponseMessage response = await client.PostAsync("api/Request/updateOneCategory", content);
                if (response.IsSuccessStatusCode)
                {
                    string result = await response.Content.ReadAsAsync<string>();
                    status = JsonConvert.DeserializeObject<string>(result);
                }
                else
                {
                    Console.WriteLine("Internal server Error");
                }
            }
            return Json(category, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> insertOneCategory(category category)
        {
            string status = string.Empty;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(serviceURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                string jsonContent = JsonConvert.SerializeObject(category);

                //var content = new FormUrlEncodedContent(values);
                var content = new StringContent(jsonContent, Encoding.UTF8, "application/json");

                //Post Method  
                HttpResponseMessage response = await client.PostAsync("api/Request/insertOneCategory", content);
                if (response.IsSuccessStatusCode)
                {
                    string result = await response.Content.ReadAsAsync<string>();
                    status = JsonConvert.DeserializeObject<string>(result);
                }
                else
                {
                    Console.WriteLine("Internal server Error");
                }
            }
            return Json(category, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> getAllCategoryCount(string q)
        {
            string status = string.Empty;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(serviceURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //GET Method  
                HttpResponseMessage response = await client.GetAsync("api/Request/getAllCategoryCount?q=" + q);
                if (response.IsSuccessStatusCode)
                {
                    string result = await response.Content.ReadAsAsync<string>();
                    status = JsonConvert.DeserializeObject<string>(result);
                }
                else
                {
                    Console.WriteLine("Internal server Error");
                }
            }
            return Json(status, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> getAllCategoryByPage()
        {
            List<category> categories = new List<category>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(serviceURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //GET Method  
                HttpResponseMessage response = await client.GetAsync("api/Request/getAllCategoryByPage");
                if (response.IsSuccessStatusCode)
                {
                    string result = await response.Content.ReadAsAsync<string>();
                    categories = JsonConvert.DeserializeObject<List<category>>(result);
                }
                else
                {
                    Console.WriteLine("Internal server Error");
                }
            }
            return Json(categories, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> getOneCategory(int id)
        {
            category category = new category();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(serviceURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //GET Method  
                HttpResponseMessage response = await client.GetAsync("api/Request/getOneCategory?id=" + id);
                if (response.IsSuccessStatusCode)
                {
                    string result = await response.Content.ReadAsAsync<string>();
                    category = JsonConvert.DeserializeObject<category>(result);
                }
                else
                {
                    Console.WriteLine("Internal server Error");
                }
            }
            return Json(category, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> deleteOneNewsInfo(int id)
        {
            string status = string.Empty;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(serviceURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //GET Method  
                HttpResponseMessage response = await client.GetAsync("api/Request/deleteOneNewsInfo?id=" + id);
                if (response.IsSuccessStatusCode)
                {
                    string result = await response.Content.ReadAsAsync<string>();
                    status = JsonConvert.DeserializeObject<string>(result);
                }
                else
                {
                    Console.WriteLine("Internal server Error");
                }
            }
            return Json(status, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> updateOneNewsInfo(int id, news_info news_info)
        {
            string status = string.Empty;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(serviceURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                string jsonContent = "{'id':" + id.ToString() + ",'news_info':" + JsonConvert.SerializeObject(news_info) + "}";

                //var content = new FormUrlEncodedContent(values);
                var content = new StringContent(jsonContent, Encoding.UTF8, "application/json");

                //Post Method  
                HttpResponseMessage response = await client.PostAsync("api/Request/updateOneNewsInfo", content);
                if (response.IsSuccessStatusCode)
                {
                    string result = await response.Content.ReadAsAsync<string>();
                    status = JsonConvert.DeserializeObject<string>(result);
                }
                else
                {
                    Console.WriteLine("Internal server Error");
                }
            }
            return Json(status, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> insertOneNewsInfo(news_info news)
        {
            string status = string.Empty;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(serviceURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                string jsonContent = JsonConvert.SerializeObject(news);

                //var content = new FormUrlEncodedContent(values);
                var content = new StringContent(jsonContent, Encoding.UTF8, "application/json");

                //Post Method  
                HttpResponseMessage response = await client.PostAsync("api/Request/insertOneNewsInfo", content);
                if (response.IsSuccessStatusCode)
                {
                    string result = await response.Content.ReadAsAsync<string>();
                    status = JsonConvert.DeserializeObject<string>(result);
                }
                else
                {
                    Console.WriteLine("Internal server Error");
                }
            }
            return Json(news, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> getAllNewsInfoCount(string q)
        {
            string status = string.Empty;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(serviceURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //GET Method  
                HttpResponseMessage response = await client.GetAsync("api/Request/getAllNewsInfoCount?q=" + q);
                if (response.IsSuccessStatusCode)
                {
                    string result = await response.Content.ReadAsAsync<string>();
                    status = JsonConvert.DeserializeObject<string>(result);
                }
                else
                {
                    Console.WriteLine("Internal server Error");
                }
            }
            return Json(status, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> getAllNewsInfoByPage()
        {
            List<news_info> news = new List<news_info>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(serviceURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //GET Method  
                HttpResponseMessage response = await client.GetAsync("api/Request/getAllNewsInfoByPage");
                if (response.IsSuccessStatusCode)
                {
                    string result = await response.Content.ReadAsAsync<string>();
                    news = JsonConvert.DeserializeObject<List<news_info>>(result);
                }
                else
                {
                    Console.WriteLine("Internal server Error");
                }
            }
            return Json(news, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> getOneNewsInfo(int id)
        {
            news_info news = new news_info();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(serviceURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //GET Method  
                HttpResponseMessage response = await client.GetAsync("api/Request/getOneNewsInfo?id=" + id);
                if (response.IsSuccessStatusCode)
                {
                    string result = await response.Content.ReadAsAsync<string>();
                    news = JsonConvert.DeserializeObject<news_info>(result);
                }
                else
                {
                    Console.WriteLine("Internal server Error");
                }
            }
            return Json(news, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> isFeaturedNewsExceed()
        {
            int count = 0;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(serviceURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //GET Method  
                HttpResponseMessage response = await client.GetAsync("api/Request/isFeaturedNewsExceed");
                if (response.IsSuccessStatusCode)
                {
                    string result = await response.Content.ReadAsAsync<string>();
                    count = JsonConvert.DeserializeObject<int>(result);
                }
                else
                {
                    Console.WriteLine("Internal server Error");
                }
            }
            return Json(count, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public async Task<ContentResult> login(user usr)
        {
            string result = string.Empty;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(serviceURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                string jsonContent = JsonConvert.SerializeObject(usr);
                var content = new StringContent(jsonContent, Encoding.UTF8, "application/json");

                HttpResponseMessage response = await client.PostAsync("api/Request/login", content);
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsAsync<string>();
                }
            }
            return Content(result, "application/json");
        }
    }
}