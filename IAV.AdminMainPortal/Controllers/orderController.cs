﻿using IAV.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;// added
using System.Net.Http.Headers;// added
using System.Threading.Tasks;// added
using System.Net.Http.Formatting;// added
using System.Web;
using System.Web.Mvc;
using Newtonsoft; 
// Install-Package Newtonsoft.Json -Version 4.5.1
// update-package Newtonsoft.Json -reinstall
// Install-Package Newtonsoft.Json –IncludePrerelease - u need to upgrade first:  Install-Package NuGet.Client -Version 4.2.0

namespace IAV.AdminMainPortal.Controllers
{
    public class OrderController : Controller
    {
        public ActionResult Index()
        {
            return View("List");
        }  

        public ActionResult List()
        {
            return View();
        }

        public ActionResult Create()
        {
            return View();
        }

        public ActionResult Details()
        {
            return View();
        }

        public ActionResult Product_Result()
        {
            return View();
        }

        public ActionResult Product_Pick()
        {
            return View();
        }

        public ActionResult Product_Edit()
        {
            return View();
        }  
    }
}