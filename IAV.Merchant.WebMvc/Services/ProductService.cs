﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IAV.Data.Models;
using IAV.Merchant.WebMvc.Models;

namespace IAV.Merchant.WebMvc.Services
{
    public class ProductService
    {
        markeet_demoEntities dbContextIAV = new markeet_demoEntities();

        public ProductSearchViewModel Search(ProductSearchViewModel model)
        {
            var query = (from p in dbContextIAV.products
                         select p
                            );
            model.Products = query.OrderBy(k =>k.id).Skip(model.NoOfResultPerPage * (model.PageNo - 1)).Take(model.NoOfResultPerPage).ToList<product>();
            //.ToList<ProductSearchView>().Skip(model.NoOfResultPerPage * (model.PageNo - 1)).Take(model.NoOfResultPerPage);

            model.TotalCount = query.Count();
            model.Pages = Pagination.PopulateAllGridPager(model.TotalCount, model.PageNo, model.NoOfResultPerPage);
            //dbContextIAV.product_merchant.Where(p => string.IsNullOrEmpty(model.ProductName) || p.pro
            return model;
        }


        public product GetProductById(int pId)
        {            
            return dbContextIAV.products.Include("product_category").Include("product_image").Where(k => k.id == pId).FirstOrDefault(); 
        }
    }
}