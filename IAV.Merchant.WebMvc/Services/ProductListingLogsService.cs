﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IAV.Data.Models;
using IAV.Merchant.WebMvc.Models;

namespace IAV.Merchant.WebMvc.Services
{
    public class ProductListingLogsService
    {
        markeet_demoEntities dbContextIAV = new markeet_demoEntities();

        public ProductListingLogsSearchViewModel Search(ProductListingLogsSearchViewModel model, string merchantId)
        {
            var query = (from p in dbContextIAV.product_logs
                         join pm in dbContextIAV.product_merchant
                         on p.product_merchant_id equals pm.id
                         where pm.merchant_id == merchantId
                         select p
                            );
            model.ProductLogs = query.OrderBy(k => k.id).Skip(model.NoOfResultPerPage * (model.PageNo - 1)).Take(model.NoOfResultPerPage).ToList<product_log>();
            //.ToList<ProductSearchView>().Skip(model.NoOfResultPerPage * (model.PageNo - 1)).Take(model.NoOfResultPerPage);

            model.TotalCount = query.Count();
            model.Pages = Pagination.PopulateAllGridPager(model.TotalCount, model.PageNo, model.NoOfResultPerPage);
            //dbContextIAV.product_merchant.Where(p => string.IsNullOrEmpty(model.ProductName) || p.pro
            return model;
        }


        //public product_order_detail GetOrderById(int oId)
        //{
        //    return dbContextIAV.product_order_detail.Where(k => k.id == oId).OrderBy(k => k.id).FirstOrDefault();
        //}
    }
}