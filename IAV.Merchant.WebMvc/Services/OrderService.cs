﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IAV.Data.Models;
using IAV.Merchant.WebMvc.Models;

namespace IAV.Merchant.WebMvc.Services
{
    public class OrderService
    {
        markeet_demoEntities dbContextIAV = new markeet_demoEntities();

        public OrderSearchViewModel Search(OrderSearchViewModel model, string merchantId)
        {
            var query = (from o in dbContextIAV.product_order_detail
                         where o.merchant_id == merchantId

                         select new OrderSearchView()
                         {
                             id = o.id,
                             order_id = o.order_id,
                             product_name = o.product_name,
                             amount = o.amount,
                             price_item = o.price_item,
                             created_at = o.created_at,
                             last_update = o.last_update
                         }
                            );
            model.Orders = query.OrderBy(k =>k.id).Skip(model.NoOfResultPerPage * (model.PageNo - 1)).Take(model.NoOfResultPerPage).ToList<OrderSearchView>();
            //.ToList<ProductSearchView>().Skip(model.NoOfResultPerPage * (model.PageNo - 1)).Take(model.NoOfResultPerPage);

            model.TotalCount = query.Count();
            model.Pages = Pagination.PopulateAllGridPager(model.TotalCount, model.PageNo, model.NoOfResultPerPage);
            //dbContextIAV.product_merchant.Where(p => string.IsNullOrEmpty(model.ProductName) || p.pro
            return model;
        }


        public product_order_detail GetOrderById(int oId)
        {
            return dbContextIAV.product_order_detail.Where(k => k.id == oId).OrderBy(k => k.id).FirstOrDefault();
        }
    }
}