﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Web;
using IAV.Data.Models;
using IAV.Merchant.WebMvc.Models;

namespace IAV.Merchant.WebMvc.Services
{
    public class AccountService
    {
        markeet_demoEntities dbContextIAV = new markeet_demoEntities();

        public LoginResponse Login(LoginView request)
        {
            LoginResponse response = new LoginResponse();

            user_account user_account = dbContextIAV.user_accounts.Where(m => m.user_name.ToLower() == request.UserName.ToLower()).FirstOrDefault();

            if (user_account != null)
            {
                if (user_account.is_active == true)
                {
                    if (PasswordHash.ValidatePassword(request.Password, user_account.password))
                    {
                        if (user_account.is_lock == true)
                        {
                            DateTime unlockDate = Convert.ToDateTime(user_account.lockout_date).AddMinutes(30);

                            if (DateTime.Now > unlockDate)
                                user_account.is_lock = false;
                        }

                        if (user_account.is_lock == false)
                        {
                            response.Success = true;
                            user_account.failed_password_attempt_count = 0;
                        }
                        else
                        {
                            response.Message = "Account is locked. Consult admin.";
                        }


                        response.Success = true;
                        response.Message = "Successfully login";
                    }
                    else
                    {
                        user_account.failed_password_attempt_count += 1;

                        if (user_account.failed_password_attempt_count >= 5)
                        {
                            user_account.is_lock = true;
                            response.Message = "Account is locked. Consult admin.";
                        }
                        else
                        {
                            response.Message = "Invalid user id and password.";

                        }
                    }

                    dbContextIAV.SaveChanges();

                }
                else
                {
                    response.Message = "Invalid UserId/Password";
                }
            }
            else
            {
                response.Message = "Invalid UserId/Password";
            }

            return response;
        }

        public LoginResponse ForgotPassword(ForgotPasswordRequest request)
        {
            LoginResponse response = new LoginResponse();

            //user_account user = dbContextIAV.user_accounts.Where(m => m.email != null && m.email.ToLower() == request.Email.ToLower()).FirstOrDefault();

            //if (user != null)
            //{
            //    user_reset_account usr_reset_ac = new user_reset_account();
            //    usr_reset_ac.user_account = user;
            //    usr_reset_ac.is_active = true;
            //    usr_reset_ac.password_reset_id = new Random().Next(10000000, 999999999).ToString();
            //    usr_reset_ac.password_reset_date = DateConverter.ConvertDateTimeToEpoc(DateTime.Now.Date);
            //    usr_reset_ac.created_at = DateConverter.ConvertDateTimeToEpoc(DateTime.Now.Date);
            //    usr_reset_ac.last_update = DateConverter.ConvertDateTimeToEpoc(DateTime.Now.Date);

            //    dbContextIAV.user_reset_accounts.Add(usr_reset_ac);
            //    dbContextIAV.SaveChanges();

            //    string callbackUrl = ConfigurationManager.AppSettings["MerchantResetPasswordUrl"].ToString() + "?code=" + mer_reset_ac.password_reset_id;
            //    EmailHelper.SendMail(request.Email, "Reset Password", "Please reset your password by clicking < a href =\"" + callbackUrl + "\">here</a>");

            //    response.Success = true;
            //    response.Message = "Email has been sent to your email id. please check";
            //}
            //else
            //{
            //    response.Message = "Invalid UserId/Password";
            //}

            return response;
        }


        public ResponseBase ResetPassword(ResetPasswordViewModel model)
        {
            ResponseBase response = new ResponseBase();
            user_account user = dbContextIAV.user_accounts.Where(m => m.email.ToLower() == model.Email.ToLower()).FirstOrDefault();

            if (user != null)
            {
                var usr_reset_ac = dbContextIAV.user_reset_accounts.Where(m => m.user_id == user.id && m.password_reset_id == model.Code).FirstOrDefault();

                if (usr_reset_ac != null && usr_reset_ac.is_active == true)
                {
                    if (Convert.ToDateTime(DateConverter.ConvertEpocToDateTime(usr_reset_ac.created_at)).AddDays(1) >= System.DateTime.Now)
                    {
                        //dbContextIAV.Entry(user_account).State = EntityState.Modified;


                        user.password = PasswordHash.CreateHash(model.Password);
                        user.lockout_date = null;
                        user.is_lock = false;
                        user.failed_password_attempt_count = 0;
                        user.failed_password_start_window = null;

                        dbContextIAV.Entry(user).State = EntityState.Modified;
                        usr_reset_ac.last_update = DateConverter.ConvertDateTimeToEpoc(DateTime.Now.Date);
                        usr_reset_ac.is_active = false;
                        dbContextIAV.SaveChanges();

                        response.Success = true;
                    }
                    else
                    {
                        response.Message = "Reset link expired";
                    }

                }
                else
                {
                    response.Message = "Invalid reset link";
                }
            }
            else
            {
                response.Success = true;
            }

            return response;
        }


        public merchant GetMerchantProfile(string mid)
        {
            var query = (from m in dbContextIAV.merchants
                     where m.mid == mid
                     select m
                         );

            return query.ToList<merchant>().FirstOrDefault();

            //return dbContextIAV.merchants.Where(m => m.mid != mid).FirstOrDefault();

        }
    }
}