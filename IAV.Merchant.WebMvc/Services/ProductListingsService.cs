﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IAV.Data.Models;
using IAV.Merchant.WebMvc.Models;

namespace IAV.Merchant.WebMvc.Services
{
    public class ProductListingsService
    {
        markeet_demoEntities dbContextIAV = new markeet_demoEntities();

        public ProductListingsSearchViewModel Search(ProductListingsSearchViewModel model, string merchantId)
        {
            var query = (from p in dbContextIAV.products
                         join pm in dbContextIAV.product_merchant
                         on p.id.ToString() equals pm.product_id
                         where pm.merchant_id == merchantId
                         select new ProductListingsSearchView()
                         {
                             id = pm.id,
                             name = p.name,
                             quantity_available = pm.quantity_available,
                             actual_price = pm.actual_price,
                             discounted_price = pm.discounted_price
                         }
                            );
            model.ProductListings = query.OrderBy(k => k.id).Skip(model.NoOfResultPerPage * (model.PageNo - 1)).Take(model.NoOfResultPerPage).ToList<ProductListingsSearchView>();
            //.ToList<ProductSearchView>().Skip(model.NoOfResultPerPage * (model.PageNo - 1)).Take(model.NoOfResultPerPage);

            model.TotalCount = query.Count();
            model.Pages = Pagination.PopulateAllGridPager(model.TotalCount, model.PageNo, model.NoOfResultPerPage);
            //dbContextIAV.product_merchant.Where(p => string.IsNullOrEmpty(model.ProductName) || p.pro
            return model;
        }


        public ProductListingsSearchView GetProductListingsByProductName(string productName)
        {
            ProductListingsSearchView productListings = new ProductListingsSearchView();

            var query = (from p in dbContextIAV.products
                         join pm in dbContextIAV.product_merchant
                         on p.id.ToString() equals pm.product_id
                         where p.name == productName
                         select new ProductListingsSearchView()
                         {
                             id = pm.id,
                             product_id= p.id,
                             name = p.name,
                             quantity_available = pm.quantity_available,
                             actual_price = pm.actual_price,
                             discounted_price = pm.discounted_price
                         }
                            );

            productListings = query.ToList<ProductListingsSearchView>().FirstOrDefault();

            if (productListings == null)
            {
                query = (from p in dbContextIAV.products
                         where p.name == productName
                         select new ProductListingsSearchView()
                         {
                             id = 0,
                             product_id=p.id,
                             name = p.name,
                             quantity_available = 0,
                             actual_price = 0,
                             discounted_price = 0
                         }
                         );

                productListings = query.ToList<ProductListingsSearchView>().FirstOrDefault();
            }

            return productListings;
        }

        public SaveProductListingResposne SaveProductListings(ProductListingsSearchView productListingsSearchView, string merchantId)
        {
            SaveProductListingResposne response = new SaveProductListingResposne();
            ProductListingsSearchView existingProductListingsSearchView = null;

            if (productListingsSearchView.id == 0)
            {
                existingProductListingsSearchView = GetProductListingsByProductName(productListingsSearchView.name);
            }

            if (productListingsSearchView.id > 0 || existingProductListingsSearchView != null)
            {
                var query = (from p in dbContextIAV.products
                             join pm in dbContextIAV.product_merchant
                             on p.id.ToString() equals pm.product_id
                             where pm.id == productListingsSearchView.id || p.name == productListingsSearchView.name
                             select pm
                             );

                var product_log = new product_log();

                var product_merchant = query.ToList<product_merchant>().FirstOrDefault();

                if (product_merchant == null)
                {
                    product_merchant = new product_merchant();
                    product_merchant.product_id = productListingsSearchView.product_id.ToString();
                    product_merchant.merchant_id = merchantId;
                    product_merchant.quantity_available = productListingsSearchView.quantity_available;
                    product_merchant.actual_price = productListingsSearchView.actual_price;
                    product_merchant.discounted_price = productListingsSearchView.discounted_price;
                    product_merchant.created_at = DateConverter.ConvertDateTimeToEpoc(System.DateTime.Now);
                    product_merchant.last_update = DateConverter.ConvertDateTimeToEpoc(System.DateTime.Now);

                    dbContextIAV.product_merchant.Add(product_merchant);
                }
                else
                {
                    product_merchant.quantity_available = productListingsSearchView.quantity_available;
                    product_merchant.actual_price = productListingsSearchView.actual_price;
                    product_merchant.discounted_price = productListingsSearchView.discounted_price;
                    product_merchant.last_update = DateConverter.ConvertDateTimeToEpoc(System.DateTime.Now);


                }

                dbContextIAV.SaveChanges();

                if (productListingsSearchView.id == 0)
                {

                    response.Message = "Product listing has been added successfully.";
                }
                else
                {
                    response.Message = "Product listing has been udpated successfully.";
                }

                productListingsSearchView = GetProductListingsByProductName(productListingsSearchView.name);

                product_log.product_merchant_id = product_merchant.id;
                product_log.description = "Quantity " + product_merchant.quantity_available + " || Actual_price "
                    + product_merchant.actual_price + " || discounted_price " + product_merchant.discounted_price;
                product_log.created_at = DateConverter.ConvertDateTimeToEpoc(System.DateTime.Now);
                product_log.last_update = DateConverter.ConvertDateTimeToEpoc(System.DateTime.Now);

                dbContextIAV.product_logs.Add(product_log);
                dbContextIAV.SaveChanges();


                response.Success = true;

                

                response.ProductListings = productListingsSearchView;
            }
            else
            {
                response.ProductListings = existingProductListingsSearchView;
                response.Message = "Invalid request";
            }

            return response;
        }

        public string[] GetAllProducts()
        {
            var query = (from p in dbContextIAV.products
                         where p.status == "READY STOCK"
                         select  p.name
                         
                           );
            string[] products = query.ToArray();
            //.ToList<ProductSearchView>().Skip(model.NoOfResultPerPage * (model.PageNo - 1)).Take(model.NoOfResultPerPage);
            return products;
        }

        public ProductListingsSearchView GetProductListingsById(long id)
        {
            ProductListingsSearchView productListings = new ProductListingsSearchView();

            var query = (from p in dbContextIAV.products
                         join pm in dbContextIAV.product_merchant
                         on p.id.ToString() equals pm.product_id
                         where pm.id == id
                         select new ProductListingsSearchView()
                         {
                             id = pm.id,
                             product_id = p.id,
                             name = p.name,
                             quantity_available = pm.quantity_available,
                             actual_price = pm.actual_price,
                             discounted_price = pm.discounted_price
                         }
                            );

            productListings = query.ToList<ProductListingsSearchView>().FirstOrDefault();

            return productListings;
        }
    }
}
