﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IAV.Merchant.WebMvc.Models
{
    public class DateConverter
    {
        public static DateTime? ConvertEpocToDateTime(Int64 unixTimeStamp)
        {
            return new DateTime(1970, 1, 1, 0, 0, 0).AddMilliseconds(Convert.ToDouble(unixTimeStamp));
        }

        public static Int64 ConvertDateTimeToEpoc(DateTime datetime)
        {
            TimeSpan t = datetime - new DateTime(1970, 1, 1);
            Int64 secondsSinceEpoch = (Int64)t.TotalMilliseconds;
            return secondsSinceEpoch;
        }
    }
}