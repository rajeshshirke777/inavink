﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IAV.Merchant.WebMvc.Models
{
    public class PageIndex
    {
        public PageIndex()
        {

        }
        public PageIndex(string Name, int Id, bool Is_Enable)
        {
            id = Id;
            name = Name;
            is_enable = Is_Enable;
        }
        public int id { get; set; }
        public string name { get; set; }
        public bool is_enable { get; set; }
    }

    public class Pagination
    {
        public static List<PageIndex> PopulateAllGridPager(int recordCount, int currentPage, int NumberOfResultsPerPage)
        {
            double dblPageCount = (double)((decimal)recordCount / NumberOfResultsPerPage);
            int pageCount = (int)Math.Ceiling(dblPageCount);
            List<PageIndex> pages = new List<PageIndex>();
            if (pageCount > 0)
            {
                int firstPage = ((currentPage - 1) / 10 * 10) + 1;
                int lastPage = firstPage + 9;
                if (lastPage > pageCount)
                {
                    lastPage = pageCount;
                }
                //pages.Add(new PageIndex("First", 1, true));
                if (firstPage != 1)
                {
                    pages.Add(new PageIndex("...", (firstPage - 1), true));
                }
                for (int i = firstPage; i <= lastPage; i++)
                {
                    pages.Add(new PageIndex(i.ToString(), i, i != currentPage));
                }
                if (lastPage != pageCount)
                {
                    pages.Add(new PageIndex("...", (lastPage + 1), true));
                }
                //pages.Add(new PageIndex("Last", pageCount, true));
            }
            else
            {
                pages.Clear();
            }
            return pages;
        }
    }
}