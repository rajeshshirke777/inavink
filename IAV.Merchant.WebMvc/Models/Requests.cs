﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using IAV.Data.Models;

namespace IAV.Merchant.WebMvc.Models
{
    public class CustomSerializeModel
    {
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public List<string> RoleName { get; set; }

    }

    public class LoginView
    {
        [Required]
        [Display(Name = "UserName")]
        public string UserName { get; set; }
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }
    }

    public class ForgotPasswordRequest
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class ResetPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class ProductSearchViewModel : SearchViewModel
    {
        public IEnumerable<product> Products { get; set; }
        public string ProductName { get; set; }
        public string Category { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }

    public class SaveProductListingResposne
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public ProductListingsSearchView ProductListings { get; set; }
    }

    public class ProductListingsSearchView
    {
        [Required]
        [Display(Name = "Id")]
        public long id { get; set; }

        [Required]
        [Display(Name = "Product Id")]
        public long product_id { get; set; }

        [Required]
        [Display(Name = "Product Name")]
        public string name { get; set; }

        [Required]
        [Display(Name = "Quantity Available")]
        public Nullable<long> quantity_available { get; set; }

        [Required]
        [Display(Name = "Actual Price")]
        public Nullable<decimal> actual_price { get; set; }

        [Required]
        [Display(Name = "Discounted Price")]
        public Nullable<decimal> discounted_price { get; set; }
    }


    public class ProductListingsView
    {
        public product product { get; set; }
        public product_merchant product_merchant { get; set; }
    }

    public class OrderSearchViewModel : SearchViewModel
    {
        public IEnumerable<OrderSearchView> Orders { get; set; }
        public string ProductName { get; set; }
        public string Category { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }

    public class OrderSearchView
    {
        public long id { get; set; }
        public long order_id { get; set; }
        public long product_id { get; set; }
        public string merchant_id { get; set; }
        public string product_name { get; set; }
        public int amount { get; set; }
        public decimal price_item { get; set; }
        public long created_at { get; set; }
        public long last_update { get; set; }
    }

    public class ProductListingsSearchViewModel : SearchViewModel
    {
        public IEnumerable<ProductListingsSearchView> ProductListings { get; set; }
        public string Description { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }

    public class ProductListingLogsSearchViewModel : SearchViewModel
    {
        public IEnumerable<product_log> ProductLogs { get; set; }
        public string Description { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }

    public class SearchViewModel
    {
        public int NoOfResultPerPage { get; set; }
        public int PageNo { get; set; }
        public int TotalCount { get; set; }
        public List<PageIndex> Pages { get; set; }
    }
}
