﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IAV.Data.Models;

namespace IAV.Merchant.WebMvc.Models
{
    public class LoginResponse : ResponseBase
    {
        public merchant Merchant { get; set; }
    }

    public class ResponseBase
    {
        public string Message { get; set; }
        public bool Success { get; set; }
    }

    public class ProductSearchResponse : SearchResponseBase
    {
        public IEnumerable<product_merchant> product_merchants { get; set; }
    }

    public class SearchResponseBase : ResponseBase
    {
        public int NoOfResultPerPage { get; set; }
        public int PageNo { get; set; }
        public int TotalPages { get; set; }
    }
}