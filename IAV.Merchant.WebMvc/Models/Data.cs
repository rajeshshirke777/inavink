﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IAV.Merchant.WebMvc.Models
{
    public class Navbar
    {
        public int Id { get; set; }
        public string nameOption { get; set; }
        public string controller { get; set; }
        public string action { get; set; }
        public string area { get; set; }
        public string imageClass { get; set; }
        public string activeli { get; set; }
        public bool status { get; set; }
        public int parentId { get; set; }
        public bool isParent { get; set; }
    }

    public class Data
    {
        public IEnumerable<Navbar> navbarItems()
        {
            var menu = new List<Navbar>();
            menu.Add(new Navbar { Id = 1, nameOption = "Dashboard", controller = "Dashboard", action = "Index", imageClass = "fa fa-dashboard fa-fw", status = true, isParent = false, parentId = 0 });
            //menu.Add(new Navbar { Id = 2, nameOption = "Merchants", controller = "Merchants", action = "Index", imageClass = "fa fa-bar-chart-o fa-fw", status = true, isParent = false, parentId = 0 });
            //menu.Add(new Navbar { Id = 3, nameOption = "Products", controller = "Products", action = "Index", imageClass = "fa fa-bar-chart-o fa-fw", status = true, isParent = false, parentId = 0 });
            menu.Add(new Navbar { Id = 4, nameOption = "Orders", controller = "Orders", action = "Index", imageClass = "fa fa-table fa-fw", status = true, isParent = false, parentId = 0 });
            menu.Add(new Navbar { Id = 5, nameOption = "My Listings", controller = "ProductListings", action = "Index", imageClass = "fa fa-edit fa-fw", status = true, isParent = false, parentId = 0 });
            menu.Add(new Navbar { Id = 6, nameOption = "Logs", controller = "ProductListingLogs", action = "Index", imageClass = "fa fa-edit fa-fw", status = true, isParent = false, parentId = 0 });
            menu.Add(new Navbar { Id = 7, nameOption = "Profile", imageClass = "fa fa-wrench fa-fw", controller = "Accounts", action = "Profile", status = true, isParent = false, parentId = 0 });
            
            return menu.ToList();
        }
    }
}