﻿using System;
using IAV.Data.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace IAV.Merchant.WebMvc.CustomAuthentication
{
    public class CustomMembershipUser : MembershipUser
    {
        #region User Properties

        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public ICollection<role> Roles { get; set; }

        #endregion

        public CustomMembershipUser(user_account user):base("CustomMembership", user.user_name, user.id, user.email, string.Empty, string.Empty, true, false, DateTime.Now, DateTime.Now, DateTime.Now, DateTime.Now, DateTime.Now)
        {
            UserId = user.id;
            Roles = user.roles;
        }
    }
}