﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using IAV.Data.Models;
using IAV.Merchant.WebMvc.CustomAuthentication;
using IAV.Merchant.WebMvc.Models;
using IAV.Merchant.WebMvc.Services;
using Newtonsoft.Json;

namespace IAV.Merchant.WebMvc.Controllers
{
    [AllowAnonymous]
    public class AccountsController : Controller
    {
        private AccountService service = new AccountService();
        //
        // GET: /Login/
        [HttpGet]
        public ActionResult Login(string ReturnUrl = "")
        {
            if (User.Identity.IsAuthenticated)
            {
                return LogOut();
            }
            ViewBag.ReturnUrl = ReturnUrl;
            return View();
        }

        //
        // Post: /Login/
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginView loginView, string ReturnUrl = "")
        {
            if (ModelState.IsValid)
            {
                //if (Membership.ValidateUser(loginView.UserName, loginView.Password))
                LoginResponse response = service.Login(loginView);

                if (response.Success)
                {
                    var user = (CustomMembershipUser)Membership.GetUser(loginView.UserName, false);
                    if (user != null)
                    {
                        CustomSerializeModel userModel = new Models.CustomSerializeModel()
                        {
                            UserId = user.UserId,
                            FirstName = user.FirstName,
                            LastName = user.LastName,
                            RoleName = user.Roles.Select(r => r.role_name).ToList()
                        };

                        string userData = JsonConvert.SerializeObject(userModel);
                        FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket
                            (
                            1, loginView.UserName, DateTime.Now, DateTime.Now.AddMinutes(15), false, userData
                            );

                        string enTicket = FormsAuthentication.Encrypt(authTicket);
                        HttpCookie faCookie = new HttpCookie("InAVinkCookie", enTicket);
                        Response.Cookies.Add(faCookie);
                    }

                    if (Url.IsLocalUrl(ReturnUrl))
                    {
                        return Redirect(ReturnUrl);
                    }
                    else
                    {
                        return RedirectToAction("Index", "ProductListings");
                    }
                }
                else
                {
                    ModelState.AddModelError("", response.Message);
                }
            }
            else
            {
                ModelState.AddModelError("", "Invalid UserId/Password");
            }
            return View(loginView);




            //if (!ModelState.IsValid)
            //{
            //    return View();
            //}

            //LoginResponse response = service.Login(request);

            //if (response.Success)
            //{
            //    Session["UserId"] = response.Merchant.mid;
            //    Session["UserName"] = "";//response.Merchant.merchant_account.user_name;
            //    return RedirectToAction("Index", "Orders");
            //}
            //else
            //    ModelState.AddModelError("", response.Message);

            //return View(request);
        }

        public ActionResult LogOut()
        {
            HttpCookie cookie = new HttpCookie("InAVinkCookie", "");
            cookie.Expires = DateTime.Now.AddYears(-1);
            Response.Cookies.Add(cookie);

            FormsAuthentication.SignOut();
            return RedirectToAction("Login", "Accounts", null);
        }

        //
        // GET: /Login/
        public ActionResult ForgotPassword()
        {
            return View();
        }

        //
        // Post: /Login/
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ForgotPassword(ForgotPasswordRequest request)
        {
            if (ModelState.IsValid)
            {
                using (markeet_demoEntities dbContext = new markeet_demoEntities())
                {
                    var user = (from us in dbContext.user_accounts
                                where string.Compare(request.Email, us.email, StringComparison.OrdinalIgnoreCase) == 0
                                select us).FirstOrDefault();

                    if (user != null)
                    {
                         user_reset_account usr_reset_ac = new user_reset_account();
                        usr_reset_ac.user_account = user;
                        usr_reset_ac.is_active = true;
                        usr_reset_ac.password_reset_id = new Random().Next(10000000, 999999999).ToString();
                        usr_reset_ac.password_reset_date= DateConverter.ConvertDateTimeToEpoc(DateTime.Now.Date);
                        usr_reset_ac.created_at = DateConverter.ConvertDateTimeToEpoc(DateTime.Now.Date);
                        usr_reset_ac.last_update = DateConverter.ConvertDateTimeToEpoc(DateTime.Now.Date);

                        dbContext.user_reset_accounts.Add(usr_reset_ac);
                        dbContext.SaveChanges();

                        //string callbackUrl = ConfigurationManager.AppSettings["MerchantResetPasswordUrl"].ToString() + "?code=" + mer_reset_ac.password_reset_id;
                        ResetPasswordEmail(request.Email, usr_reset_ac.password_reset_id);
                    }
                }

                return View("ForgotPasswordConfirmation");
            }

            return View();
        }

        [NonAction]
        public void ResetPasswordEmail(string email, string activationCode)
        {
            var url = string.Format("/Accounts/ResetPassword?code={0}", activationCode);
            var link = Request.Url.AbsoluteUri.Replace(Request.Url.PathAndQuery, url);

            var fromEmail = new MailAddress("rajeshshirke777@gmail.com", "Reset Password - InAVink");
            var toEmail = new MailAddress(email);

            string subject = "Reset Password";

            string body = "<br/> Please click on the following link in order to reset your account" + "<br/><a href='" + link + "'> Reset Password ! </a>";

            SendEmail(fromEmail, toEmail, subject, body);
        }

        [NonAction]
        public bool SendEmail(MailAddress fromEmail, MailAddress toEmail, string subject, string body)
        {
            try
            {
                var fromEmailPassword = "@dotnet10$";
                var smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromEmail.Address, fromEmailPassword)
                };

                using (var message = new MailMessage(fromEmail, toEmail)
                {
                    Subject = subject,
                    Body = body,
                    IsBodyHtml = true

                })

                smtp.Send(message);
                return true;
            }
            catch(Exception ex)
            {

            }

            return false;

        }

        //
        // GET: /Account/ResetPassword
        [AllowAnonymous]
        public ActionResult ResetPassword(string code)
        {
            return code == null ? View("Error") : View();
        }

        //
        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            if (model.Password == model.ConfirmPassword)
            {
                ResponseBase response = service.ResetPassword(model);

                if(response.Success)
                {
                    return RedirectToAction("ResetPasswordConfirmation", "Accounts");
                }

                ModelState.AddModelError("", response.Message);
            }
            else
                ModelState.AddModelError("", "Passwords do not match");

            return View();
        }

        //
        // GET: /Account/ForgotPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        //
        // GET: /Account/ForgotPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        //
        // GET: /Profile/
        [CustomAuthorize(Roles = "merchant")]
        public ActionResult Profile()
        {
            CustomPrincipal principal = (CustomPrincipal)System.Web.HttpContext.Current.User;

            string merchantId = principal.UserId.ToString();

            var merchant = service.GetMerchantProfile(merchantId);

            return View(merchant);
        }
    }
}