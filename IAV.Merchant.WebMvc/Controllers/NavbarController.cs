﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IAV.Merchant.WebMvc.Models;

namespace IAV.Merchant.WebMvc.Controllers
{
    public class NavbarController : Controller
    {
        // GET: Navbar
        public ActionResult Index()
        {
            var data = new IAV.Merchant.WebMvc.Models.Data();
            return PartialView("_Navbar", data.navbarItems().ToList());
        }
    }
}