﻿using IAV.Data.Models;
using IAV.Merchant.WebMvc.CustomAuthentication;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IAV.Merchant.WebMvc.Controllers
{
    // [CustomAuthorize(Roles = "admin")]
    [AllowAnonymous]
    public class MerchantsController : Controller
    {
        // GET: Merchants
        public ActionResult MerchantAdd()
        {
            
            return View();
        }
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Add([Bind(Include = "mid,organization_type,merchant_name_one,merchant_name_two,merchant_name_three,merchant_name_four,merchant_name_five,merchant_name_six,organization_name,store_name,gstin,cin,contact_person_name ,primary_contact_number,secondary_contact_number,primary_emailid,secondary_emailid,working_hours,business_name,signature,address_line1,address_line2,pincode,city,state,store_no,phone_no,latitude,longitude ,store_picture,delivery_charges,charge")]merchant m, HttpPostedFileBase gstin, HttpPostedFileBase gumasta_license, HttpPostedFileBase cin, HttpPostedFileBase store_picture)
        {
            // string db = m.gumasta_license;

           

            CheckFileIsNull(gumasta_license, "gumasta_license");
            CheckFileIsNull(cin, "cin");
            CheckFileIsNull(store_picture, "store_picture");
            CheckFileIsNull(gstin, "gstin");

            if (ModelState.IsValid)
            {
                using (markeet_demoEntities dbContext = new markeet_demoEntities())
                {
                    try
                    {
                       
   
                        m.gstin = SaveFile(gstin);
                        m.tan   = SaveFile(gumasta_license);
                        m.cin   = SaveFile(cin);
                m.store_picture = SaveFile(store_picture);

                        dbContext.merchants.Add(m);
                        dbContext.Entry(m).State = System.Data.Entity.EntityState.Added;
                        dbContext.SaveChanges();
                    }
                    catch (Exception )
                    {

                        throw;
                       
                    }
                    
                }


            }

                return View("MerchantAdd");
        }

      

        public void CheckFileIsNull(HttpPostedFileBase file, string name)
        {
            if (file == null || file.ContentLength == 0)
            {
                ModelState.AddModelError(name, "This field is required");
            }
        }

        public string SaveFile(HttpPostedFileBase file)
        {
             var newid=Guid.NewGuid().ToString() + System.IO.Path.GetExtension(file.FileName); 
            var uploadDir = "~/Uploads";
          //  var UniqueFileName =  file.FileName;
            var localPath = Path.Combine(Server.MapPath(uploadDir), newid);
            var localUrl = Path.Combine(uploadDir, newid);
            file.SaveAs(localPath);
            return localUrl;
        }



    }
}