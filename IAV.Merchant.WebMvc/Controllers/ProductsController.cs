﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IAV.Merchant.WebMvc.CustomAuthentication;
using IAV.Merchant.WebMvc.Models;
using IAV.Merchant.WebMvc.Services;

namespace IAV.Merchant.WebMvc.Controllers
{
    [CustomAuthorize(Roles = "admin")]
    public class ProductsController : Controller
    {
        ProductService service = new ProductService();

        // GET: Products
        public ActionResult Index()
        {           
            ProductSearchViewModel model = new ProductSearchViewModel();
            model.PageNo = 1;
            model.NoOfResultPerPage = 1;

            return View(SearchProducts(model));
        }


        // GET: Product Details
        public ActionResult Details(int id)
        {
            var product = service.GetProductById(id);
            return View(product);
        }

        private ProductSearchViewModel SearchProducts(ProductSearchViewModel model)
        {
            return service.Search(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(ProductSearchViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            return View(SearchProducts(model));
        }

        //public JsonResult getProductCategories()
        //{
        //    //List<Category> categories = new List<Category>();
        //    //using (MyDatabaseEntities dc = new MyDatabaseEntities())
        //    //{
        //    //    categories = dc.Categories.OrderBy(a => a.CategortyName).ToList();
        //    //}
        //    //return new JsonResult { Data = categories, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        //}

        //public JsonResult getProducts(int categoryID)
        //{
        //    //List<Product> products = new List<Product>();
        //    //using (MyDatabaseEntities dc = new MyDatabaseEntities())
        //    //{
        //    //    products = dc.Products.Where(a => a.CategoryID.Equals(categoryID)).OrderBy(a => a.ProductName).ToList();
        //    //}
        //    //return new JsonResult { Data = products, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        //}
    }
}