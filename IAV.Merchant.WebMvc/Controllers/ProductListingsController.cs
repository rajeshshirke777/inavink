﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using IAV.Data.Models;
using IAV.Merchant.WebMvc.CustomAuthentication;
using IAV.Merchant.WebMvc.Models;
using IAV.Merchant.WebMvc.Services;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace IAV.Merchant.WebMvc.Controllers
{
    [CustomAuthorize(Roles = "merchant")]
    public class ProductListingsController : Controller
    {
        ProductListingsService service = new ProductListingsService();

        // GET: Products
        public ActionResult Index()
        {
            ProductListingsSearchViewModel model = new ProductListingsSearchViewModel();
            model.PageNo = 1;
            model.NoOfResultPerPage = 10;

            return View(SearchProductListings(model));
        }

        // GET: ProductListings Details
        public ActionResult Add()
        {
            ViewBag.ShowSearch = true;
            if (TempData["products"] == null)
            {
                TempData["products"] = JsonConvert.SerializeObject(service.GetAllProducts());
                TempData.Keep();
            }
            //ProductListingsSearchView productListings = null;

            //if (!string.IsNullOrEmpty(ProductName))
            //{
            //    productListings = service.GetProductListingsByProductName(ProductName);

            //}
            //else
            //{
            //    productListings = new ProductListingsSearchView();
            //}
            return View(new ProductListingsSearchView());
        }
        // GET: ProductListings Details
        public ActionResult Edit(long id)
        {
            TempData["products"] = "[]";
            var productListings = service.GetProductListingsById(id);
            ViewBag.ShowSearch = false;
            //var product = null; // service.GetProductById(id);
            return View("add", productListings);
        }


        // GET: ProductListings Details
        [HttpPost]
        public JsonResult Save(ProductListingsSearchView model)
        {
            SaveProductListingResposne resposne = new SaveProductListingResposne();

            //if (ModelState.IsValid)
            //{
                CustomPrincipal principal = (CustomPrincipal)System.Web.HttpContext.Current.User;

                string merchantId = principal.UserId.ToString();
                resposne = service.SaveProductListings(model, merchantId);
            //}

            return Json(resposne);
        }


        // GET: ProductListings Details
        [HttpPost]
        public JObject SearchProduct(ProductSearchViewModel request)
        {
            JObject response = new JObject();
            response["Success"] = false;
            //if (TempData["products"] == null)
            //{
            //    TempData["products"] = JsonConvert.SerializeObject(service.GetAllProducts());
            //    TempData.Keep();
            //}

            if (request != null)
            {
                string productName = request.ProductName;
                ProductListingsSearchView productListings = null;


                if (!string.IsNullOrEmpty(productName))
                {
                    productListings = service.GetProductListingsByProductName(productName);

                    if (productListings != null)
                    {
                        response["Success"] = true;
                        response["Message"] = "Product found";
                        response["ProductListings"] = JObject.Parse(JsonConvert.SerializeObject(productListings));
                    }
                    else
                    {
                        response["Message"] = "Product not found";
                    }
                }
                else
                {
                    response["Message"] = "Please enter Product";
                }
            }
            else
            {
                response["Message"] = "Invalid request";
            }
            return response;
        }

        private ProductListingsSearchViewModel SearchProductListings(ProductListingsSearchViewModel model)
        {
            CustomPrincipal principal = (CustomPrincipal)System.Web.HttpContext.Current.User;

            string merchantId = principal.UserId.ToString();
            return service.Search(model, merchantId);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(ProductListingsSearchViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            return View(SearchProductListings(model));
        }

        //public JsonResult getProductCategories()
        //{
        //    //List<Category> categories = new List<Category>();
        //    //using (MyDatabaseEntities dc = new MyDatabaseEntities())
        //    //{
        //    //    categories = dc.Categories.OrderBy(a => a.CategortyName).ToList();
        //    //}
        //    //return new JsonResult { Data = categories, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        //}

        //public JsonResult getProducts(int categoryID)
        //{
        //    //List<Product> products = new List<Product>();
        //    //using (MyDatabaseEntities dc = new MyDatabaseEntities())
        //    //{
        //    //    products = dc.Products.Where(a => a.CategoryID.Equals(categoryID)).OrderBy(a => a.ProductName).ToList();
        //    //}
        //    //return new JsonResult { Data = products, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        //}
    }
}