﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using IAV.Merchant.WebMvc.CustomAuthentication;
using IAV.Merchant.WebMvc.Models;
using IAV.Merchant.WebMvc.Services;

namespace IAV.Merchant.WebMvc.Controllers
{
    [CustomAuthorize(Roles = "merchant")]
    public class ProductListingLogsController : Controller
    {
        ProductListingLogsService service = new ProductListingLogsService();

        // GET: Orders
        public ActionResult Index()
        {
            ProductListingLogsSearchViewModel model = new ProductListingLogsSearchViewModel();
            model.PageNo = 1;
            model.NoOfResultPerPage = 1;

            return View(SearchProductListingLogs(model));
        }


        //// GET: Order Details
        //public ActionResult Details(int id)
        //{
        //    var ProductLog = service.GetProductLogById(id);
        //    return View(productLog);
        //}

        private ProductListingLogsSearchViewModel SearchProductListingLogs(ProductListingLogsSearchViewModel model)
        {
            CustomPrincipal principal = (CustomPrincipal)System.Web.HttpContext.Current.User;

            string merchantId = principal.UserId.ToString();
            return service.Search(model, merchantId);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(ProductListingLogsSearchViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            return View(SearchProductListingLogs(model));
        }

        //[HttpPost]
        //public JsonResult save(OrderMaster order)
        //{
        //    //bool status = false;
        //    //DateTime dateOrg;
        //    //var isValidDate = DateTime.TryParseExact(order.OrderDateString, "mm-dd-yyyy", null, System.Globalization.DateTimeStyles.None, out dateOrg);
        //    //if (isValidDate)
        //    //{
        //    //    order.OrderDate = dateOrg;
        //    //}

        //    //var isValidModel = TryUpdateModel(order);
        //    //if (isValidModel)
        //    //{
        //    //    using (MyDatabaseEntities dc = new MyDatabaseEntities())
        //    //    {
        //    //        dc.OrderMasters.Add(order);
        //    //        dc.SaveChanges();
        //    //        status = true;
        //    //    }
        //    //}
        //    //return new JsonResult { Data = new { status = status } };
        //}
    }
}